<?php

class AdminEditContact {

    private $db;

    public function __construct() {

        $this->db = new \Libs\Database();

        $this->contactFormData();
    }

    public function contactData()
    {
        $this->db->query("SELECT * FROM contact");
        return $this->db->resultSet();
    }


    public function updateContactInfo($data = [])
    {

        $sql = "UPDATE contact
                SET Contact_Phone = :Phone, Contact_Email = :Email, Contact_Address = :Address, Contact_MobilePhone = :Mobile";

        $this->db->query($sql);

        foreach($data as $key => $value) {
            $this->db->bind($key, $value);
        }

        return $this->db->execute();

    }

    public function contactFormData()
    {
        if($_SERVER['REQUEST_METHOD'] == 'POST') {

            $contactData = [
                ':Phone'     => trim($_POST['phone']),
                ':Email'     => trim($_POST['email']),
                ':Address'   => trim($_POST['address']),
                ':Mobile'    => trim($_POST['mobile'])
            ];

            //call update method
            $this->updateContactInfo($contactData);
            //header("Location: edit_contact");
            return $contactData;

        }
    }

}