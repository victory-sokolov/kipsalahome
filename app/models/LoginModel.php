<?php

class LoginModel
{

    private $db;

    public function __construct() {

        $this->db = new \Libs\Database();

        //$this->insertHashedPassword();
    }

    public function rememberMeCheckbox()
    {
        // 7 day session exparation
        $exparation_time = 604800;

        if(isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > $exparation_time)) {

            session_unset($_SESSION['LAST_ACTIVITY']);
            header('Location: login');
        }

        $_SESSION['LAST_ACTIVITY'] = time();
    }

    public function insertHashedPassword()
    {
        $this->db->query("INSERT INTO admin (password, email) VALUES(:password, :login)");
        $this->db->bind('password', PASSWORD_HASH('',PASSWORD_DEFAULT));
        $this->db->bind('login', "");
        $this->db->execute();
    }

    public function validateLogin()
    {
        $loginErrors = [];

        if($_SERVER['REQUEST_METHOD'] == 'POST')
        {

            $loginDetails = [
                'email'     => trim(htmlspecialchars($_POST['email'])),
                'password'  => trim(htmlspecialchars($_POST['password']))
            ];

            //if checkbox checked
            if(isset($_POST['remember_me'])) {
                $this->rememberMeCheckbox();
            }


             if(!empty($loginDetails['email']) || !empty($loginDetails['password']))
            {
                
                $request = $this->login($loginDetails['email'], $loginDetails['password']);
                
            
            
                if(!empty($request) && password_verify($loginDetails['password'] ,$request[0]->password)) {

                    $_SESSION['admin_auth'] = $loginDetails['email'];
                    header('Location: admin');

                } else {
                    $loginErrors[] = "User not found";
                } 
                


            }
            else {
                $loginErrors[] = "Login or password cannot be blank";
            }

        }
        return $loginErrors;
    }

    public function login($email, $password) {

        $this->db->query("SELECT email,password from admin WHERE email = :email");

        $this->db->bind(':email', $email);

        return $this->db->resultSet();
    }


}