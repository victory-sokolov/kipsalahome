<?php

require 'Requests.php';


require __DIR__ . "/../views/classes/UploadImages.php";

class AdminEditHome extends Requests {


    public $lang;

    public $statement;

    public $errors = [];

    public function __construct()
    {

        $this->db = new \Libs\Database();

        $this->setDefLang();

        $this->addRecord();

        $this->getAboutUsData();

        $this->updateRecord();

    }


    public function setDefLang()
    {
        if(isset($_SESSION['admin_lang'])) {
            return $this->lang = $_SESSION['admin_lang'];
        }

        // default language
        return $_SESSION['admin_lang'] = "en";
    }

    public function inputData()
    {
        $inputData = [
            ':Title' => trim($_POST['slider-title']),
            ':Body'  => trim($_POST['slider-body']),
            ":Image" => basename($_FILES["fileToUpload"]["name"]),
            ":Alt"   => trim($_POST['slider-alt'])
        ];

       return $inputData;
    }


    // Add records via modal window
    public function addRecord()
    {

        if (isset($_POST['imageUpload'])) {

            $table = $_POST['value-submit'];

            //Upload image
            UploadImages::uploadImage(ucfirst($table));

            switch ($table) {
                // slider
                case 'slider':
                        $table = "slider";
                        $fields = "slider_title_$this->lang, slider_content_$this->lang, slider_image, image_alt";
                    break;
                // project
                case 'project':
                        $table = "project";
                        $fields = "Project_Name_$this->lang, Project_Description_$this->lang, Project_Image, image_alt";
                    break;
            }


            $sql = "INSERT INTO $table ($fields) VALUES (:Title, :Body, :Image, :Alt)";
            $this->db->query($sql);

            foreach($this->inputData() as $key => $value) {
                $this->db->bind($key, $value);
            }

            $this->db->execute();

            header("Location: " . $_SERVER['REQUEST_URI']);

        }
    }


    public function updateRecord()
    {

        if (isset($_POST['edit-submit'])) {

            $table = $_POST['value-submit'];
            $card_id =  $_SESSION['card_id'];

            $param = [];

            if(!empty($this->inputData()[":Image"])) {
                $param[$table.'_image'] = $this->inputData()[':Image'];
            }

            if(!empty($param)) {
                $this->statement = ", ".$table."_image = :Image";
                UploadImages::uploadImage(ucfirst($table));
            }

            switch ($table) {
                // slider update
                case 'slider':
                    $table = "slider";
                    $fields = "slider_title_$this->lang = :Title, slider_content_$this->lang = :Body $this->statement, image_alt = :Alt";
                    break;
                // project update
                case 'project':
                    $table = "project";
                    $fields = "Project_Name_$this->lang = :Title, Project_Description_$this->lang = :Body $this->statement, image_alt = :Alt";
                    break;
                }

            $sql = "UPDATE $table
                    SET $fields
                    WHERE id = :id";

            $this->db->query($sql);

            foreach($this->inputData() as $key => $value) {
                if(!empty($value)) {
                    $this->db->bind($key, $value);
                }
            }
            $this->db->bind(":id", $card_id);
            $this->db->execute();

           header("Location: " . $_SERVER['REQUEST_URI']);

        }
    }


    public function getAboutUsData()
    {
        $errors = [];

        $table = "about_us_main_page";

        if(isset($_POST['save_about'])) {

            $data = [
                ':Title'     => trim($_POST['about_title']),
                ':Sub_Title' => trim($_POST['sub_title']),
                ':Content'   => trim($_POST['about_content']),
                ':Image'     => basename($_FILES["fileToUpload"]["name"]),
                ':Alt'       => (isset($_POST['image-alt']) ? $_POST['image-alt'] : "")
            ];


            if(!empty($data[':Title']) && !empty($data[':Sub_Title']) && !empty($data[':Content'])) {

                //upload image
                if(!empty($data[':Image'])) {
                    UploadImages::uploadImage();
                }

                //update data
                parent::updateData($data, $table);

            }
        }

    }



}