<?php


class ContactModel {

    private $db;

    public function __construct() {

        $this->db = new \Libs\Database();
    }

    public function getContacts()
    {

        $this->db->query("SELECT * from contact");
        return $this->db->resultSet();
    }

}