<?php


class MainModel {

    private $db;
    public $lang;
    public $content_length = 100;

    public function __construct()
    {

        $this->db = new \Libs\Database();

        $this->lang = $_SESSION['lang'];

    }

    public function latestBlogPost()
    {
        $this->db->query("SELECT id, Blog_Title_$this->lang as title, Blog_Title_en as url,
                    SUBSTRING(`Blog_Content_$this->lang`, 1, $this->content_length) as content, Blog_Image, alt_tag
                    FROM blog LIMIT 3");
        return $this->db->resultSet();
    }


    public function selectAboutUsData()
    {
        $sql = "SELECT id, about_title_$this->lang as title,
                       sub_title_$this->lang as sub_title,
                        content_$this->lang as content,
                        about_us_image as image,
                        alt_tag as alt
                FROM about_us_main_page";

        $this->db->query($sql);
        return $this->db->resultSet();
    }


    public function selectProject()
    {
        $sql = "SELECT id, Project_Name_$this->lang as name,
                        Project_Description_$this->lang as content,
                        Project_Image as image,
                        image_alt as alt
                FROM project";

        $this->db->query($sql);
        return $this->db->resultSet();
    }


    public function selectHomeSlides()
    {
        $sql = "SELECT slider_title_$this->lang as title,
                        slider_content_$this->lang as content,
                        slider_image as image,
                        image_alt as alt
                FROM slider";

        $this->db->query($sql);
        return $this->db->resultSet();
    }
}