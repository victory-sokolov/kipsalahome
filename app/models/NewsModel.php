<?php


class NewsModel
{

    private $db;

    public $lang;

    public $content_lenght = 80;

    public function __construct()
    {
        $this->lang = strtolower($_SESSION['lang']);

        $this->db = new \Libs\Database();
    }

    public function metaDescription()
    {
        $this->db->query("SELECT Meta_Description as meta FROM blog");
        return $this->db->singleFetch();
    }


    public function getNews()
    {
        $this->db->query("SELECT * FROM blog ORDER BY id DESC");
        return $this->db->resultSet();
    }


    public function shortNews()
    {
        $this->db->query("SELECT SUBSTRING(`Blog_Content_$this->lang`, 1, $this->content_lenght) as content FROM blog");
        return $this->db->resultSet();
    }

}