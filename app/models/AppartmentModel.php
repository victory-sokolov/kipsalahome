<?php


class AppartmentModel {

    public $db;

    public $lang;
    public $appartmentUrl;
    public $appartmentTitle;
    public $appartmentID;

    public function __construct()
    {

        $this->db = new \Libs\Database();
        $this->lang = ucfirst(strtolower($_SESSION['lang']));
        
        // Apartment ID for features
        $this->appartmentID = $this->getApartmentId();
        
        $this->getPropertyDetails();

    }

    public function sliderGallery()
    {
        $this->db->query("SELECT image_url as image, alt_tag as alt FROM apartment_gallery WHERE apartment_id = :id");
        $this->db->bind(":id", $this->appartmentID);
        return $this->db->resultSet();
    }

    public function metaDescription()
    {
        $this->db->query("SELECT Meta_Description as meta FROM appartment");
        return $this->db->singleFetch();
    }


    public function setAppartmentUrl()
    {

        $this->appartmentUrl     = str_replace("_", ' ', $_SERVER['REQUEST_URI']);
        $this->appartmentUrl     = preg_split( "/(\/|&)/", $this->appartmentUrl);
   
        $this->appartmentTitle   = $this->appartmentUrl[2];
        return $this->appartmentTitle;
    }
    

    /* Get apartment ID from URL */
    public function getApartmentId() {
        return preg_split("/(\/|&|=)/" ,$_SERVER['REQUEST_URI'])[4];
    }
    

    // public function getApartmentId()
    // {
    //     $this->db->query("SELECT idAppartment FROM appartment WHERE Appartment_Title_En = :appartment_title");

    //     $this->db->bind(":appartment_title", $this->setAppartmentUrl());

    //     return $this->db->singleFetch();
    // }
    

    public function featuresTitle()
    {

        $sql = "SELECT Feature_Title_$this->lang as features FROM features
                WHERE Appartment_ID = :appartment_id";

        $this->db->query($sql);
        $this->db->bind(':appartment_id', $this->appartmentID);

        return $this->db->resultSet();
    }


    public function getPropertyDetails()
    {

        $sql = "SELECT idAppartment, Appartment_Title_$this->lang as title,
                        Appartment_Description_$this->lang as description,
                        Appartment_Price,
                        Appartment_address,
                        Property_Type, Appartment_Images as image,
                        PD_Rooms, PD_Size, PD_PriceUnit, PD_Bathrooms, PD_Floor, PD_AptFloor, PD_Parking, Appartment_View, Project_Name_en
                FROM appartment
                INNER JOIN propertydetails ON appartment.PropDet_ID = propertydetails.idPropertyDetails
                INNER JOIN property ON appartment.Property_ID  = property.idProperty
                INNER JOIN project ON appartment.Project_ID = project.id
                WHERE idAppartment = :id";

        $this->db->query($sql);
        $this->db->bind(':id', $this->appartmentID);

        return $this->db->resultSet();
    }


}