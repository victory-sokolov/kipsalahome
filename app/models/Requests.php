<?php

class Requests
{

    public $db;

    public function __construct()
    {
        $this->db = new \Libs\Database();
    }

    public function updateData($data = [], $table)
    {

        $imageCol = $alt = "";

        if(!empty($_FILES["fileToUpload"]["name"])) {
            $imageCol = ', about_us_image = :Image';
        }

        if(!empty($data[':Alt'])) {
            $alt = ', alt_tag = :Alt';
        }

        $sql = "UPDATE $table
                SET about_title_$this->lang = :Title,
                    sub_title_$this->lang   = :Sub_Title,
                    content_$this->lang     = :Content
                    $alt
                    $imageCol";


        $this->db->query($sql);

        foreach($data as $key => $value) {
            if(!empty($value)) {
                $this->db->bind($key, $value);
            }
        }

        $this->db->execute();

        header("Location: " . $_SERVER['REQUEST_URI']);

    }


}