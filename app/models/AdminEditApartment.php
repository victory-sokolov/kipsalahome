<?php

require_once __DIR__ . "/../views/classes/UploadImages.php";
require_once __DIR__ . "/../views/classes/PropertyData.php";
require_once __DIR__ . '/../views/classes/DBRequests.php';


class AdminEditApartment {

    private $db;

    public $lang;

    public $propertyId;

    private $apartmentId;


    public function __construct() {

        $this->db = new \Libs\Database();

        $this->lang = isset($_SESSION['admin_lang']) ? $_SESSION['admin_lang'] : 'en';

        $this->propertyId = new DBRequests();

        $this->updateApartment();

        $this->defaultApartmentImage();

    }

    public function updateApartment()
    {

        if(isset($_POST['update_apartment'])) {

            $this->apartmentId = $_GET['id'];

            $data = new PropertyData();

            $this->updateApartmentDetails($data);
            $this->updateSliderGallery($data);
            $this->updateProperty($data);
            $this->updateFeatures($data);

            header("Location: " . $_SERVER['REQUEST_URI']);
        }
    }


    public function updateFeatures($data)
    {
        $featuresArr = $data->returnPropertyData()['featuresData'];

        for($i = 0; $i < count($featuresArr['Features']); $i++) {

            $update_features = "UPDATE features SET Feature_Title_$this->lang = :Feature WHERE idFeature = :id";

            $this->db->query($update_features);
            $this->db->bind(':Feature', $featuresArr['Features'][$i]);
            $this->db->bind(":id", $featuresArr['FeatureId'][$i]);
            $this->db->execute();

        }
    }


    public function updateApartmentDetails($data)
    {

        $update_apartment = "UPDATE appartment
            SET Appartment_Title_$this->lang         = :Title,
                Appartment_Description_$this->lang   = :Content,
                Appartment_Price                     = :Price,
                Appartment_View                      = :View,
                Appartment_address                   = :Address,
                Meta_Description                     = :Meta,
                Project_ID                           = :ProjectId,
                Property_ID                          = :Rent
            WHERE idAppartment                       = :id";

        // Update apartment
        $this->db->query($update_apartment);

        foreach($data->returnPropertyData()['apartmentInfo'] as $key => $value) {
            $this->db->bind($key, $value);
        }
        $this->db->bind(':id', $this->apartmentId);

        $this->db->execute();

    }


    public function updateProperty($data)
    {

        $update_property = "UPDATE propertydetails
                            SET PD_Size             = :Size,
                                PD_Rooms            = :Rooms,
                                PD_PriceUnit        = :PriceUnit,
                                PD_Bathrooms        = :Bathrooms,
                                PD_Floor            = :TotalFloors,
                                PD_Parking          = :Parking,
                                PD_AptFloor         = :Floor
                            WHERE idPropertyDetails = :id";

        // Update property
        $this->db->query($update_property);

        foreach($data->returnPropertyData()['propertyData'] as $key => $value) {
            $this->db->bind($key, $value);
        }

        $prop_id = json_decode(json_encode($this->propertyId->propertyID($this->apartmentId)), true);
        $this->db->bind(":id", $prop_id[0]['PropDet_ID']);
        $this->db->execute();
    }

    public function updateSliderGallery($data)
    {
        $folder = "appartment";

        if(!empty($data->returnPropertyData()['apartmentGallery'][':Image'])) {

            $insert_image_gallery = "INSERT INTO apartment_gallery (image_url, alt_tag, apartment_id)
                                    VALUES(:Image, :Alt, :id)";

            UploadImages::uploadImage($folder);
            $id = $this->apartmentId;


            $this->db->query($insert_image_gallery);

            $this->db->bind(':Image', $data->returnPropertyData()['apartmentGallery'][':Image']);
            $this->db->bind(':Alt',  $data->returnPropertyData()['apartmentGallery'][':Alt']);
            $this->db->bind(':id', $id);

            $this->db->execute();

         } else {
            // Update Alt tags
            $alt_image = $data->returnPropertyData()['apartmentGallery'][':Alt'];
            $alt_id = $_POST['alt-id'];

            $arrayValues = array_combine($alt_id, $alt_image);

            foreach($arrayValues as $id => $img) {

                $update_image_gallery = "UPDATE apartment_gallery SET alt_tag = :Alt WHERE id = :id";

                $this->db->query($update_image_gallery);

                $this->db->bind(":Alt", $img);
                $this->db->bind(":id",  $id);
                $this->db->execute();
            }

       }

    }


    public function defaultApartmentImage()
    {
        if(isset($_POST['default_apartment'])) {

            $apartment_id = $_POST['default_apartment'];

            // select image from apartment_gallery
            $this->db->query("SELECT image_url,apartment_id  FROM apartment_gallery WHERE id = :id");
            $this->db->bind(":id", $apartment_id);
            $result = $this->db->resultSet();

            $image = $result[0]->image_url;
            $apartment_id = $result[0]->apartment_id;

            $this->db->query("UPDATE appartment SET Appartment_Images= :image WHERE idAppartment = :id");
            $this->db->bind(":image", $image);
            $this->db->bind(":id", $apartment_id);
            $this->db->execute();

            header("Location: " . $_SERVER['REQUEST_URI']);
        }
    }
}