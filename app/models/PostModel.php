<?php


class PostModel {

    private $db;

    public function __construct() {

        $this->db = new \Libs\Database();
    }

    public function getPosts() {

        $this->db->query("SELECT * from blog ORDER BY id DESC");
        return $this->db->resultSet();

    }

}