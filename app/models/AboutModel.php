<?php


class AboutModel
{

    private $db;

    public $lang;

    public function __construct()
    {
        $this->db = new \Libs\Database();

        $this->lang = $_SESSION['lang'];
    }


    public function selectAbout()
    {
        $sql = "SELECT about_content_left_$this->lang as content_left, about_content_right_$this->lang as content_right FROM about WHERE id = :id";
        $this->db->query($sql);

        $this->db->bind(':id', 1);
        return $this->db->singleFetch();
    }


    public function selectTeamContent()
    {
        $sql = "SELECT content_$this->lang as content FROM team_content WHERE id = :id";
        $this->db->query($sql);

        $this->db->bind(':id', 1);
        return $this->db->singleFetch();
    }

    public function selectEmployees()
    {
        $sql = "SELECT team_id as id, employee_name_$this->lang as name, position_$this->lang as position, employee_photo as photo
                FROM our_team";
        $this->db->query($sql);
        return $this->db->resultSet();
    }

}