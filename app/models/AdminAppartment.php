<?php


class AdminAppartment {

    private $db;

    public function __construct() {

        $this->db = new \Libs\Database();
    }

    public function outputAllApartments()
    {
        $this->db->query("SELECT idAppartment as id, Appartment_Title_En as                 title, Project_Name_en as project, date_added as date
                         FROM appartment
                         INNER JOIN project ON appartment.Project_ID = project.id");

        return $this->db->resultSet();
    }


}