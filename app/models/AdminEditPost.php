<?php


require __DIR__ . "/../views/classes/UploadImages.php";

class AdminEditPost {

    private $db;

    public $lang;

    public $statement;

    public $data;

    public $table = "blog";

    public function __construct() {

        $this->lang = isset($_SESSION['admin_lang']) ? $_SESSION['admin_lang'] : "en";

        $this->db = new \Libs\Database();

        $this->data = $this->formData();
        
        $this->updatePost();

        $this->addPost();
    }


    public function formData()
    {
        if(isset($_POST['blog_update']) || isset($_POST['add_post'])) {

            $data = [
                ':Title'            => $_POST['post-title'],
                ':Content'          => $_POST['content'],
                ':ImageDescription' => (isset($_POST['image-title'])) ? $_POST['image-title'] : "",
                ':MetaDescription'  => (isset($_POST["meta-description"])) ? $_POST["meta-description"] : "",
                ":Alt"              => (isset($_POST["image-alt"])) ? $_POST["image-alt"] : "",
                ':Image'            => basename($_FILES["fileToUpload"]["name"]),
                ":id"               => (isset($_POST['post-id'])) ? $_POST['post-id'] : ""
            ];


            if(empty($data[':id'])) {
                array_pop($data);
            }
            return $data;
        }
    }


    public function updatePost()
    {

        if(isset($_POST['blog_update'])) {

            // call method that checks if image is being uploaded
            $this->checkImage($this->data);

            $sql = "UPDATE $this->table
                    SET Blog_Title_$this->lang             = :Title,
                        Blog_Content_$this->lang           = :Content,
                        Blog_ImageDescription_$this->lang  = :ImageDescription,
                        Meta_Description                   = :MetaDescription
                        $this->statement,
                        alt_tag                            = :Alt
                    WHERE id = :id";

            $this->db->query($sql);

            foreach($this->data as $key => $value) {
                if($key == ':Image' && (empty($value))) continue;
                $this->db->bind($key, $value);
            }

            if(!empty($this->data[':Image'])) {
                $this->db->bind(":Image", $this->data[':Image']);
            }

            $this->db->execute();

            header("Location: " . $_SERVER['REQUEST_URI']);

        }
    }

    /**
     * Checks whether image is being uploaded
    */
    public function checkImage($data)
    {
        //Upload image
        $param = [];

        if(!empty($this->data[":Image"])) {
            $param[$this->table.'_image'] = $this->data[':Image'];
        }

        if(!empty($param)) {
            $this->statement = ", ".$this->table."_image = :Image";
            UploadImages::uploadImage($this->table);
        }

        return $param;
    }


    public function addPost()
    {
        if(isset($_POST['add_post'])) {

           $this->checkImage($this->data);
           $date = date("Y-m-d");

           $sql = "INSERT INTO $this->table
                        (Blog_Title_$this->lang,
                        Blog_Content_$this->lang,
                        Blog_ImageDescription_$this->lang,
                        Meta_Description,
                        Blog_Image, alt_tag, Blog_Date)
                    VALUES (:Title, :Content, :ImageDescription, :MetaDescription, :Image, :Alt, :date)";

            $this->db->query($sql);

            foreach($this->data as $key => $value) {
                $this->db->bind($key, $value);
            }
            $this->db->bind(":date", $date);

            $this->db->execute();

            //header("Location: " . $_SERVER['REQUEST_URI']);
        }
    }



}