<?php

require_once __DIR__ . "/../views/classes/UploadImages.php";
require_once __DIR__ . "/../views/classes/PropertyData.php";

class AddApartment {

    public $db;

    public $lang;

    public $lastId;

    public function __construct()
    {

        $this->db = new \Libs\Database();

        $this->lang = isset($_SESSION['admin_lang']) ? $_SESSION['admin_lang'] : "en";

        $this->apartmentData();

    }


    public function apartmentData()
    {

        if(isset($_POST['add_apartment'])) {

            $data = new PropertyData();

            $this->addProperty($data);
            $this->addAppartment($data);
            $this->addApartmentGallery($data);
            $this->addFeatures($data);

            header("Location: " . $_SERVER['REQUEST_URI']);

        }
    }

    public function addApartmentGallery($data)
    {
        //Upload image to fodler
        $folder = "appartment";

        UploadImages::uploadImage($folder);

        $gallery = "INSERT INTO apartment_gallery (image_url, apartment_id, alt_tag)
                                VALUES(:Image, :id, :Alt)";

        $this->db->query($gallery);

        foreach($data->returnPropertyData()['apartmentGallery'] as $key => $value) {
            $this->db->bind($key, $value);
        }

        $this->db->bind(":id",  $this->lastId);
        $this->db->execute();


    }

    public function addProperty($data)
    {

        $property_details = "INSERT INTO propertydetails
                                (PD_Size, PD_Rooms, PD_PriceUnit, PD_Bathrooms, PD_Floor, PD_Parking, PD_AptFloor)
                            VALUES(:Size, :Rooms, :PriceUnit, :Bathrooms, :TotalFloors, :Parking, :Floor)";

        $this->db->query($property_details);

        foreach($data->returnPropertyData()['propertyData'] as $key => $value) {
           $this->db->bind($key, $value);
        }

        $this->db->execute();
        $this->lastId = $this->db->lastID();

    }


    public function addFeatures($data)
    {

        foreach($data->returnPropertyData()['featuresData']['Features'] as $key => $value) {

            $features = "INSERT INTO features (Feature_Title_$this->lang, Appartment_ID) VALUES (:Features, :id)";

            $this->db->query($features);
            $this->db->bind(':Features', $value);
            $this->db->bind(":id",  $this->lastId);
            $this->db->execute();

        }
    }


    public function addAppartment($data)
    {

        $apartment_image = $data->returnPropertyData()['apartmentGallery'][':Image'];

        $apartment = "INSERT INTO appartment
                (Appartment_Title_$this->lang,
                Appartment_Description_$this->lang,
                Appartment_Price,
                Appartment_View,
                Appartment_address,
                Meta_Description,
                Project_ID, Property_ID, PropDet_ID,
                Appartment_Images, date_added)
            VALUES(:Title, :Content, :Price, :View, :Address, :Meta, :ProjectId, :Rent, :PropertyId, :Image, NOW())";


        $this->db->query($apartment);

        foreach($data->returnPropertyData()['apartmentInfo'] as $key => $value) {

            $this->db->bind($key, $value);
        }

        $this->db->bind('PropertyId', $this->lastId);
        $this->db->bind('Image',      $apartment_image);

        $this->db->execute();
        $this->lastId = $this->db->lastID();
    }



}