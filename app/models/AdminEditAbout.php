<?php

require_once __DIR__ . "/../views/classes/UploadImages.php";


class AdminEditAbout {

    private $db;

    public $lang;

    public $folder = "team";

    public function __construct() {

        $this->db = new \Libs\Database();

        $this->lang = isset($_SESSION['admin_lang']) ? $_SESSION['admin_lang'] : 'en';

        $this->addTeamMember();

        $this->updateAbout();

        $this->updateTeamContent();

        $this->updateEmployee();
    }

    public function addTeamMember()
    {
        if(isset($_POST['teamUpload'])) {

            $name = $_POST['member-name'];
            $position = $_POST['position'];
            $photo =  basename($_FILES["fileToUpload"]["name"]);

            $sql = "INSERT INTO our_team (employee_name_$this->lang,  position_$this->lang, employee_photo)
                    VALUES(:name, :position, :photo)";

            $this->db->query($sql);
            $this->db->bind(":name", $name);
            $this->db->bind(":position", $position);
            $this->db->bind(":photo", $photo);

            $this->db->execute();
            
           UploadImages::uploadImage($this->folder);

           header("Location: " . $_SERVER['REQUEST_URI']);
        }
    }


    public function checkImage($data)
    {
        //Upload image
        $param = [];

        if(!empty($this->data[":Image"])) {
            $param[$this->table.'_image'] = $this->data[':Image'];
        }

        if(!empty($param)) {
            $this->statement = ", ".$this->table."_image = :Image";
            UploadImages::uploadImage($this->table);
        }

        return $param;
    }


    public function updateAbout()
    {
        if(isset($_POST['about-us-left']) || isset($_POST['about-us-right'])) {

            $left_column = $_POST['about-us-left'];
            $right_column = $_POST['about-us-right'];

            $sql = "UPDATE about SET about_content_left_$this->lang = :left_column, about_content_right_$this->lang =:right_column
                    WHERE id = 1";

            $this->db->query($sql);
            $this->db->bind(':left_column', $left_column);
            $this->db->bind(':right_column', $right_column);

            $this->db->execute();

           header("Location: " . $_SERVER['REQUEST_URI']);
        }
    }

    public function updateTeamContent()
    {
        if(isset($_POST['team-description'])) {

            $content = $_POST['team-description'];

            $sql = "UPDATE team_content SET content_$this->lang = :content WHERE id = :id";
            $this->db->query($sql);

            $this->db->bind(':content', $content);
            $this->db->bind(':id', 1);
            $this->db->execute();

            header("Location: " . $_SERVER['REQUEST_URI']);
        }
    }

    public function updateEmployee()
    {

        if(isset($_POST['employee-name'])) {

            $employee_name = $_POST['employee-name'];
            $employee_postion = $_POST['position'];
            $employee_id = $_POST['employee-id'];

            $iterator = new MultipleIterator ();
            $iterator->attachIterator (new ArrayIterator ($employee_name));
            $iterator->attachIterator (new ArrayIterator ($employee_postion));
            $iterator->attachIterator (new ArrayIterator ($employee_id));

            $sql = "UPDATE our_team SET employee_name_$this->lang = :name,
                    position_$this->lang = :position
                     WHERE team_id = :id";
            $this->db->query($sql);

            foreach($iterator as $item) {
                $this->db->bind(':name', $item[0]);
                $this->db->bind(':position',$item[1]);
                $this->db->bind(':id', $item[2]);
                $this->db->execute();
            }

            header("Location: " . $_SERVER['REQUEST_URI']);
        }
    }



}