<?php

require_once __DIR__ . '/../views/classes/DBRequests.php';
require_once __DIR__ . '/../views/classes/Logout.php';


class Add_apartment extends Libs\Controller
{

    public function __construct() {

        Logout::unauth();
        $this->AddApartment = $this->model('AddApartment');

    }

    public function index() {

        $req = new DBRequests();

         $data = [
            "Project"   => $req->selectProjects(),
            "Property"  => $req->propertyRent()
        ];

        if(isset($_SESSION['admin_auth'])) {
            $this->view('pages/admin/add_apartment', $data);
        } else {
            header("Location: /admin");
        }


    }

}