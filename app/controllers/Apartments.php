<?php

require_once __DIR__ . '/../views/classes/Logout.php';

class Apartments extends Libs\Controller {


    public function __construct()
    {
        Logout::unauth();
        $this->AdminApartment = $this->model('AdminAppartment');

    }

    public function index()
    {

        $data = [
            "Appartment_short" => $this->AdminApartment->outputAllApartments()
        ];
        

        if(isset($_SESSION['admin_auth'])) {
            $this->view('pages/admin/apartments', $data);
        } else {
            header("Location: /admin");
        }

    }
    

}