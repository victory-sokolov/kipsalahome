<?php

require_once __DIR__ . '/../views/classes/Logout.php';

class Posts extends Libs\Controller {


    public function __construct() {

        Logout::unauth();
        $this->PostModel = $this->model('AdminPostsModel');

    }

    public function index() {

        $data = $this->PostModel->postPreview();

        if(isset($_SESSION['admin_auth'])) {
            $this->view('pages/admin/posts', $data);
        } else {
            header("Location: /admin");
        }

    }


}