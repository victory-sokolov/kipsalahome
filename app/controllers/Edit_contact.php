<?php

require_once __DIR__ . '/../views/classes/Logout.php';

class Edit_contact extends Libs\Controller {


    public function __construct() {

        Logout::unauth();
        $this->EditContact = $this->model('AdminEditContact');

    }

    public function index() {

        $data = $this->EditContact->contactData();

        if(isset($_SESSION['admin_auth'])) {
            $this->view('pages/admin/edit_contact', $data);
        } else {
            header("Location: /admin");
        }
    }


}