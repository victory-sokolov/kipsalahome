<?php


class Search extends Libs\Controller {

    public function __construct() {

        $this->searchModel = $this->model('SearchModel');

    }


    public function index() {

        $data = $this->searchModel->getTotalAppartments();

        $this->view('pages/search', $data);

    }



}