<?php

class Login extends Libs\Controller
{

    public function __construct() {

        $this->loginModel = $this->model('LoginModel');

    }

    public function index()
    {

        $data = $this->loginModel->validateLogin();

        $this->view('pages/admin/login', $data);

    }



}


