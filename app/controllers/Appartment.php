<?php

require_once __DIR__ .  '/../models/ContactModel.php';

class Appartment extends Libs\Controller {

    public $phone;

    public $description;

    public function __construct()
    {

        $this->appartmentModel = $this->model('AppartmentModel');
        
        $this->phone = new ContactModel();

        $this->getApartmentDescription();

    }


    public function getApartmentDescription()
    {
       $this->description = $this->appartmentModel->getPropertyDetails()[0]->description;
    }


    public function wordCount()
    {

        $string = preg_replace('/\s+/', ' ', trim($this->description));
        $words = explode(" ", $string);
        return count($words) / 2;
    }


    public function wordsLimit($string, $count){
        $words = explode(' ', $string);

        $two_column = [];

        $first_column = array_slice($words, 0, $count);
        $second_column = array_slice($words, $count);

        $string1 = implode(' ', $first_column);
        $string2 = implode(' ', $second_column);

        array_push($two_column, $string1, $string2);
        return $two_column;
    }



    public function index() {

        $data = [
            "Appartment_info"       => $this->appartmentModel->getPropertyDetails(),
            "Appartment_features"   => $this->appartmentModel->featuresTitle(),
            "Appartment_slider"     => $this->appartmentModel->sliderGallery(),
            "MetaDescription"       => $this->appartmentModel->metaDescription(),
            "Phone"                 => $this->phone->getContacts(),
            "Apartment_Description" => $this->wordsLimit($this->description, $this->wordCount())
        ];

        $this->view('pages/appartment', $data);

    }


}