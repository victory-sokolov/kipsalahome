<?php

require_once __DIR__ . '/../views/classes/Logout.php';

class Pages extends Libs\Controller {


    public function __construct() {

        Logout::unauth();
        $this->PagesModel = $this->model('PagesModel');

    }

    public function index() {

        $data = [];

        if(isset($_SESSION['admin_auth'])) {
            $this->view('pages/admin/pages', $data);
        } else {
            header("Location: /admin");
        }

    }


}