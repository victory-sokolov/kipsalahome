<?php

require_once __DIR__ . '/../views/classes/Logout.php';

class Admin extends Libs\Controller
{

    public function __construct() {

        Logout::unauth();
        $this->dashBoardModel = $this->model('AdminMainModel');

        $this->loadDefaultController();
    }


   public function loadDefaultController()
   {

      $exparation_time = 604800; // 7 day exparation

      // check if remember me session is not expired
      if(isset($_SESSION['auth_timestamp']) && (time() - $_SESSION['auth_timestamp'] > $exparation_time))
      {
          Logout::unAuth();
          //header('Location: /admin');

      } else if(isset($_SESSION['admin_auth'])){
        //header('Location: admin');
      } else {
          header('Location: login');
      }
   }


    public function index()
    {

        $data = [];

        $this->view('pages/admin/admin', $data);

    }



}


