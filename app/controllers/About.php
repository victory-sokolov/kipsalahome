<?php


class About extends Libs\Controller
{

    public function __construct() {

        $this->AboutUs = $this->model('AboutModel');

    }

    public function index() {


         $data = [
            "AboutDescription"   =>  $this->AboutUs->selectAbout(),
            "OurTeam"            =>  $this->AboutUs->selectTeamContent(),
            "Employees"          =>  $this->AboutUs->selectEmployees()
        ];

        $this->view("pages/about", $data);
    }

}