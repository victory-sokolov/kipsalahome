<?php

require_once __DIR__ . '/../views/classes/Logout.php';

class Edit_home extends Libs\Controller {


    public function __construct() {

        Logout::unauth();
        $this->EditHome = $this->model('AdminEditHome');

    }



    public function index() {

        $data = [
            //'Project' => $this->EditHome->projectSelect()
            //'Slider_Data'   => $this->EditHome->sliderDataSelect(),
            //'About_Us'      => $this->EditHome->selectAboutUsData()
        ];

        if(isset($_SESSION['admin_auth'])) {
            $this->view('pages/admin/edit_home', $data);
        } else {
            header("Location: /admin");
        }

    }


}