<?php


class News extends Libs\Controller
{

    // public $wordsCount = 100;
    public $lang;

    public function __construct() {

        $this->lang = $_SESSION['lang'];

        $this->NewsModel = $this->model('NewsModel');

    }

    public function index() {

        $data = [
            'MetaDescription' => $this->NewsModel->metaDescription(),
            'News'            => $this->NewsModel->getNews(),
            'ShortNews'       => $this->NewsModel->shortNews()
        ];

        $this->view('pages/news', $data);

    }

}