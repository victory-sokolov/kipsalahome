<?php

// require_once __DIR__ . '/../libraries/Controller.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;


class Contact extends Libs\Controller {

    public function __construct() {

        $this->ContactModel = $this->model('ContactModel');

        $this->emailValidation();

    }

    public function index()
    {
        $contactDataModel = $this->ContactModel->getContacts();

        $this->view('pages/contact', $contactDataModel);

    }

    public function sendEmail($email, $name, $message, $phone)
    {

        $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
        try {
            //Server settings
            //$mail->SMTPDebug = 1;                               // Enable verbose debug output
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = 'smtp.gmail.com';                       // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = "patekdevelopment@gmail.com";       // SMTP username
            $mail->Password = "patek123";                         // SMTP password
            $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 587;                                    // TCP port to connect to

            //Recipients
            $mail->setFrom($email, 'KipsalaHome', 0);
            $mail->addAddress('patekdevelopment@gmail.com');     // Add a recipient

            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = 'KipsalaHome New Message';
            $mail->Body    = "From: $name < $email > <br> Phone number: $phone <br><br>  $message";
            $mail->AltBody = $message;

            $mail->send();
            return true;

        } catch (Exception $e) {
            echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
        }

    }


    public function emailValidation()
    {
        
        if($_SERVER['REQUEST_METHOD'] == 'POST') {


            // Sanitize POST data
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            //Init data
            $contactInfo = [
                "name" => trim($_POST['contactName']),
                "email" => trim($_POST['contactEmail']),
                "phone" => isset($_POST['contactNumber']) ? $_POST['contactNumber'] : null,
                "message" => trim($_POST['contactMessage'])
            ];
            
            

            //Check if inputs are not empty
            if(!empty($contactInfo['name']) && !empty($contactInfo['email']) && !empty($contactInfo['message'])) {
                if($this->sendEmail($contactInfo['email'], $contactInfo['name'], $contactInfo['message'], $contactInfo['phone'])) {
                    $_SESSION['msg'] = LANG::textStuff_emailSent;
                }
            }
            
        }
        


    }

    public function successMessage()
    {
        $successAlert = "<div class='alert alert-success' role='alert'>";
        $successAlert .= $_SESSION['msg'];
        $successAlert .= " <button type='button' class='close' data-dismiss='alert' aria-label='Close'>";
        $successAlert .= "<span aria-hidden='true'>&times;</span></button>";
        $successAlert .= "</div>";
        return $successAlert;
    }

}

