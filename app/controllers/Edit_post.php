<?php

require_once __DIR__ . '/../views/classes/Logout.php';

class Edit_post extends Libs\Controller {


    public function __construct() {

        Logout::unauth();
        $this->EditPost = $this->model('AdminEditPost');

    }

    public function index() {

        $data = [
            //'SelectPost' => $this->EditPost->selectBlogPosts()
        ];

        if(isset($_SESSION['admin_auth'])) {
            $this->view('pages/admin/edit_post', $data);
        } else {
            header("Location: /admin");
        }

    }


}