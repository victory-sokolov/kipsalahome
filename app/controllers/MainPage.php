<?php

class MainPage extends Libs\Controller {

    public $content_len;

    public function __construct()
    {

        $this->mainModel = $this->model('MainModel');

    }

    public function index()
    {

        $data = [
            "Blog_Content" => $this->mainModel->latestBlogPost(),
            "AboutUs"      => $this->mainModel->selectAboutUsData(),
            "OurProjects"  => $this->mainModel->selectProject(),
            "Slider"       => $this->mainModel->selectHomeSlides()
        ];

        $this->view('pages/index', $data);

    }


}