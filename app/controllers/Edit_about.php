<?php

require_once __DIR__ . '/../views/classes/Logout.php';

class Edit_about extends Libs\Controller {

    public function __construct() {
        
        Logout::unauth();
        $this->EditAbout = $this->model('AdminEditAbout');

    }

    public function index() {

        $data = [];
    

        if(isset($_SESSION['admin_auth'])) {
            $this->view('pages/admin/edit_about', $data);
        } else {
            header("Location: /admin");
        }

    }


}