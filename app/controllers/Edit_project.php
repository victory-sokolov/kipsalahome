<?php

require_once __DIR__ . '/../views/classes/Logout.php';

class Edit_project extends Libs\Controller {


    public function __construct() {
        Logout::unauth();
        $this->EditProject = $this->model('AdminEditProject');
    }

    public function index()
    {

        if(isset($_SESSION['admin_auth'])) {
            $this->view('pages/admin/edit_project');
        } else {
            header("Location: /admin");
        }

    }


}