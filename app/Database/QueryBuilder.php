<?php


class QueryBuilder
{

    protected $sql = [];

    public $values = [];

    /*
    *   Select method
    *   By default selects all records
    */
    public function select($fields = '*')
    {
        $this->sql['select'] = "SELECT {$fields}";
        return $this;
    }


    public function delete()
    {
        $this->sql['delete'] = "DELETE ";
        return $this;
    }

    public function from($table)
    {
        $this->sql['from'] = " FROM {$table}";

        return $this;
    }


    public function where($column, $value = "")
    {
        $this->sql['where'][] = "{$column} ";
        $this->values[] = $value;

        return $this;
    }

    public function update($table)
    {
        $this->reset();
        $this->sql['update'] = "UPDATE {$table} ";

        return $this;
    }

    public function insert($table)
    {
        $this->reset();
        $this->sql['insert'] = "INSERT INTO {$table} ";

        return $this;
    }


    public function sql()
    {
        $sql = '';

        if(!empty($this->sql)) {
            foreach ($this->sql as $key => $value) {
                if ($key == 'where') {
                    $sql .= ' WHERE ';
                    foreach ($value as $where) {
                        $sql .= $where;
                        if (count($value) > 1 and next($value)) {
                            $sql .= ' AND ';
                        }
                    }
                } else {
                    $sql .= $value;
                }
            }
        }

        return $sql;
    }

}

