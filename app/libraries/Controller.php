<?php

/*
* Base Controllers
* Loads the models and views
*/

namespace Libs;


class Controller {

    //Load model
    public function model($model) {

        // Require model file
        require_once '../app/models/' . $model . '.php';

        return new $model();

    }


    // Load view
    public function view($view, $data = []) {
        // Checl for view file
        if(file_exists('../app/views/' . $view . '.php')) {
            require_once '../app/views/' . $view . '.php';
        } else {
            die('View does not exists');
        }
    }

}