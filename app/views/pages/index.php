
<?php require(__DIR__ . '/../includes/header.php'); ?>
<?php require(__DIR__ . '/../includes/navigation.php'); ?>
<?php 
    require_once('PostClass.php');
    if(isset($_SESSION['lang'])) {
        $currentLang = strtolower($_SESSION['lang']);
    }
?>

<div class="full-width-slider">

<div class="property-slider">

    <div id='carousel-custom' class='carousel slide' data-ride='carousel'>

        <div class='carousel-outer'>
            <!-- Wrapper for slides -->
            <div class='carousel-inner'>
                <?php foreach($data['Slider'] as $slide) : ?>

                    <div class='carousel-item'>
                        <img src=<?php echo 'images/Slider/'.$slide->image ?> alt="<?php echo $slide->alt ?>"">
                        <h1 class="h-underline animated fadeInDown delay-.5s">
                            <?php echo $slide->title; ?>
                        </h1>
                        <p class="animated fadeInLeft delay-1s">
                            <?php echo $slide->content; ?>
                        </p>
                    </div>

                <?php endforeach; ?>
            </div>


            <a class="carousel-control-prev" href="#carousel-custom" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel-custom" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>

        </div>

    </div>

    </div>

</div>


<div class="container-fluid">

    <section class="about-us-section">
        <?php foreach($data['AboutUs'] as $about): ?>
        <div class="row">
            <div class="col-md-6">
                <div class="about-us-image">
                    <img src="<?php echo "images/".$about->image ?>" alt="<?php echo $about->alt?>">
                </div>
            </div>
            <div class="col-md-5">
                <div class="about-detail-section">
                    <h1 class="text-style"> <?php echo LANG::textStuff_whoweare; ?> </h1>
                    <!--<hr class="horizontal">-->
                    <div class="who-we-are-content col-md-8">

                        <!--<p class="sub-title"><strong><?php echo $about->sub_title; ?></strong></p>-->

                        <p><?php echo $about->content; ?></p>
                        <!-- <button class="btn btn-outline-primary col-md-6">
                            <?php echo LANG::buttons_read; ?>
                        </button> -->
                    </div>

                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </section>
</div>

    <div class="container">
        <section class="our-project-section">

             <h1 class="text-center text-style">
                <?php echo LANG::menu_ourProject ?>
             </h1>
             <!--<hr class="horizontal">-->

             <div class="row">

                <div class="projects-wrapper">

                <?php foreach($data['OurProjects'] as $project) : ?>
                    <div class="col-xs-12 col-md-8">
                        <div class="project-presentation" data-aos="zoom-out-right" data-aos-duration="1500" class="hover01">
                            <img src="images/<?php echo $project->image ?>" alt="<?php echo $project->alt ?>">
                            <div class="project-description" data-aos="fade-right" data-aos-duration="1000">
                                <h3><?php echo $project->name ?></h3>
                                <hr>
                                <p><?php echo $project->content ?></p>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>

                 </div>
             </div>

         </section>


     <section class="latest-blog-posts">

         <h1 class="text-center text-style">
            <?php echo LANG::textStuff_latestPosts; ?>
         </h1>
         <!--<hr class="horizontal">-->

         <div class="row">

            <?php foreach($data['Blog_Content'] as $blogPost):
 
                //$BGTL = "Blog_Title_" . $currentLang;
                
                $postModel = new PostClass();
                $uBGTL = $postModel->url_slug($blogPost->title, array());

                $url = str_replace(' ', '-', "news/" .$uBGTL.'/'.$blogPost->id.'/'.'?lang='.$currentLang);

            ?>
            <?php
                $BGTL = "Blog_Title_" . $currentLang;
                if(!empty($blogPost->title)) :
            ?>            
             <div class="col-sm-12 col-md-4 my-5">

                <div class="card card-body bg-light">
                    <figure>
                        <img src="<?php echo "../public/images/blog/".$blogPost->Blog_Image;?>" alt="<?php echo $blogPost->alt_tag ?>">
                    </figure>
                    <h3 class="text-center"><?php echo $blogPost->title; ?></h3>
                    <p><?php echo $blogPost->content . '...'; ?></p>
                    
                    <div class="text-center">
                 
                        <a href="<?php echo $url; ?>" class="btn btn-outline-primary col-sm-8">
                            <?php echo LANG::buttons_read; ?>
                        </a>
                    </div>

                </div>
             </div>
            <?php endif; ?>
            <?php endforeach; ?>

         </div>

     </div>
     </section>

 </div><!-- Container -->



<?php require (__DIR__ . '/../includes/footer.php'); ?>