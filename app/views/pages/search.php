<?php

    require_once(__DIR__ . '/../includes/header.php');
    require_once(__DIR__ . '/../includes/navigation.php');
    require_once(__DIR__ . '/../includes/meta.php');
    require_once(__DIR__ . '/../../controllers/Contact.php');

    meta("Apartment search by specific category");

?>


<div class="full-width-slider-search">
    <div class="property-slider">

        <div id='carousel-custom' class='carousel slide' data-ride='carousel'>


            <div class='carousel-outer'>
                <div class='carousel-inner'></div>

                <a class="carousel-control-prev" href="#carousel-custom" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carousel-custom" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>

        </div>

    </div>

</div>

<div class="container">
    <section class="appartment-section">

    <?php require_once __DIR__ . '/../includes/breadcrumbs.php'; ?>
    <div class="filter-appartment">

        <div class="col-md-12">
        <h1 class="text-center text-style">
        <?php echo LANG::textStuff_searchHeader; ?>
    </h1>

            <form class="form-inline selection-form">

                <div class="form-group col-sm-6 col-lg-2">
                    <label class="control-label" for="select-project"> <?php echo LANG::searchFilters_allProjects?> </label>
                    <select class="form-control select-projects" id="select-project">
                        
                        <option value="All Projects" label="<?php echo LANG::searchFilters_any ?>">
                            <?php echo LANG::searchFilters_any ?>
                        </option>
                        
                        <option value="1">ZundasDarzi</option>
                        <option value="2">Riverside</option>
                    </select>
                </div>

                <div class="form-group col-sm-6 col-lg-2">
                    <label for="select-property"> <?php echo LANG::searchFilters_allProperty?> </label>
                    <select class="form-control select-property" id="select-property">
                        
                        <option value="All Property" label="<?php echo LANG::searchFilters_any ?>">
                            <?php echo LANG::searchFilters_any ?>
                        </option>

                        <option value="For Sale"><?php echo LANG::searchFilters_forSale ?></option>

                        <option value="For Rent"><?php echo LANG::searchFilters_forRent ?></option>
                    </select>
                </div>

                <div class="form-group col-sm-5 col-lg-2">
                    <label for="minPrice"> <?php echo LANG::searchFilters_price?> (EUR)</label>
                    <input aria-label="Min Price" id="minPrice" type="text" placeholder="<?php echo LANG::searchFilters_minPrice ?>:" class="form-control col-md-12" class="default" list="price-min">
                </div>

                <span> &#45; </span>

                <div class="form-group col-sm-5 col-lg-2">
                    <input type="text" aria-label="Max Price" placeholder="<?php echo LANG::searchFilters_maxPrice ?>:" class="form-control col-md-12" class="default" list="price-max">
                </div>

                <div class="form-group col-sm-12 col-lg-2">
                    <label for="rooms"> <?php echo LANG::searchFilters_rooms?> </label>
                    <select class="form-control select-rooms col-sm-12" id="rooms">

                        <option value="0">
                            <?php echo LANG::searchFilters_allRooms; ?>
                        </option>

                        <option value="1">
                            1 <?php echo LANG::searchFilters_room;?>
                        </option>

                        <option value="2">
                            2 <?php echo LANG::searchFilters_rooms;?>
                        </option>

                        <option value="3">
                            3 <?php echo LANG::searchFilters_rooms;?>
                        </option>

                        <option value="4">
                            4 <?php echo LANG::searchFilters_rooms;?>
                        </option>

                    </select>
                </div>

                <a href="#" class="clear-filter-link btn btn-outline-primary">
                    <?php echo LANG::searchFilters_clearFileds; ?>
                </a>

            </form>

        </div>

    </section>
    
    <?php require_once(__DIR__ . '/../includes/contact-form.php'); ?>


    <!-- Search results -->
    <section class="appartment-result-section">

        <div class="col-md-12">

            <div class="top-result-indicator">
                <div class="col-md-12">

                <div class="parameter-tags"></div>
                <h2>
                    <?php echo LANG::searchFilters_resultsFound; ?> <span></span>
                </h2>

                </div>
            </div>


            <div class="appartment-search-results">
                <div class="appartments-carts">
                    <div class="row"></div>
                </div>
            </div>

        </div>

        </div>

    </section>
</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="js/search.js"></script>

<?php require_once('../app/views/includes/footer.php'); ?>

