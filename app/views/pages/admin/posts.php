<?php include_once(__DIR__ . '/../../includes/admin/header.php'); ?>

    <header id="header">
      <div class="container">
        <div class="row ">
          <h4>
            <a href="/">Visit Website</a>
          </h4>
          <div class="col-md-8">
            <h1 class="text-center">Blog Posts</h1>
          </div>
          <?php include_once(__DIR__ . '/../../includes/admin/logout.php'); ?>
        </div>
      </div>
    </header>

    <section id="main">
      <div class="container">
        <div class="row">

          <?php include_once( __DIR__ . '/../../includes/admin/dashboard.php'); ?>

          <div class="col-md-9">
            <!-- Website Overview -->
            <div class="panel panel-default">
            <div class="panel-heading main-color-bg">
                <h3 class="panel-title">Posts</h3>
              </div>
              <div class="panel-body">

                <table class="table table-striped table-hover edit-posts-panel">
                      <tr>
                        <th>Title</th>
                        <th>Created</th>
                        <th>Edit/Delete</th>
                      </tr>

                      <?php foreach($data as $value) : ?>
                      <tr>

                        <td><?php echo $value->Blog_Title_En; ?></td>
                        <td><?php echo $value->Blog_Date; ?></td>
                        <td>

                          <form method="POST">
                            <a class="btn btn-outline-warning" href="<?php echo 'edit_post?id='.$value->id ?>">Edit</a>
                            <button type="button" class="btn btn-outline-danger delete" name="blog-delete">Delete</button>
                            <input type="hidden" name="blog_id" value="<?php echo $value->id ?>">
                          </form>

                        </td>

                      </tr>
                      <?php endforeach; ?>

                    </table>

                    <a href="edit_post" class="btn btn-outline-success post" name="add_post">Add New Post</a>
              </div>
              </div>

          </div>
        </div>
      </div>
    </section>

<?php include_once(__DIR__ . '/../../includes/admin/footer.php'); ?>
<script src="/js/posts.js"></script>
