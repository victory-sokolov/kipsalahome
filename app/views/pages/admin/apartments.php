<?php include_once(__DIR__ . '/../../includes/admin/header.php'); ?>


    <header id="header">
        <div class="container">
            <div class="row">
                <h4>
                    <a href="/">Visit Website</a>
                </h4>
                <div class="col-md-8">
                    <h1 class="text-center">Apartment</h1>
                </div>
                <?php include_once(__DIR__ . '/../../includes/admin/logout.php'); ?>
            </div>
        </div>
    </header>


    <section id="main">
        <div class="container">
            <div class="row">

                <?php include_once( __DIR__ . '/../../includes/admin/dashboard.php'); ?>

                    <div class="col-md-9">
                        <div class="panel panel-default">
                            <div class="panel-heading main-color-bg">
                                <h3 class="panel-title">Apartment</h3>
                            </div>
                            <div class="panel-body">

                                <br>
                                <table class="table table-striped table-hover">
                                    <tr>
                                        <th>Apartment Title</th>
                                        <th>Project</th>
                                        <th>Created</th>
                                        <th>Edit/Delete</th>
                                    </tr>

                                    <?php foreach($data['Appartment_short'] as $value): ?>
                                    <tr>
                                        <td><?php echo $value->title ?></td>
                                        <td><?php echo $value->project ?></td>
                                        <td><?php echo $value->date?></td>
                                        <td>
                                            <a class="btn btn-outline-warning" href="<?php echo 'edit_apartment?id='.$value->id ?>">Edit</a>
                                            <button type="button" class="btn btn-outline-danger delete" name="appartment-delete">Delete</button>
                                            <input type="hidden" name="apartment_id" value="<?php echo $value->id ?>">
                                        </td>

                                    </tr>
                                    <?php endforeach; ?>

                                </table>

                                <a href="add_apartment" class="btn btn-outline-success apartment" name="add_apartment">Add New Apartment</a>

                            </div>
                        </div>

                    </div>
            </div>
        </div>
    </section>
</div>
<?php include_once(__DIR__ . '/../../includes/admin/footer.php'); ?>