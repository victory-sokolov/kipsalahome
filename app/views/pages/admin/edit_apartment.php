<?php include_once(__DIR__ . '/../../includes/admin/header.php'); ?>

    <header id="header">
        <div class="container">
            <div class="row">
                <h4>
                    <a href="/">Visit Website</a>
                </h4>
                <div class="col-md-8">
                    <h1 class="text-center">Edit Apartment</h1>
                </div>
                <?php include_once(__DIR__ . '/../../includes/admin/logout.php'); ?>
            </div>
        </div>
    </header>

    <section id="main">
        <div class="container">
            <div class="row">

                <?php include_once( __DIR__ . '/../../includes/admin/dashboard.php'); ?>

                    <div class="col-md-9">
                        <div class="panel panel-default">
                            <div class="panel-heading main-color-bg">
                                <h3 class="panel-title">Edit Apartment</h3>
                            </div>

                            <div class="panel-body">

                                <form action="<?php URLROOT . "app/views/classes/PropertyData.php"?>" method="POST" enctype="multipart/form-data">

                                    <div class="form-group apartment-form">
                                        <div class="row">
                                            <?php include_once( __DIR__ . '/../../includes/admin/languages.php'); ?>
                                            <?php include_once( __DIR__ . '/../../includes/admin/project.php'); ?>
                                            <?php require_once( __DIR__ . '/../../includes/admin/property_type.php'); ?>
                                        </div>
                                    </div>

                                    <div class="panel panel-default">

                                        <div class="panel-heading slider-heading main-color-bg">
                                            <h3 class="panel-title">Slider Gallery</h3>
                                        </div>

                                        <div class="container p-y-1">
                                            <div class="row m-b-1">
                                                <div class="col-sm-6 offset-sm-3">
                                                    <div class="form-group inputDnD project-image-upload">
                                                        <label class="sr-only" for="inputFile">File Upload</label>
                                                        <input type="file" name="fileToUpload" class="form-control-file font-weight-bold" id="inputFile" accept="image/*" onchange="loadFile(event, 'apartment-gallery')" data-title="Drag and drop a file">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    <!-- Gallery Display -->
                                    <div class="apartment-gallery">
                                        <div class="container">
                                            <div class="row"></div>
                                        </div>
                                    </div>

                                    <div class="panel-heading main-color-bg">
                                        <h3 class="panel-title">Property details</h3>
                                    </div>

                                    <div class="panel-body property-details"></div>


                                    <div class="panel-heading main-color-bg">
                                        <h3 class="panel-title">Features</h3>
                                    </div>

                                    <div class="form-group features-group">
                                        <div class="row"></div>
                                    </div>
                                    <hr>
                                    <input type="submit" name="update_apartment" class="btn btn-outline-success col-md-10 offset-sm-1" value="Update">

                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


   <?php include_once( __DIR__ . '/../../includes/admin/footer.php'); ?>
   <script src="/libs/ckeditor5/ckeditor.js"></script>

    <script>

        ClassicEditor
        .create( document.querySelector( '#editor' ) )
            .then( editor => {
                console.log( editor );
            })
            .catch( error => {
                console.error( error );
        });


    </script>


<script src="/js/appartments.js"></script>




