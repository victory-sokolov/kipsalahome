<?php include_once(__DIR__ . '/../../includes/admin/header.php'); ?>

    <header id="header">
      <div class="container">
        <div class="row">
          <h4>
            <a href="/">Visit Website</a>
          </h4>
          <div class="col-md-8">
            <h1 class="text-center"> Contact Us</h1>
          </div>
          <?php include_once(__DIR__ . '/../../includes/admin/logout.php'); ?>
        </div>
      </div>
    </header>

     <section id="main">
      <div class="container">
        <div class="row">

          <?php include_once( __DIR__ . '/../../includes/admin/dashboard.php'); ?>

          <div class="col-md-9">
            <!-- Website Overview -->
            <div class="panel panel-default">
              <div class="panel-heading main-color-bg">
                <h3 class="panel-title">Contact Us</h3>
              </div>
              <div class="panel-body">

                <form method="POST">

                <?php foreach($data as $contact): ?>
                  <div class="form-group">
                    <label>Location</label>
                    <input type="text" name="address" class="form-control" placeholder="Location" value="<?php echo $contact->Contact_Address ?>">
                  </div>

                  <div class="form-group">
                    <label>Phone</label>
                    <input type="text" name="phone" class="form-control" placeholder="Phone" value="<?php echo $contact->Contact_Phone ?>">
                  </div>

                  <div class="form-group">
                      <label>Mobile Phone</label>
                      <input type="text" name="mobile" class="form-control" placeholder="Mobile Phone" value="<?php echo $contact->Contact_Mobilephone ?>">
                  </div>

                  <div class="form-group">
                      <label>Email</label>
                      <input type="email" name="email" class="form-control" placeholder="Email" value="<?php echo $contact->Contact_Email ?>">
                  </div>

                  <input type="submit" class="btn btn-outline-success" value="Save">
                  <?php endforeach; ?>
                </form>
              </div>
          </div>
        </div>
      </div>
    </section>

    <?php include_once( __DIR__ . '/../../includes/admin/footer.php'); ?>
