<?php include_once(__DIR__ . '/../../includes/admin/header.php'); ?>

    <header id="header">
        <div class="container">
            <div class="row">
                <h4>
                    <a href="/">Visit Website</a>
                </h4>
                <div class="col-md-8">
                    <h1 class="text-center"> Post Page</h1>
                </div>
                <?php include_once(__DIR__ . '/../../includes/admin/logout.php'); ?>
            </div>
        </div>
    </header>

    <section id="main">
        <div class="container">
            <div class="row">

                <?php include_once( __DIR__ . '/../../includes/admin/dashboard.php'); ?>

                    <div class="col-md-9">
                        <!-- Website Overview -->
                        <div class="panel panel-default">
                            <div class="panel-heading main-color-bg">
                                <h3 class="panel-title">Blog Post</h3>
                            </div>
                            <div class="panel-body">

                                <?php include_once( __DIR__ . '/../../includes/admin/languages.php'); ?>

                                <form action="<?php URLROOT . "app/models/AdminEditPost.php"?>" method="POST" class="postData" enctype="multipart/form-data">

                                    <div class="form-group">
                                        <label>Post Title</label>
                                        <input type="text" name="post-title" class="form-control" placeholder="Post Title">
                                    </div>

                                    <div class="form-group">
                                        <label>Post content</label>
                                        <textarea id="editor" name="content" class="form-control" placeholder="Post content"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label>Meta Description</label>
                                        <input type="text" name="meta-description" class="form-control" placeholder="Meta Description" value="">
                                    </div>

                                     <div class="panel-heading slider-heading main-color-bg">
                                        <h3 class="panel-title">Blog Image</h3>
                                    </div>

                                    <div class="container p-y-1">
                                        <div class="row m-b-1">
                                            <div class="col-sm-6 offset-sm-3">
                                                <div class="form-group inputDnD project-image-upload">
                                                    <label class="sr-only" for="inputFile">File Upload</label>
                                                    <input type="file" name="fileToUpload" class="form-control-file font-weight-bold" id="inputFile" accept="image/*" onchange="loadFile(event, 'blog')" data-title="Drag and drop a file">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <hr>

                                    <div class="container">
                                        <div class="blog">
                                            <div class="row"></div>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-8 mx-auto pb-5">
                                        <label>Image Title</label>
                                        <input type="text" name="image-title" class="form-control" placeholder="Add Image Title">
                                    </div>

                                    <div class="form-group text-center">
                                        <input type="hidden" name="post-id" value="">
                                        <input type="submit" class="btn btn-outline-success col-md-6 blog-state" name="" value="Submit">
                                    </div>

                                </form>

                            </div>

                        </div>
                    </div>
            </div>
        </div>
    </section>


<?php include_once( __DIR__ . '/../../includes/admin/footer.php'); ?>

<script>
        ClassicEditor
        .create( document.querySelector( '#editor' ) )
            .then( editor => {
                theEditor = editor;
            })
            .catch( error => {
                console.error( error );
        });
</script>

<script src="/js/posts.js"></script>




