<?php include_once(__DIR__ . '/../../includes/admin/header.php'); ?>

  <header id="header">
    <div class="container">
      <div class="row">
      <div class="col-md-2">
          <h4>
            <a href="/">Visit Website</a>
          </h4>
        </div>
        <div class="col-md-8">
          <h1 class="text-center">Apartment</h1>
        </div>
        <?php include_once(__DIR__ . '/../../includes/admin/logout.php'); ?>
      </div>
    </div>
  </header>

  <section id="main">
    <div class="container">
      <div class="row">

        <?php include_once( __DIR__ . '/../../includes/admin/dashboard.php'); ?>

        <div class="col-md-9">

          <div class="panel panel-default">
            <div class="panel-heading main-color-bg">
              <h3 class="panel-title">Add Apartment</h3>
            </div>
            <div class="panel-body">

              <form method="POST" action="<?php URLROOT . "app/models/AddApartment.php"?>" enctype="multipart/form-data">

                <div class="row">

                  <?php include_once( __DIR__ . '/../../includes/admin/languages.php'); ?>

                  <?php require_once( __DIR__ . '/../../includes/admin/project.php'); ?>

                  <?php require_once( __DIR__ . '/../../includes/admin/property_type.php'); ?>

                </div>


                <div class="row">

                  <div class="form-group col-md-4">
                    <label>Apartment Title</label>
                    <input type="text" name="apartment_title" class="form-control"  placeholder="Apartment Title" require/>
                  </div>

                  <div class="form-group col-md-4">
                    <label>Address</label>
                    <input type="text" name="apartment_address" class="form-control" placeholder="Address" require />
                  </div>

                  <div class="form-group col-md-4">
                    <label>Price</label>
                    <input type="text" name="apartment_price" class="form-control" placeholder="Price" require />
                  </div>

                </div>


                <div class="form-group">
                  <label>Apartment Description</label>
                  <textarea id="editor" name="content" class="form-control" placeholder="Apartment Description"></textarea>
                </div>

                <div class="form-group">
                  <label>Meta Description</label>
                  <input type="text" name="meta_description" class="form-control" placeholder="Meta Description" require/>
                </div>

                <div class="form-group">
                  <label>360 Apartment View URL</label>
                  <input type="text" name="apartment_view" class="form-control" placeholder="360 Apartment View URL" require />
                </div>

                <div class="panel-heading main-color-bg">
                  <h3 class="panel-title">Slider Gallery</h3>
                </div>

              <div class="container p-y-1">
                <div class="row m-b-1">
                    <div class="col-sm-6 offset-sm-3">
                        <div class="form-group inputDnD project-image-upload">
                            <label class="sr-only" for="inputFile">File Upload</label>
                            <input type="file" name="fileToUpload" multiple="multiple" class="form-control-file font-weight-bold" id="inputFile" accept="image/*" onchange="loadFile(event, 'apartment-gallery')" data-title="Drag and drop a file">
                        </div>
                    </div>
                </div>
              </div>

              <!-- Gallery Display -->
              <div class="container">
                <div class="apartment-gallery">
                  <div class="row"></div>
                </div>
              </div>

                <div class="panel panel-default">
                  <div class="panel-heading main-color-bg">
                    <h3 class="panel-title">Property details</h3>
                  </div>
                  <div class="panel-body">
                    <div class="row">

                        <div class="form-group col-md-6">
                          <label>Size (m<sup>2</sup>)</label>
                          <input type="number" name="size" class="form-control" placeholder="Size" require>
                        </div>

                        <div class="form-group col-md-3">
                          <label>Rooms</label>
                          <input type="text" name="rooms" class="form-control" placeholder="Rooms" require>
                        </div>

                        <div class="form-group col-md-3">
                          <label>Price</label>
                          <input type="text" name="price" class="form-control" placeholder="Price" require>
                        </div>


                      </div>

                      <div class="form-group">

                        <div class="row">

                          <div class="form-group col-md-3">
                            <label>Bathroom</label>
                            <input type="number" name="bathroom" class="form-control" placeholder="Bathroom" >
                          </div>

                          <div class="col-md-3">
                            <label>Current Floor</label>
                            <input type="text" name="floor" class="form-control" placeholder="Floor of the flat" require>
                          </div>

                        <div class="col-md-3">
                          <label>Total Floors</label>
                          <input type="text" name="total_floors" class="form-control" placeholder="Total Floors" require>
                        </div>

                        <div class="form-group col-md-3">
                          <label>Parking place</label>
                          <input type="text" name="parking" class="form-control" placeholder="Total Parking place" require>
                        </div>

                        </div><!-- row -->

                        </table>
                      </div>
                    </div>
                  </div>

                </div>

              <div class="panel-heading main-color-bg">
                <h3 class="panel-title">Features</h3>
              </div>

                <div class="form-group features-group">
                  <div class="row">

                    <div class="col-sm-4">
                      <input type="text" name="features[]" class="form-control" placeholder="Features" >
                      <input type="hidden" name="feature_id[]" value="">
                    </div>

                    <div class="col-sm-4">
                      <input type="text" name="features[]" class="form-control" placeholder="Features" require />
                      <input type="hidden" name="feature_id[]" value="">
                    </div>

                    <div class="col-sm-4">
                      <input type="text" name="features[]" class="form-control" placeholder="Features" require />
                      <input type="hidden" name="feature_id[]" value="">
                    </div>

                     <div class="col-sm-4">
                      <input type="text" name="features[]" class="form-control" placeholder="Features" require />
                      <input type="hidden" name="feature_id[]" value="">
                    </div>

                     <div class="col-sm-4">
                      <input type="text" name="features[]" class="form-control" placeholder="Features" require />
                      <input type="hidden" name="feature_id[]" value="">
                    </div>

                     <div class="col-sm-4">
                      <input type="text" name="features[]" class="form-control" placeholder="Features" require />
                      <input type="hidden" name="feature_id[]" value="">
                    </div>


                  </div>

                </div>
                <hr/>

                <input type="submit" name="add_apartment" class="btn btn-success" value="Add Apartment">

              </form>

            </div>

          </div>
        </div>
      </div>
      </div>
  </section>


    </div>
  </div>


  <?php include_once( __DIR__ . '/../../includes/admin/footer.php'); ?>
  <script src="/js/appartments.js"></script>