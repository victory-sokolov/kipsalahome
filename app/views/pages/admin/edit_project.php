<?php include_once(__DIR__ . '/../../includes/admin/header.php'); ?>

    <header id="header">
        <div class="container">
            <div class="row">
                <h4>
                    <a href="/">Visit Website</a>
                </h4>
                <div class="col-md-8">
                    <h1 class="text-center">Appartment Page</h1>
                </div>
                <?php include_once(__DIR__ . '/../../includes/admin/logout.php'); ?>
            </div>
        </div>
    </header>

    <section id="main">
        <div class="container">
            <div class="row">

                <?php include_once( __DIR__ . '/../../includes/admin/dashboard.php'); ?>

                    <div class="col-md-9">
                        <div class="panel panel-default">
                            <div class="panel-heading main-color-bg">
                                <h3 class="panel-title">Edit Page</h3>
                            </div>
                            <div class="panel-body">
                                <form>
                                    <div class="form-group">
                                        <label>Apartment Title</label>
                                        <input type="text" class="form-control" placeholder="Apartment Title" value="">
                                    </div>
                                    <div class="form-group">
                                        <label>Adress</label>
                                        <input type="text" class="form-control" placeholder="Adress" value="">
                                    </div>

                                    <div class="form-group">

                                        <label>Page Body</label>
                                        <textarea name="editor1" class="form-control" placeholder="Page Body">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</textarea>

                                    </div>

                                    <div class="panel panel-default">
                                        <div class="panel-heading main-color-bg">
                                            <h3 class="panel-title"> 360 Apartment  View</h3>
                                        </div>
                                    </div>

                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroup-sizing-default">Add Link</span>
                                        </div>
                                        <input type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                                    </div>

                                    <div class="panel panel-default">
                                        <div class="panel-heading main-color-bg">
                                            <h3 class="panel-title">Property details</h3>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <form>

                                                        <div class="form-group">
                                                            <label>Size</label>
                                                            <input type="text" class="form-control" placeholder="Enter size">
                                                        </div>

                                                        <div class="form-group">
                                                            <label>Adress</label>
                                                            <input type="text" class="form-control" placeholder="Enter adress of ">
                                                        </div>

                                                        <div class="form-group">
                                                            <label>Price</label>
                                                            <input type="text" class="form-control" placeholder="Enter price">
                                                        </div>

                                                    </form>

                                                </div>

                                                <div class="col-md-6">
                                                    <form>

                                                        <div class="form-group">
                                                            <label>Bathroom</label>
                                                            <input type="number" class="form-control" placeholder="Bathroom count">
                                                        </div>

                                                        <div class="form-group">
                                                            <label>Floor</label>
                                                            <input type="text" class="form-control" placeholder="Floor of the flat">
                                                        </div>

                                                        <div class="form-group">
                                                            <label>Parking place</label>
                                                            <input type="text" class="form-control" placeholder="Number of Parking places">
                                                        </div>
                                                    </form>

                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label>Features</label>
                                        <input type="text" class="form-control" placeholder="Features" value="About">
                                    </div>

                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" checked> Published
                                        </label>
                                    </div>

                                    <div class="form-group">
                                        <label>Meta Tags</label>
                                        <input type="text" class="form-control" placeholder="Add Some Tags..." value="tag1, tag2">
                                    </div>

                                    <div class="form-group">
                                        <label>Meta Description</label>
                                        <input type="text" class="form-control" placeholder="Add Meta Description..." value="  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et ">
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleFormControlFile1">Add picture</label>
                                        <input type="file" class="form-control-efil" id="exampleFormControlFile1">
                                    </div>

                                    <div class="card card-body bg-light image-preview col-md-8 mx-auto">

                                    </div>

                                    <input type="submit" class="btn btn-default" value="Submit">
                                </form>

                            </div>

                        </div>
                    </div>
            </div>
    </div>
</section>


<?php include_once(__DIR__ . '/../../includes/admin/footer.php'); ?>