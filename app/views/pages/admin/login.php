<?php include_once(__DIR__ . '/../../includes/admin/header.php'); ?>

<header id="header">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-12">
        <h1 class="text-center">Admin Area</h1>
      </div>
    </div>
  </div>
</header>


<section id="main">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-6 col-md-offset-4">

        <form id="login" class="well" method="POST">

            <div class="form-group">
              <label>Email Address</label>
              <input type="text" name="email" class="form-control" placeholder="Email: " required >
            </div>

            <div class="form-group">
              <label>Password</label>
              <input type="password" name="password" class="form-control" placeholder="Password: " required>
            </div>

            <div class="form-group">
              <input type="checkbox" name="remember_me" id="remember_me">
              <label for="remember_me">Remember Me</label>
            </div>

            <?php

              if( count($data) > 0) {
                echo "<div class='alert alert-danger'> " .$data[0] . "</div>";
              }

            ?>

            <button type="submit" class="btn btn-success btn-block">Login</button>

        </form>

      </div>
    </div>
  </div>
</section>


<?php include_once(__DIR__ . '/../../includes/admin/footer.php'); ?>

