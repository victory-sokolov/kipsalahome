<?php include_once(__DIR__ . '/../../includes/admin/header.php'); ?>


    <header id="header">
        <div class="container">
            <div class="row">
                <h4>
                    <a href="/">Visit Website</a>
                </h4>
                <div class="col-md-8">
                    <h1 class="text-center">Home Page</h1>
                </div>
                <?php include_once(__DIR__ . '/../../includes/admin/logout.php'); ?>
            </div>
        </div>
    </header>

    <section id="main">
        <div class="container">
            <div class="row">

              <?php include_once( __DIR__ . '/../../includes/admin/dashboard.php'); ?>

                    <div class="col-md-9">
                        <!-- Website Overview -->
                        <div class="panel panel-default">

                            <div class="panel-heading main-color-bg">
                                <h3 class="panel-title">Slider</h3>
                            </div>

                            <form action="<?php URLROOT . "app/models/AdminEditHome.php"?>" method="POST" enctype="multipart/form-data">

                            <div class="modal fade" id="EditSliderForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header text-center">
                                            <h4 class="modal-title w-100 font-weight-bold">Edit Slider</h4>

                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>

                                        <div class="modal-body mx-3">

                                            <div class="md-form mb-5">
                                                <i class="prefix grey-text"></i>
                                                <label data-error="wrong" data-success="right" for="slider_title">Title*</label>
                                                <input type="text" name="slider-title" id="slider_title" class="form-control validate" placeholder="Title">
                                            </div>


                                            <div class="form-group md-form mb-5">
                                                <label for="slider_body">Content*</label>
                                                <textarea name="slider-body" id="slider_body" cols="60" rows="5" placeholder="Content"></textarea>
                                            </div>

                                             <div class="md-form mb-5">
                                                <i class="prefix grey-text"></i>
                                                <label for="slider_alt">Image Alt Tag</label>
                                                <input type="text" name="slider-alt" id="slider_alt" class="form-control" placeholder="Alt Tag">
                                            </div>

                                            <div class="container p-y-1">
                                                <div class="row m-b-1">
                                                    <div class="col-sm-6 offset-sm-3">
                                                        <button type="button" class="btn btn-outline-danger btn-block" onclick="document.getElementById('inputFile').click()">Add Image</button>

                                                        <div class="form-group inputDnD">
                                                            <label class="sr-only" for="inputFile">File Upload</label>
                                                            <input type="file" name="fileToUpload" class="form-control-file font-weight-bold" id="inputFile" accept="image/*" onchange="loadFile(event, 'home-slider')" data-title="Drag and drop a file">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="image-slider-block">
                                            <img class="image-slider-preview">
                                        </div>

                                        <div class="modal-footer d-flex justify-content-center">
                                            <button class="btn btn-outline-primary" type="submit" name="imageUpload">Submit <i class="fas fa-paper-plane"></i></button>
                                            <input type="hidden" name="value-submit" value="">
                                        </div>

                                    </div>

                                </div>

                            </div>

                            </form>

                            <?php include_once( __DIR__ . '/../../includes/admin/languages.php'); ?>

                            <div class="text-center">
                                <button type="button" class="btn btn-outline-danger gallery" name="slider-upload" data-toggle="modal" data-target="#EditSliderForm">Upload Image</button>
                            </div>

                            <div class="row slider"></div><!-- row slider end -->

                            <div class="panel-body">

                                <form name="about-form" action="<?php URLROOT . "app/models/AdminEditHome.php"?>" method="POST"  enctype="multipart/form-data">

                                    <div class="wrapper">

                                        <div class="panel panel-default">
                                            <div class="panel-heading main-color-bg">
                                                <h3 class="panel-title">About Us</h3>
                                            </div>
                                        </div>



                                        <div class="about-us-admin"></div>

                                        <div class="container p-y-1">
                                            <div class="row m-b-1">
                                                <div class="col-sm-6 offset-sm-4">
                                                    <div class="form-group inputDnD">
                                                        <label class="sr-only" for="inputFile">File Upload</label>
                                                        <input type="file" name="fileToUpload" class="form-control-file font-weight-bold" id="inputFile" accept="image/*" onchange="loadFile(event, 'about-us-main')" data-title="Drag and drop a file">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="container">
                                            <div class="about-us-main">
                                                <div class="row"></div>
                                            </div>
                                        </div>

                                        <div class="col-md-12 text-center">
                                            <button type="submit" class="btn btn-outline-success col-md-4" name="save_about">Save</button>
                                        </div>

                                </form>

                                <div class="panel panel-default">
                                    <div class="panel-heading main-color-bg">
                                        <h3 class="panel-title"> Our Projects</h3>
                                    </div>

                                    <div class="modal fade" id="modalContactForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">

                                            <div class="modal-content">
                                                <div class="modal-header text-center">
                                                    <h4 class="modal-title w-100 font-weight-bold">Create New Project</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body mx-3">

                                                    <div class="md-form mb-5">
                                                        <label data-error="wrong" data-success="right" for="form34">Title</label>
                                                        <input type="text" id="form34" class="form-control validate">
                                                    </div>

                                                    <div class="md-form">
                                                        <label data-error="wrong" data-success="right" for="form8">Description </label>
                                                        <textarea type="text" id="form8" class="md-textarea form-control" rows="4"></textarea>
                                                    </div>

                                                </div>

                                                <div class="container p-y-1">
                                                    <div class="row m-b-1">
                                                        <div class="col-sm-6 offset-sm-3">
                                                            <button type="button" class="btn btn-outline-danger btn-block" onclick="document.getElementById('inputFile').click()">Add Image</button>
                                                            <div class="form-group inputDnD project-image-upload">
                                                                <label class="sr-only" for="inputFile">File Upload</label>
                                                                <input type="file" class="form-control-file font-weight-bold" id="inputFile" accept="image/*" onchange="loadFile(event, 'project')" data-title="Drag and drop a file">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="image-slider-block">
                                                    <img class="image-project-preview ">
                                                </div>

                                                <div class="modal-footer d-flex justify-content-center">
                                                    <button type="button" class="btn btn-unique" name="project_type" >Submit <i class="fas fa-paper-plane"></i></button>
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                 </div>

                             </div>
                        </div>

                        <!-- </form> -->

                    </div>

                <!--<div class="text-center">-->
                <!--    <button type="button" class="btn btn-danger gallery" name="project-upload" data-toggle="modal" data-target="#EditSliderForm">Add Project</button>-->
                <!--</div>-->

                <div class="row project-gallery"></div>

            </div>
        </div>
    </div>

</section>


<?php include_once( __DIR__ . '/../../includes/admin/footer.php'); ?>
