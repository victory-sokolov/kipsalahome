<?php include_once(__DIR__ . '/../../includes/admin/header.php'); ?>

    <header id="header">
        <div class="container">
            <div class="row">
                <h4>
                    <a href="/">Visit Website</a>
                </h4>
                <div class="col-md-8">
                    <h1 class="text-center">Site Pages</h1>
                </div>
                <?php include_once(__DIR__ . '/../../includes/admin/logout.php'); ?>
            </div>
        </div>
    </header>

    <section id="main">
        <div class="container">

            <div class="row">

                <?php include_once( __DIR__ . '/../../includes/admin/dashboard.php'); ?>


                    <div class="col-md-9">
                        <!-- Website Overview -->
                        <div class="panel panel-default">
                            <div class="panel-heading main-color-bg">
                                <h3 class="panel-title">Pages</h3>
                            </div>
                            <div class="panel-body">

                                <table class="table table-striped table-hover">

                                    <tr>
                                        <th>Page</th>
                                        <th>Edit</th>
                                    </tr>


                                    <tr>
                                        <td>Home</td>
                                        <td><a class="btn btn-default <?php active("edit_home"); ?>" href="edit_home">Edit</a></td>
                                    </tr>
                                    <tr>
                                        <td>About</td>
                                        <td><a class="btn btn-default" href="edit_about">Edit</a></td>
                                    </tr>

                                    <tr>
                                        <td>Contact</td>
                                        <td><a class="btn btn-default" href="edit_contact">Edit</a></td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                    </div>
            </div>
        </div>
    </section>


        <!-- Modals -->
        <div class="modal fade" id="addPage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form>
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Add Page</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Page Title</label>
                                <input type="text" class="form-control" placeholder="Page Title">
                            </div>
                            <div class="form-group">
                                <label>Page Body</label>
                                <textarea name="editor1" class="form-control" placeholder="Page Body"></textarea>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox"> Published
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Meta Tags</label>
                                <input type="text" class="form-control" placeholder="Add Some Tags...">
                            </div>
                            <div class="form-group">
                                <label>Meta Description</label>
                                <input type="text" class="form-control" placeholder="Add Meta Description...">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

<?php include_once(__DIR__ . '/../../includes/admin/footer.php'); ?>