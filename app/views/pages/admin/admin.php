<?php include_once(__DIR__ . '/../../includes/admin/header.php'); ?>

<header id="header">
    <div class="container">
      <div class="row">
        <div class="col-md-2">
          <h4>
            <a href="/">Visit Website</a>
          </h4>
        </div>
        <div class="col-md-8">
          <h1 class="text-center">Dashboard</h1>
        </div>
        
        <?php include_once(__DIR__ . '/../../includes/admin/logout.php'); ?>
        </div>

    </div>
</header>

    <section id="main">
      <div class="container">
        <div class="row">

          <?php include_once(__DIR__ . '/../../includes/admin/dashboard.php'); ?>

          <div class="col-md-9">
            <!-- Website Overview -->
            <div class="panel panel-default">
              <div class="panel-heading main-color-bg">
                <h3 class="panel-title">Website Overview</h3>
              </div>

              <div class="panel-body">
                <div class="row">
                  <div class="col-md-3">
                      <div class="card card-body bg-light">
                        <h3><span class="glyphicon glyphicon-user" aria-hidden="true"></span> 203</h3>
                        <h4>Users</h4>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="card card-body bg-light">
                        <h3><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
                            <?php echo $dashboard->apartmentCount(); ?>
                        </h3>
                        <h4>Apartments</h4>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="card card-body bg-light">
                        <h3><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                            <?php echo $dashboard->postsCount(); ?>
                        </h3>
                        <h4>Posts</h4>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="card card-body bg-light">
                        <h3><span class="glyphicon glyphicon-stats" aria-hidden="true"></span> 12,334</h3>
                        <h4>Visitors</h4>
                      </div>
                    </div>
                </div>

              </div>
            </div>


          </div>
        </div>
      </div>
    </section>


<?php include_once(__DIR__ . '/../../includes/admin/footer.php'); ?>