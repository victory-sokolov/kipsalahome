<?php

    require_once(__DIR__ . '/../../controllers/Contact.php');
    require_once(__DIR__ . '/../includes/header.php');
    require_once(__DIR__ . '/../includes/meta.php');

    meta($data['MetaDescription']->meta);

    require_once(__DIR__ . '/../includes/navigation.php');

?>

<div class="container page-wrapper appartment-page-wrapper">

<div class="appartment-page-section">
    <div class="row">

    <?php require_once __DIR__ . '/../includes/breadcrumbs.php'; ?>

    <?php foreach($data['Appartment_info'] as $value): ?>
        <div class="property-header section-styles">
           
            <div class="row">
                
            <div class="col-md-8">
                    
              <div class="left-property-sect">
                <h1><?php echo $value->Project_Name_en; ?>, <?php echo $value->title; ?></h1>

                <p>
                    <i class="fas fa-map-marker-alt"></i>
                    <strong>
                    <?php echo $value->Appartment_address; ?>
                </strong></p>
            </div>
            
         </div>

            <div class="col-md-4 price-section-wrapper">
              
                <div class="price-section-type">
                    <h4>
                        <?php 
                            $propertyType = ($value->Property_Type === "For sale") ? LANG::searchFilters_forSale : LANG::searchFilters_forRent;
                            echo $propertyType;
                        ?>
                    </h4>
                </div>
                
                <div class="price-section-div">
                    <?php  if($value->Property_Type == "For sale") : ?>
                        <h4>
                            € <?php echo number_format($value->Appartment_Price); ?></h4>
                    <?php else: ?>
                        <h4>
                            <?php echo "€ ".number_format($value->Appartment_Price). " / " . LANG::apartment_month?></h4>
                    <?php endif; ?>
                </div>
                    
                </div>
            
        </div>



            <div class="col-md-12">
                <div class="property-slider">

                    <div id='carousel-custom' class='carousel slide' data-ride='carousel'>

                        <div class='carousel-outer'>
                            <!-- Wrapper for slides -->

                            <div class='carousel-inner'>
                                <?php foreach($data['Appartment_slider'] as $slide) : ?>
                                    <div class='carousel-item'>
                                        <img src=<?php echo '/images/appartment/'.$slide->image ?> alt=<?php echo $slide->alt ?>>
                                    </div>
                                <?php endforeach; ?>
                            </div>

                            <a class="carousel-control-prev" href="#carousel-custom" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carousel-custom" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>

                        </div>


                        <!-- Indicators -->
                        <ol class='carousel-indicators mCustomScrollbar'>

                            <?php
                                $i = 0;
                                foreach($data['Appartment_slider'] as $slide) :
                            ?>
                            <li data-target='#carousel-custom' data-slide-to=<?php echo $i; ?> class='active'>
                                <img src=<?php echo "/images/appartment/".$slide->image ?> alt=<?php echo $slide->alt ?>>
                            </li>

                            <?php $i++; endforeach; ?>
                        </ol>

                    </div>

                </div>

            </div>
            </div>
    </div>

</div>
<!-- End Appartment Page Section -->


<!-- Property Details Section -->
<section class="property-description-section">

    <div class="row">
    <?php if(!empty($data['Apartment_Description'][0])) : ?>
        <div class="col-md-12">
            <div class="property-description section-styles">
                <h1 class="text-center"><?php echo LANG::apartment_property ?></h1>

                <div class="property-content">
                    <div class="row">

                        <?php foreach($data['Apartment_Description'] as $descr ): ?>
                            <div class="col-md-6">
                                <p>
                                    <?php echo $descr; ?>
                                </p>
                            </div>

                        <?php endforeach; ?>
                    </div>

                <?php if(!empty($value->Appartment_View)): ?>
                <div class="col-md-12">
                    <section class="apartment-view">
                        <iframe src="<?php echo $value->Appartment_View; ?>" height="340px" width="100%" frameborder="0" title="Apartment View"></iframe>
                    </section>
                </div>
               <?php endif; ?>

              </div>

            </div>

        </div>
        <?php endif; ?>

    <?php require_once(__DIR__ . '/../includes/contact-form.php'); ?>

    </div>

</section>

<?php if(!empty($value->PD_Size)) : ?>

<div class="row">
    <div class="col-md-12">
    <!-- Property Details -->
    <section class="property-details-section">

        <div class="col-md-12 col section-styles">

            <h1 class="text-center "><?php echo LANG::apartment_propertyDetails?></h1>

                <ul class="list-group">
                    <div class="row">
                        <div class="col-md-6">

                            <li class="list-group-item">
                                <i class="far fa-square"></i>
                                <span><?php echo LANG::apartment_size ?>:</span>
                                <span><strong>
                                    <?php echo $value->PD_Size; ?>m<sup>2</sup></sup>
                                </strong></span>
                            </li>

                            <li class="list-group-item">
                                <i class="fas fa-bed"></i>
                                <span><?php echo LANG::apartment_rooms ?>: </span>
                                <span><strong> <?php echo $value->PD_Rooms; ?> </strong></span>
                            </li>

                            <li class="list-group-item">
                                <i class="far fa-money-bill-alt"></i>
                                <span><?php echo LANG::apartment_price ?>: </span>
                                <span><strong>
                                    <?php echo $value->PD_PriceUnit; ?> &euro; / m<sup>2</sup>
                                </strong></span>
                            </li>
                        </div>

                    <div class="col-md-6">
                        <li class="list-group-item">
                            <i class="fas fa-bath"></i>
                            <span><?php echo LANG::apartment_bathrooms ?>:</span>
                            <span><strong>
                                <?php echo $value->PD_Bathrooms; ?>
                            </strong></span>
                        </li>

                        <li class="list-group-item">
                            <img src="/images/stairs-with-handrail.svg" alt="stairs-icon">
                            <span><?php echo LANG::apartment_floor ?>:</span>
                            <span><strong>
                                <?php echo $value->PD_AptFloor ."/". $value->PD_Floor ?>
                            </strong></span>
                        </li>

                        <li class="list-group-item">
                            <img src="/images/parking-solid.svg" alt="parking-icon">
                            <span><?php echo LANG::apartment_parking ?>:</span>
                            <span><strong>
                                <?php echo $value->PD_Parking; ?>
                            </strong></span>
                        </li>
                    </div>
                </div>
            </ul>
        </div>
    </div>
</div>
<?php endif; ?>

</section>
<!-- Property Details End -->

<?php endforeach; ?>

<!-- Features Section -->

<?php  

function non_empty($db_obj) {
    $non_empty = 0;
    
    foreach($db_obj as $value) {
        if(!empty($value->features)) {
            $non_empty++;
        }
    }
    return $non_empty;
}

if(non_empty($data['Appartment_features']) > 0): ?>

<section class="features-section">

    <div class="col-md-12 section-styles">

        <h1 class="text-center"><?php echo LANG::apartment_features; ?> </h1>

            <div class="features-wrapper">

                <div class="custom-control custom-checkbox my-1 mr-sm-2">

                <div class="row">
                        <?php
                            foreach($data['Appartment_features'] as $value) :
                                if(!empty($value->features)) :
                        ?>

                        <div class="col-sm-6">
                            <input type="checkbox" class="custom-control-input" id="customControlInline" checked>
                            <label class="custom-control-label" for="customControlInline">
                                <?php echo $value->features; ?>
                            </label>
                        </div>
                    <?php endif; ?>
                    <?php endforeach; ?>

                </div>

            </div>

        </div>

    </div>

</section>
<?php endif; ?>

<!-- Features Section End -->
</div> <!-- Container -->

<script>
// Prevent form resubmission
if ( window.history.replaceState ) {
	window.history.replaceState( null, null, window.location.href );
}

</script>

<?php require_once('../app/views/includes/footer.php'); ?>




