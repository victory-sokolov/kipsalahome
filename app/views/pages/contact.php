<?php

    require_once(__DIR__ . '/../../controllers/Contact.php');
    require_once(__DIR__ . '/../includes/header.php');
    require_once(__DIR__ . '/../includes/meta.php');

    //call meta description function
    meta("Kipsalahome Contact Page");

    require_once(__DIR__ . '/../includes/navigation.php');

?>

<div class="contact-page-wrapper page-wrapper">
    <div class="contact-image-header">
        <img src="/../public/images/contact-us-3.jpg" alt="About Us">
    </div>
<div class="container-fluid">

    <h1 class="heading-text"><?php echo LANG::menu_contactUs; ?></h1>

    <div class="contact-section">

        <div class="row">

            <div class="col-md-8 col-xs-12">
            <?php require_once __DIR__ . '/../includes/breadcrumbs.php'; ?>
                <div class="location-map-wrapper">
                    <div id="map"> 
                        <div class="google-maps">
	                        <iframe class="google-maps-iframe" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3658.533979422594!2d24.078679012205555!3d56.95946893592435!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46eecfe5dfaeeba1%3A0x4539cddcbf5bcc65!2sZvejnieku+iela+24%2C+Kurzemes+rajons%2C+R%C4%ABga%2C+LV-1048%2C+Latvia!5e0!3m2!1sen!2sus!4v1447182881956" width="auto" height="auto" frameborder="0" style="border:0">
	                        </iframe>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-4 col-xs-12">
                
                <div class="contact-information">

                    <!--<h1 class="text-center text-style">-->
                    <!--    <?php echo LANG::menu_contactUs; ?>-->
                    <!--</h1>-->

                    <ul class="contact-details">
                    <?php foreach($data as $contactVal) : ?>

                        <li><i class="fas fa-map-marker-alt"></i>
                            <?php echo LANG::contact_location; ?>: <?php echo $contactVal->Contact_Address; ?>
                        </li>

                        <li><i class="fas fa-phone"></i>
                            <?php echo LANG::contact_phone; ?>: <?php echo $contactVal->Contact_Phone;?>
                        </li>

                        <li><i class="fas fa-mobile-alt"></i>
                            <?php echo LANG::contact_mobilePhone; ?>: <?php echo $contactVal->Contact_Mobilephone;?>
                        </li>


                        <li><i class="far fa-envelope"></i>
                            <?php echo LANG::placeholder_email; ?>: <?php echo $contactVal->Contact_Email;?>
                        </li>
                    </ul>
                    <?php endforeach; ?>
                </div>

                <div class="static-contact-form cf-pos">

                    <form action="<?php URLROOT . '/../../app/controllers/Contact.php' ?>" method="POST" class="contact-form">

                        <div class="form-group group">
                            <label for=""></label>
                            <input type="text" class="form-control inputMaterial" id="contact-name" name="contactName" autocomplete="off" required>
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label for="contact-name"><?php echo LANG::placeholder_name; ?> *</label>
                        </div>

                        <div class="form-group group">
                            <input type="number" class="form-control inputMaterial" id="phone-number" name="contactNumber" autocomplete="off">
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label for="phone-number">
                                <?php echo LANG::placeholder_phone; ?>
                            </label>
                        </div>

                        <div class="form-group group">
                            <input type="email" class="form-control inputMaterial" id="contact-email" name="contactEmail" autocomplete="off" required>
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label for="contact-email">
                                <?php echo LANG::placeholder_email; ?> *</label>
                        </div>

                        <div class="form-group group penult-form">
                            <textarea cols="10" rows="2" class="form-control inputMaterial" id="message" name="contactMessage" required></textarea>
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label for="message">
                                <?php echo LANG::placeholder_message; ?> *</label>
                        </div>

                        <div class="button-group group">
                            <button type="submit" class="btn btn-outline-primary submit-form col-sm-4 send-btn" name="send-message">
                                <?php echo LANG::buttons_send; ?>
                            </button>
                            
                            <a href="tel:<?php echo $contactVal->Contact_Phone;?>" class="btn btn-outline-success col-sm-4 offset-sm-1 phone-btn">
                                <i class="fas fa-phone"></i>
                                <?php echo $contactVal->Contact_Mobilephone;?>
                            </a>
                        </div>



                    </form>

                    <?php
                        if(isset($_POST['send-message'])) {
                            
                            echo $this->successMessage();
                            unset($_SESSION['msg']);
                        }
                    ?>
                </div>

            </div>

    </div>
    </div>
</div>

</div>


<script>

    // Prevent form resubmission
    if ( window.history.replaceState ) {
    	window.history.replaceState( null, null, window.location.href );
    }

    
</script>

<?php require_once('../app/views/includes/footer.php'); ?>