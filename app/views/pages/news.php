<?php

    require_once(__DIR__ . '/../includes/header.php');
    require_once(__DIR__ . '/../includes/meta.php');
    require_once('PostClass.php');
    meta($data['MetaDescription']->meta);

    require_once(__DIR__ . '/../includes/navigation.php');

    if(isset($_SESSION['lang'])) {
        $currentLang = strtolower($_SESSION['lang']);
    }
    $clink = explode('/',$_GET['url']);

?>

<?php if(count($clink) == 1) : ?>

<div class="container-fluid">

    <section class="news-blog">

        <div class="row">
            <div class="news-header">
                <img src="/public/images/blog/about-3.jpg" alt="">
            </div>
        </div>

        <!-- Section heading -->
        <div class="container">

        <h1 class="heading-text"><?php echo LANG::news_news; ?></h1>

        <?php require_once __DIR__ . '/../includes/breadcrumbs.php'; ?>

            <?php
                foreach($data['News'] as $newsVal) :
                    $title = "Blog_Title_".$currentLang;
                    if(!empty($newsVal->$title)) :
            ?>

            <div class="row newblogpost aos-init" data-aos="fade-right" data-aos-duration="1000">

                <?php
                    $BGTL = "Blog_Title_" . $currentLang;
                    $postModel = new PostClass();
                    $uBGTL = $postModel -> url_slug($newsVal->$BGTL,array()); 
                    $link = "news/" .$uBGTL.'/'.$newsVal->id.'/'.'?lang='.$currentLang;
                    $URL=str_replace(' ', '-',$link);
                ?>

            <div class="col-lg-7">
                <!-- Featured image -->
                <div class="view overlay rounded z-depth-2 mb-lg-0 mb-4">
                    <a href="<?php echo $URL?>">
                        <img class="img-fluid" src="/public/images/blog/<?php echo $newsVal->Blog_Image; ?>" alt=<?php echo $newsVal->alt_tag ?>>
                    </a>
                </div>
            </div>

            <!-- Grid column -->
            <div class="col-lg-5">
                <a href="<?php echo $URL?>">
                    <h3 class="font-weight-bold mb-3">
                        <strong class="underline"> <?php echo $newsVal->$BGTL; ?></strong>
                    </h3>
                </a>

                <?php
                    $BGCL = "Blog_Content_" . $currentLang;

                    $shortNews = explode(' ', $newsVal->$BGCL);
                    $shortNews = array_slice($shortNews,0, 30);
                    $shortString = implode(' ', $shortNews);

                ?>

                <p><?php echo $shortString . ' ...'; ?></p>

                <!-- Read more button -->
                <a href="<?php echo $URL?>" class="btn btn-outline-primary btn-md"><?php echo LANG::buttons_read; ?></a>

            </div>
            <!-- Grid column -->

            </div>
            <!-- Grid row -->

            <hr class="my-5">
            <?php endif; ?>
        <?php endforeach; ?>

    </section>
    </div>
</div>

<?php else: ?>

<?php

    $plink = explode('/',$_GET['url']);
    if(isset($_GET['lang'])){
        $currentLang = strtolower($_GET['lang']);
    }
    else{
        $currentLang = $plink[3];
    }
    $TESTBGTL = "'{$plink[1]}'";
    $TESTBGTL= str_replace('-', ' ',$TESTBGTL);
    $postModel = new PostClass();

    $id = $plink[2];
    $newdata = $postModel->getPosts($id);
?>


<div class="container page-wrapper news-page-wrapper">
                <?php require_once __DIR__ . '/../includes/breadcrumbs.php'; ?>
    <div class="blogpost-wrapper">

        <?php foreach($newdata as $postVal) : ?>
            <div class="col-sm-12">

            <?php
                $BGTL = "Blog_Title_" . $currentLang;
                if(!empty($postVal->$BGTL)) :
            ?>

                <h1 class="text-center"><?php echo $postVal->$BGTL; ?></h1>
               
                <div class="blogpost">
    
                    <div class="top-list">
                        <div class="row">

                            <div class="col-md-2 col-xs-3">
                                <span><i class="far fa-calendar-alt"></i><?php echo $postVal->Blog_Date; ?></span>
                            </div>

                            <div class="col-md-2 col-xs-3">
                                <span><i class="fas fa-home"></i>Real Estate, Riga</span>
                            </div>

                        </div>

                    </div>


                        <?php $BGCL = "Blog_Content_" . $currentLang; ?>
                        <div class="blog-description">
                            <img src=<?php echo "/public/images/blog/". $postVal->Blog_Image; ?> alt=<?php echo $postVal->alt_tag?>>
                            <?php $BIDL = "Blog_ImageDescription_" . $currentLang; ?>
                            <strong class="text-center"><i> <?php echo $postVal->$BIDL;?> </i></strong>
                            <p><?php echo nl2br($postVal->$BGCL);?></p>
                        </div>
                    </div>

                <?php else : $currentLang = strtolower($_GET['lang']) ?>
                <div class="alert alert-grey text-center">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <h1 class="text-center ">Post Not Found</h1>
                    <p>
                        <a href=<?php echo "/news?lang=".$currentLang ?>><strong>Please visit other posts</strong></a>
                    </p>
                </div>
                <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </div>


    </div>
<?php endif;?>

<?php require_once('../app/views/includes/footer.php'); ?>