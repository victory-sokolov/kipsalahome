<?php
    require_once(__DIR__ . '/../includes/header.php');
    require_once(__DIR__ . '/../includes/navigation.php');
    require_once(__DIR__ . '/../includes/meta.php');

    meta("About KipsalaHome company");
    if(isset($_SESSION['lang'])) {
        $currentLang = strtolower($_SESSION['lang']);
    }
    $contnt = 'content_'.$currentLang; 

?>

<div class="container-fluid">

    <div class="row">

        <section class="about-header">

            <div class="about-image-header">
                <img src="/../public/images/about-us-header2.jpg" alt="About Us">
            </div>
             
            <div class="container">

                <div class="about-company">
                    <h1 class="heading-text"><?php echo LANG::aboutUs_about; ?></h1>
                    <?php require_once __DIR__ . '/../includes/breadcrumbs.php'; ?>
                    <div class="about-description">

                        <div class="angle-design angle-left"></div>
                        <div class="row">
                            <div class="col-sm-5 mr-5">
                                <p> <?php echo $data['AboutDescription']->content_left ?> </p>
                            </div>

                            <div class="col-sm-5 ml-5 right-text-column">
                                <p> <?php echo $data['AboutDescription']->content_right ?> </p>
                            </div>
                            <div class="angle-design angle-right"></div>
                        </div>
                    </div>
                </div>


                <div class="about-photos-wrapper" data-aos="fade-left" data-aos-duration="1000">

                    <div class="about_item">
                        <img src="images/about-1.jpg" alt="">

                    </div>

                    <div class="about_item">
                        <img src="images/about-2.jpg" alt="">
                    </div>

                    <div class="about_item">
                        <img src="images/about-3.jpg" alt="">
                    </div>

                </div>


                <section class="inspiration">
                    <!-- <h1 class="text-center text-style">Our Mission</h1> -->
                    <!-- <hr class="horizontal"> -->
                    <div class="our-mission" data-aos="zoom-in">
                        <div class="col-md-12">
                            <div class="row">
                                <blockquote>
                                    <!--<p><span>Lorem ipsum dolor</span>, sit amet consectetur adipisicing elit. Illum, ab?Lorem ipsum dolor, sit amet consectetur adipisicing elit. Illum, ab?</p>-->
                                    <p><span><?php echo $data['OurTeam'] -> content ?></p>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                </section>


            </div>

        </section>


        <div class="container">

            <section class="our-team">

                <h1 class="text-center text-style">
                    <?php echo LANG::aboutUs_ourTeam; ?>
                </h1>

                <div class="team-members-block">
                    <div class="row">
                        <?php foreach($data['Employees'] as $employee): ?>
                            <div class="col-sm-12 col-md-4">
                                <div class="team-member">
                                    <img src="<?php echo "images/team/".$employee->photo ?>">
                                    <div class="member-info">
                                        <p><?php echo $employee->name?></p>
                                        <p><?php echo $employee->position ?></p>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>

            </section>

            <section class="our-projects">
                <h1 class="text-center text-style"><?php echo LANG::aboutUs_ourProject; ?></h1>

                <div class="projects-wrapper"  data-aos="fade-right" data-aos-duration="1000">

                    <div class="col-md-10 portfolio-gallery-main">

                        <div class="portfolio-element left-middle-img">
                            <img src="/images/portfolio/kitchen.jpg" alt="">
                            <div class="text-overlay">
                                <p class="cmn-t-underline">Zundas Darzi</p>
                            </div>
                        </div>

                        <div class="inner-gallery">
                            <div class="portfolio-element">
                                <div class="col-md-6 portfolio-gallery-top">
                                    <img src="/images/portfolio/bedroom.jpg" alt="">
                                    <div class="text-overlay">
                                        <p class="cmn-t-underline">Zundas Darzi</p>
                                    </div>
                                </div>
                            </div>

                            <div class="portfolio-element">
                                <div class="col-md-6 portfolio-gallery-top-left">
                                    <img src="/images/portfolio/apartment-architecture-chair-892618 (1).jpeg" alt="">
                                    <div class="text-overlay">
                                        <p class="cmn-t-underline">RiverSide</p>
                                    </div>
                                </div>
                            </div>

                            <div class="portfolio-element">
                                <div class="col-md-6 portfolio-gallery-bottom">
                                    <img src="/images/portfolio/apartment.jpg" alt="">
                                    <div class="text-overlay">
                                        <p class="cmn-t-underline">RiverSide</p>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </section>



            <section class="our-partners">
                <h1 class="text-center text-style"><?php echo LANG::aboutUs_ourPartners; ?></h1>

                <div class="col-md-12">
                    <div class="row">
                        <div class="partners-wrapper">
                            <img src="/images/partners/logo1.png" alt="">
                            <img src="/images/partners/logo2.png" alt="">
                            <img src="/images/partners/logo3.png" alt="">
                            <img src="/images/partners/logo4.png" alt="">
                            <img src="/images/partners/logo5.png" alt="">
                        </div>
                    </div>

                </div>


            </section>

        </div>


    </div>
</div>


<?php require_once('../app/views/includes/footer.php'); ?>

