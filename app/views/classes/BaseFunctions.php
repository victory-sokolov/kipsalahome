<?php

session_start();

require __DIR__ . '/../../config/config.php';
require __DIR__ . '/../../libraries/Database.php';

require __DIR__ . '/../../Database/QueryBuilder.php';

class BaseFunctions
{

    public $lang;

    public function __construct()
    {
        $this->language();
        $this->lang = $_SESSION['admin_lang'];

    }


    public function language()
    {
        if(isset($_POST['language'])) {
            $_SESSION['admin_lang'] = $_POST['language'];
            return $_SESSION['admin_lang'];
        }
    }


    public function updateData()
    {

        $card_id = $_POST['edit_card'];
        $_SESSION['card_id'] = $card_id;

        $table = explode("-", $_POST['section'])[0];

        switch ($table) {
            case 'project':
                $fields = "id, Project_Name_$this->lang as title, Project_Description_$this->lang as content,Project_Image as image, image_alt as alt";
                break;
            case 'slider':
                $fields = "id, slider_title_$this->lang as title, slider_content_$this->lang as content, slider_image as image, image_alt as alt";
                break;
        }

        $sql = $this->queryBuilder->select($fields)
                    ->from($table)
                    ->where("id = :id")
                    ->sql();
        $this->db->query($sql);
        $this->db->bind(':id', $card_id);

        return $this->db->resultSet();

    }
}

$base = new BaseFunctions();