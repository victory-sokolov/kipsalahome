<?php

/*
* Get Appartment and posts count
*/

class Dashboard
{

    private $db;

    public function __construct() {

        $this->db = new \Libs\Database();
    }

    public function apartmentCount()
    {
        $this->db->query("SELECT Appartment_Title_En, Project_Name_en from appartment
                             INNER JOIN project ON appartment.Property_ID = project.id");

            return count($this->db->resultSet());
    }

    public function postsCount()
    {
        $this->db->query("SELECT Blog_Title_En, Blog_Date from blog");
        return count($this->db->resultSet());
    }
}

$dashboard = new Dashboard();