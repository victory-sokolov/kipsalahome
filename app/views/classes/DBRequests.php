<?php

class DBRequests
{
    public $db;
    public $lang;

    public function __construct()
    {
        $this->db = new \Libs\Database();
    }

    public function selectProjects()
    {
        $project = "SELECT id, Project_Name_en FROM project";

        $this->db->query($project);
        return $this->db->resultSet();
    }

    public function propertyRent()
    {
        $this->db->query("SELECT * FROM property");
        return $this->db->resultSet();
    }

    // Get property id
    public function propertyID($id)
    {
        $this->db->query("SELECT PropDet_ID FROM appartment WHERE idAppartment = :id");

        $this->db->bind(':id', $id);

        return $this->db->resultSet();

    }


    public function selectProject()
    {
        $sql = "SELECT id, Project_Name_En as name,
                        Project_Image as image,
                        image_alt as alt
                FROM project";

        $this->db->query($sql);
        return $this->db->resultSet();
    }




}
