<?php

require './BaseFunctions.php';


class Apartment extends BaseFunctions
{

    private $db;

    public $lang;

    public $id;

    public function __construct()
    {

        $this->db = new \Libs\Database;

        $this->lang = $_SESSION['admin_lang'];

        $this->returnApartmentData();

        $this->removeImage();

        //$this->employeeCardDelete();

    }


    public function PropertyDetails()
    {
        if(isset($_POST['id'])) {

            $this->id = $_POST['id'];

            $sql = "SELECT idAppartment, Appartment_Title_$this->lang as title,
                        Appartment_Description_$this->lang as content,
                        Appartment_Price as price,
                        Appartment_address as address,
                        Meta_Description as meta,
                        Appartment_View,
                        Property_Type, idProperty,id as project_id, Appartment_Images as image,
                        PD_Rooms, PD_Size, PD_PriceUnit, PD_Bathrooms, PD_Floor, PD_AptFloor, PD_Parking
                    FROM appartment
                    INNER JOIN propertydetails ON appartment.PropDet_ID = propertydetails.idPropertyDetails
                    INNER JOIN property ON appartment.Property_ID  = property.idProperty
                    INNER JOIN project ON appartment.Project_ID = project.id
                    WHERE idAppartment = :id";

            $this->db->query($sql);
            $this->db->bind(':id', $this->id);

            return $this->db->resultSet();
        }
    }

    public function selectApartmentGallery()
    {
        $sql = "SELECT * FROM apartment_gallery WHERE apartment_id = :id";
        $this->db->query($sql);

        $this->db->bind(':id', $_POST['id']);

        return $this->db->resultSet();
    }


    public function ApartmentFeatures()
    {
        $sql = "SELECT Feature_Title_$this->lang as features, idFeature FROM features
                WHERE Appartment_ID = :id";

        $this->db->query($sql);
        $this->db->bind(':id', $_POST['id']);

        return $this->db->resultSet();
    }


    public function removeImage()
    {

        if(isset($_POST['section'])) {

            $sql = null;

            $id = $_POST['removeImage'];
            $table = $_POST['section'];

            switch($_POST['section']) {
                
               
                
                case 'apartment_gallery':
                    $sql = "DELETE FROM $table WHERE id = :id";
                    break;
                case 'blog':
                    $sql = "UPDATE $table SET Blog_Image = NULL WHERE id = :id";
                    break;
                case 'main_about':
                    $sql = "UPDATE about_us_main_page SET about_us_image = NULL";
                    break;
                case 'employee':
                    $sql = 'DELETE FROM our_team WHERE team_id = :id';
                    
            }

            $this->db->query($sql);
            $this->db->bind(":id", $id);
            $this->db->execute();


        }
    }

    //Delete Employee card
    // public function employeeCardDelete()
    // {
        
    //     // if(isset($_POST['section']) && ($_POST['section'] == 'employee')) {
    

    //     //     $this->db->query('DELETE FROM our_team WHERE team_id = :id');

    //     //     $this->db->bind(":id", $_POST['removeImage']);
    //     //     $this->db->execute();

    //     //     header("Location: " . $_SERVER['REQUEST_URI']);
    //     // }
    // }


    public function returnApartmentData()
    {

        if(isset($_POST['language'])) {
            $data = [
                "PropertyDetails"   => $this->PropertyDetails(),
                "ApartmentGallery"  => $this->selectApartmentGallery(),
                "ApartmentFeatures" => $this->ApartmentFeatures()
            ];

            echo json_encode($data);
        }
    }


}

$apartment = new Apartment();