<?php


class UploadImages
{

    public static function uploadImage($dest = '')
    {

        $folder = dirname(APPROOT) . "/public/images/";

        $dest = ($dest !== null) ? ($folder . $dest . "/") : $folder;

        $fileName = basename($_FILES["fileToUpload"]["name"]);
        $fileTmpName = $_FILES["fileToUpload"]["tmp_name"];


        $fileDestination =  $dest . $fileName;


        move_uploaded_file($fileTmpName, $fileDestination);

    }
    
    

    public static function deleteImage($dest = '', $image)
    {
        $folder = dirname(APPROOT) . "/public/images/";
        $dest = ($dest !== null) ? ($folder . $dest . "/") : $folder;

        if(file_exists($dest)) {
            unlink($dest);
        }
    }

}