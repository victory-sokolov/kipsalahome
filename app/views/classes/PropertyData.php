<?php

/*
* Send data from inputs
*/

class PropertyData
{


    public function returnPropertyData()
    {
        if(isset($_POST['add_apartment']) || isset($_POST['update_apartment'])) {

            // $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $apartmentInfo = [
                ':Title'        => $_POST['apartment_title'],
                ':Content'      => $_POST['content'],
                ':Price'        => $_POST['apartment_price'],
                ':View'         => $_POST['apartment_view'],
                ':Address'      => $_POST['apartment_address'],
                ':Meta'         => (isset($_POST['meta_description']) ? $_POST['meta_description'] : ""),
                ':ProjectId'    => $_POST['select_project'],
                ':Rent'         => $_POST['rent']
            ];

            $apartmentGallery = [
                ':Image' => ($_FILES['fileToUpload']['size'] !== 0) ? basename($_FILES["fileToUpload"]["name"]) : "",
                ':Alt'   => isset($_POST['image-alt']) ? $_POST['image-alt'] : ""
            ];

            $propertyData = [
                ':Size'         => $_POST['size'],
                ':Rooms'        => $_POST['rooms'],
                ':PriceUnit'    => trim($_POST['price']),
                ':Bathrooms'    => trim($_POST['bathroom']),
                ':TotalFloors'  => trim($_POST['total_floors']),
                ':Parking'      => trim($_POST['parking']),
                ':Floor'        => trim($_POST['floor'])
            ];

            $featuresData = [
                "Features"  => $_POST['features'],
                "FeatureId" => $_POST['feature_id']
            ];

            $data = [
                "apartmentInfo"     => $apartmentInfo,
                "propertyData"      => $propertyData,
                "apartmentGallery"  => $apartmentGallery,
                "featuresData"      => $featuresData
            ];

            return $data;
        }
    }
}
