<?php


require './BaseFunctions.php';

class Post extends BaseFunctions
{

    private $db;

    public $lang;

    public function __construct()
    {

        $this->db = new \Libs\Database;

        $this->lang = $_SESSION['admin_lang'];

        $this->displayPost();

    }


    public function displayPost()
    {

       if(isset($_POST['id'])) {

            $id = $_POST['id'];

            $sql = "SELECT id, Blog_Title_$this->lang as title,
                        Blog_Content_$this->lang as content,
                        Blog_ImageDescription_$this->lang as img_descr,
                        Blog_Image as image,
                        Meta_Description,
                        alt_tag as alt
                    FROM blog WHERE id = :id";

            $this->db->query($sql);
            $this->db->bind("id", $id);

            echo json_encode($this->db->resultSet());
       }

    }


}

$post = new Post();