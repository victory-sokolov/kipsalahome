<?php

require_once './BaseFunctions.php';

class SliderClass extends BaseFunctions
{
    public $db;

    public $lang;

    public $queryBuilder;


    public function __construct()
    {

        $this->db = new \Libs\Database;

        $this->queryBuilder = new QueryBuilder();

        $this->lang = $_SESSION['admin_lang'];

        $this->returnJSONData();

        $this->editSlide();

        $this->deleteSliderCard();

    }

    public function selectAboutUsData()
    {
        $sql = $this->queryBuilder->select("id, about_title_$this->lang as title,
                                                sub_title_$this->lang as sub_title,
                                                content_$this->lang as content,
                                                about_us_image as image,
                                                alt_tag as alt
                                            ")
                                    ->from('about_us_main_page')
                                    ->sql();

        $this->db->query($sql);

        return $this->db->resultSet();

    }

    public function selectProjectData()
    {
        $sql = $this->queryBuilder->select(
                                    "id, Project_Name_$this->lang as name,
                                    Project_Description_$this->lang as content,
                                    Project_Image as image,
                                    image_alt as alt")
                                ->from("project")
                                ->sql();
        $this->db->query($sql);

        return $this->db->resultSet();

    }


    public function returnJSONData()
    {

        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        if(isset($_POST['display_home'])) {

            $data = [
                "Slider"    => $this->sliderDataSelect(),
                "About"     => $this->selectAboutUsData(),
                "Project"   => $this->selectProjectData(),
                "AboutSection" => $this->selectAbout(),
                "TeamContent"  => $this->selectTeamContent(),
                "Employee"   =>  $this->selectEmployees()
            ];

            echo json_encode($data);
        }
    }


    public function sliderDataSelect()
    {
        $sql = $this->queryBuilder->select("id, slider_title_$this->lang as title, slider_content_$this->lang as content,
                                            slider_image as image, image_alt as alt")
                    ->from("slider")
                    ->sql();

       $this->db->query($sql);

        return $this->db->resultSet();
    }


    public function editSlide()
    {
        if(isset($_POST['edit_card'])) {
           echo json_encode(parent::updateData());
        }
    }


    public function deleteSliderCard()
    {

       if(isset($_POST['delete_card'])) {

            $table = explode("-", $_POST['section'])[0];
            $card_id = $_POST['delete_card'];

            if($_POST['section'] == "appartment-delete") {

                $this->db->query("SELECT PropDet_ID FROM appartment WHERE idAppartment  = :id");
                $this->db->bind(":id", $card_id);
                # stdClass to array
                $apartmentId = json_decode(json_encode($this->db->resultSet()), true)['0']['PropDet_ID'];

                //$deleteStatement = [$card_id, $card_id, $card_id,$apartmentId];

                $deleteQuerie = "DELETE FROM features WHERE Appartment_ID = :id;
                                 DELETE FROM apartment_gallery WHERE apartment_id = :id;
                                 DELETE FROM appartment WHERE idAppartment = :id;
                                 DELETE FROM propertydetails WHERE idPropertyDetails = :id";

                $this->db->query($deleteQuerie);

                for($i = 0; $i < count($deleteQuerie-1); $i++) {
                    $this->db->bind(':id', $value);
                }
                $this->db->bind(':id', $apartmentId);
                $this->db->execute();

            }
            else {
                $delete = "DELETE FROM $table WHERE id = :id";

                $this->db->query($delete);
                $this->db->bind(':id', $card_id);
                $this->db->execute();
            }

            // Delete image locally
            //UploadImages::deleteImage($table);

       }
    }

    public function selectAbout()
    {
        $sql = "SELECT about_content_left_$this->lang as c_left, about_content_right_$this->lang as c_right FROM about WHERE id = :id";
        $this->db->query($sql);

        $this->db->bind(':id', 1);
        return $this->db->singleFetch();
    }


    public function selectTeamContent()
    {
        $sql = "SELECT content_$this->lang as content FROM team_content WHERE id = :id";
        $this->db->query($sql);

        $this->db->bind(':id', 1);
        return $this->db->singleFetch();
    }

    public function selectEmployees()
    {
        $sql = "SELECT team_id as id, employee_name_$this->lang as name, position_$this->lang as position, employee_photo as photo
                FROM our_team";
        $this->db->query($sql);
        return $this->db->resultSet();
    }


}


$slider = new SliderClass();