<?php

require '../../config/config.php';
require '../../libraries/Database.php';


class FilterData
{

    private $db;

    public $sqlStatement;
    public $filterValues = [];
    public $lang;

    public function __construct() {

        $this->db = new \Libs\Database;

        if(isset($_POST['param'])) {

            $this->filterValues = $_POST['param'];
            $this->lang = $this->filterValues['current_lang'];

            $this->sqlStatement = $this->selectAppartmentInfo();
            $this->getAJAXData();
        }

    }

    public function selectAppartmentInfo()
    {

        return "SELECT idAppartment as id, Appartment_Title_$this->lang as title, Appartment_Price, Property_Type, Property_ID,Project_ID,
                       Appartment_Images as image, PD_Rooms, PD_SIZE, PD_Floor, PD_AptFloor, Appartment_Title_En as url_title
                FROM appartment
                INNER JOIN propertydetails ON appartment.PropDet_ID = propertydetails.idPropertyDetails
                INNER JOIN property ON appartment.Property_ID  = property.idProperty
                INNER JOIN project ON appartment.Project_ID = project.id
               ";
    }


    public function getAJAXData() {

            $where = [];
            $params = [];
            
            foreach ($this->filterValues as $key => $value) {

                switch($key) {

                    case 'rooms':
                        if ($value !== '0') {
                            $where[] = "`PD_Rooms` = :roomsCount";
                            $params[':roomsCount'] = $value;
                        }
                        break;

                        case 'project_id':
                            if($value !== "All Projects") {
                                $where[] = "Project_ID  = :Project_ID";
                                $params[':Project_ID'] = $value;
                            }
                            break;


                        case 'property_type':
                            if($value !== "All Property") {
                                $where[] = "`Property_Type` = :propertyType";
                                $params[':propertyType'] = $value;
                            }
                            break;


                        case 'min_price':
                            // Between max and min price
                            if(!empty($this->filterValues['max_price']) && !empty($value)) {
                                $where[] = "`Appartment_Price` BETWEEN :minPrice AND :maxPrice";
                                $params[':minPrice'] = (int) filter_var($value, FILTER_SANITIZE_NUMBER_INT);
                                $params[':maxPrice'] = (int) filter_var($this->filterValues['max_price'], FILTER_SANITIZE_NUMBER_INT);
                                break;
                            }

                            if(!empty($value)) {
                                $where[] = "`Appartment_Price` >= :minPrice";
                                $params[':minPrice'] = (int) filter_var($value, FILTER_SANITIZE_NUMBER_INT);
                                break;
                            }


                        case 'max_price':

                            if(empty($this->filterValues['min_price']) && !empty($value)) {
                                $where[] = "`Appartment_Price` <= :maxPrice";
                                $params[':maxPrice'] = (int) filter_var($value, FILTER_SANITIZE_NUMBER_INT);
                            }
                            break;
                }
            }


            if (!empty($where)) {
                $this->sqlStatement .= ' WHERE '.implode(' AND ', $where) . ' ORDER BY appartment.Appartment_Title_'.$this->lang.' ASC';
            }
            
            $this->db->query($this->sqlStatement);

            foreach($params as $key => $val) {
                $this->db->bind($key, $val);
            }

            $result = $this->db->resultSet();


            // Total records
            $result_count = $this->db->rowCount();
            $assoc_result_count = (object) ['Total_Records' => $result_count];
            
            $res = (object) array_merge((array) $result, (array) $assoc_result_count);

            echo json_encode($res);
            
            //print_r($res);
    }
}

$fiter = new FilterData();