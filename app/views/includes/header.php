<?php
    ob_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Cache-control" content="public">
    <meta name="description" content="%Meta%">
    <title>KipsalaHome</title>

    <link rel="shortcut icon" type="image/x-icon" href="<?php echo URLROOT?>favicon.ico" />
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="/css/superfish.min.css">
    <link rel="stylesheet" href="/css/megafish.min.css">
    <link rel="stylesheet" href="/css/jquery.mhead.css">
    <link rel="stylesheet" href="/libs/aos-master/dist/aos.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/5.5.3/core/css/jquery.mmenu.all.css">

    <link rel="stylesheet" href="/css/navigation.css">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/media.css">
    <!-- <link rel='preconnect' href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" rel="stylesheet"> -->
    <link href="https://use.fontawesome.com/releases/v5.0.4/css/all.css" rel="stylesheet">


    <link href="https://fonts.googleapis.com/css?family=Cormorant+Upright" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300" rel="stylesheet">
    
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans+Condensed:300" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Montserrat:300" rel="stylesheet">


<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
<script>
window.addEventListener("load", function(){
window.cookieconsent.initialise({
  "palette": {
    "popup": {
      "background": "#252e39"
    },
    "button": {
      "background": "transparent",
      "text": "#14a7d0",
      "border": "#14a7d0"
    }
  },
  "type": "opt-out"
})});
</script>

</head>
<body>

