<footer class="admin-footer">
    <p>Copyright KipsalaHome, &copy; <?php echo date("Y");?> </p>
</footer>

    <script src="/libs/jquery-3.3.1.min.js"></script>
    <script src="/libs/popper.min.js"></script>
    <script src="/libs/bootstrap.min.js"></script>

<script src="/libs/ckeditor5/ckeditor.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/js/fileinput.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/themes/fa/theme.js" type="text/javascript"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="/js/admin.js"></script>
    <script src="js/load_image.js"></script>

</body>
</html>