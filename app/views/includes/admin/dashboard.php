<?php require_once __DIR__  . '/../../classes/Dashboard.php'; ?>


<?php 

function active($current_page){
  $url_array =  explode('/', $_SERVER['REQUEST_URI']) ;
  $url = end($url_array);  
  if(strpos($url,$current_page)!==false) {
      echo 'active'; 
  }
  
}


?>

<div class="dashboard col-md-3">
    <div class="list-group">
        <a href="admin" class="list-group-item main-color-bg">
           Dashboard
        </a>
        <a href="pages" class="list-group-item <?php active('pages'); active('edit_home'); active('edit_about'); active('edit_contact'); ?>">Pages</a>
        <a href="posts"  class="list-group-item <?php active('posts'); active('edit_post'); ?>">Posts <span class="badge">
            <?php echo $dashboard->postsCount(); ?>
        </span></a>

        <a href="apartments" class="list-group-item <?php active('apartments'); active('add_apartment'); active('edit_apartment');  ?>">Apartments
            <span class="badge">
                <?php echo $dashboard->apartmentCount(); ?>
            </span>
        </a>
    </div>
</div>


