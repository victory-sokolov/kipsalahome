<?php

    require_once __DIR__ . '/../../views/classes/DBRequests.php';
    require_once('FooterClass.php');

    $footerModel = new FooterClass();
    $contact = $footerModel->getContacts()[0];

    /* Language definition */
    $i18n = new \i18n($_SERVER['DOCUMENT_ROOT']. '/vendor/philipp15b/php-i18n/lang/lang_{LANGUAGE}.yml', 'langcache/', 'en');
    // Parameters: language file path, cache dir, default language (all optional)

    // init object: load language files, parse them if not cached, and so on.
    $i18n->setPrefix('LANG');
    $i18n->init();

    $languages = ['en', 'lv', 'ru'];
    // Current active language
    $activeLang = strtolower($i18n->getAppliedLang());

    //$_SESSION['lang'] = ucfirst(strtolower($activeLang));
    $_SESSION['lang'] = $activeLang;

    // Remove active language from dropdown
    if (($key = array_search($activeLang, $languages)) !== false) {
        unset($languages[$key]);
    }

    $projectInstance = new DBRequests();
    $projectData = $projectInstance->selectProject();
?>

<div class="main-navigation container-fluid">

<div class="container">

    <a href="#mobile-menu" class="toggle-mnu" aria-hidden=true><span></span></a>

    <div class="top-header-menu">
        <div class="logo-wrap">
            <a href="/?lang=<?php echo $activeLang; ?>">
                <img src="/public/images/KipsalaHome_White Vertical.png" alt="KipsalaHome Logotype">
            </a>
        </div>
    </div>


    <div class="header-wrap clearfix">

        <div class="top-mnu">

            <ul class="sf-menu">
                <li><a href="#"><?php echo LANG::menu_ourProject; ?></a>
                    <div class="sf-mega">

                        <div class="container-fluid sub-mnu-container">

                            <div class="row">

                                <div class="col-md-10">

                                    <div class="row">
                                    <?php foreach($projectData as $project) : ?>
                                        <div class="col-lg-3">

                                            <div class="menu-new-item">

                                            <div class="apartment-menu-section">

                                        <a href="/search?project=<?php echo $project->name.'&lang='.$activeLang; ?>">
                                            <img class="responsive-img" src="/public/images/<?php echo $project->image ?>" alt="Project">
                                                    </a>

                                                    <div class="overlay-section">
                                        
                                                        <a href="/search?project=<?php echo $project->name.'&lang='.$activeLang; ?>" class="mega-link">
                                                            <h3><?php echo $project->name;?></h3>
                                                            <hr class="horizontal">
                                                        </a>

                                                        <div class="link-container">
                                                            <div class="col-md-12">

                                                                <ul class="mega-sub">

                                          <li><a href="/search?appartment=sale&project=<?php echo $project->name.'&lang='.$activeLang; ?>
                                          ">
                                                                    &#8226; <?php echo LANG::searchFilters_forSale; ?></a>
                                                                    </li>

                                                                    <li><a href="/search?appartment=rent&project=<?php echo $project->name.'&lang='.$activeLang; ?>">
                                                                    &#8226; <?php echo LANG::searchFilters_forRent; ?></a>
                                                                    </li>

                                                                </ul>

                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                        <?php endforeach; ?>
                                    </div>

                                </div>
                            </div>

                        </div>

                    </div>

                </li>

                <li>
                    <a href="<?php echo "/news?lang=".$_SESSION['lang']?>"
                    class="cmn-t-underline"><?php echo LANG::menu_news; ?>
                    </a>
                </li>

                <li>
                    <a href="<?php echo "/about?lang=".$_SESSION['lang']?>" class="cmn-t-underline">
                    <?php echo LANG::menu_aboutUs; ?></a>
                </li>

                <li>
                    <a href="<?php echo "/contact?lang=".$_SESSION['lang']?>" class="cmn-t-underline">
                    <?php echo LANG::menu_contactUs; ?></a>
                </li>

            </ul>

        </div>
        <!-- End Top Menu class -->


        <div class="cntc-and-lang">
            <div class="header-contact-number d-none d-lg-block">
                <span class="phone-icon"><i class="fas fa-phone"></i> <?php echo $contact->Contact_Mobilephone; ?> </span>
            </div>

            <div class="btn-group language-dropdown" role="group" aria-label="Button group with nested dropdown">
                <div class="btn-group" role="group">
                    <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <a href="?lang=en" class="active-language">
                            <?php echo strtoupper($i18n->getAppliedLang()); ?>
                        </a>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                        <?php foreach($languages as $lang) :?>
                            <a class="dropdown-item" href="?lang=<?= strtolower($lang); ?>">
                                <?php echo strtoupper($lang); ?>
                            </a>
                        <?php endforeach;?>
                    </div>
                </div>
            </div>
        </div>


     </div>
    </div>

</div>
<!-- Navigation End -->


