<?php


class FooterClass {

    private $db;

    public $lang;

    public function __construct() {

        $this->lang = $_SESSION['lang'];

        $this->db = new \Libs\Database();
    }

    public function getContacts() {

        $this->db->query("SELECT * from contact");
        return $this->db->resultSet();
    }

    public function getProjects()
    {
        $this->db->query("SELECT Project_Name_en as project FROM project");
        return $this->db->resultSet();
    }

}