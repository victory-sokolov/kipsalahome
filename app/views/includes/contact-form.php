<div class="contact-form-apartment fixed">
    <h1> <?php echo LANG::apartment_contactFormText; ?></h1>

    <div class="renseignement">
        <i>&#10006;</i>
    </div>

    <form action="<?php URLROOT . '/../../app/controllers/Contact.php' ?>" method="POST" id="contact-form">

        <?php
            if(isset($_POST['send-message'])) {
                $contact = new Contact();
                echo $contact->successMessage();
                unset($_SESSION['msg']);
            }
        ?>

        <div class="form-group">
            <input type="text" name="contactName" class="form-control inputMaterial" placeholder="<?php echo LANG::placeholder_name ?>" required>
        </div>

        <div class="form-group">
            <input type="email" name="contactEmail" class="form-control inputMaterial" placeholder="<?php echo LANG::placeholder_email ?>" required>
        </div>

        <div class="form-group">
            <textarea name="contactMessage" class="form-control inputMaterial" placeholder="<?php echo LANG::placeholder_message ?>" required></textarea>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-outline-primary submit-form col-sm-5" name="send-message"><?php echo LANG::buttons_submit ?></button>
            <a href="<?php echo 'tel:' . $data['Phone'][0]->Contact_Mobilephone ?>" class="btn btn-outline-success col-sm-6 call-us"><?php echo LANG::buttons_callUs ?></a>
        </div>

    </form>

</div>