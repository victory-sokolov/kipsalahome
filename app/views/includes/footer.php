<?php
    require_once('FooterClass.php');
    $footerModel = new FooterClass();
    $data = $footerModel->getContacts();
    $projects = $footerModel->getProjects();

?>

<footer class="main-footer">

<div class="container-fluid">

    <div class="footer-wrapper">
        <div class="col-md-12">
        <div class="row">

        <div class="col-sm-4 col-lg-3">
            <div class="footer-about">
                <h4><?php echo LANG::menu_aboutUs ?></h4>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Saepe repudiandae labore voluptates aspernatur consequuntur laborum quibusdam tempora est iusto hic?</p>
            </div>
        </div>


        <?php foreach($data as $contactVal) : ?>
        <div class="offset-sm-1">
            <div class="footer-contact">
                <h4> <?php echo LANG::menu_contactUs; ?> </h4>
                <ul>
                    <li><i class="fas fa-map-marker-alt"></i><?php echo LANG::contact_location; ?>: <?php echo $contactVal->Contact_Address; ?></li>
                    <li><i class="fas fa-phone"></i><?php echo LANG::contact_mobilePhone; ?>: <?php echo $contactVal->Contact_Mobilephone; ?></li>
                    <li><i class="far fa-envelope-open"></i><?php echo LANG::placeholder_email; ?>: <?php echo $contactVal->Contact_Email; ?></li>
                </ul>
            </div>

        </div>
        <?php endforeach; ?>


    <div class="col-xs-6 mx-auto">
        <ul class="footer-navigation">
            <h4><?php echo LANG::menu_ourProject; ?></h4>
            <ul class="sub-projects">
                <?php foreach($projects as $project) : ?>
                    <li>
                        <a href="<?php echo "search?project=".$project->project .'&lang='.$activeLang; ?>">
                            <?php echo $project->project; ?>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </ul>
    </div>

    <div class="col-xs-6 mx-auto">

        <ul class="footer-navigation">
            <h4><?php echo LANG::footer_links; ?></h4>
            <ul class="sub-projects">
                <li>
                    <a href="<?php echo '/news&lang='.$activeLang; ?>">
                        <?php echo LANG::menu_news; ?>
                    </a>
                </li>
    
                <li>
                    <a href="<?php echo '/about&lang='.$activeLang; ?>"><?php echo LANG::menu_aboutUs; ?></a>
                </li>
    
                <li>
                    <a href="<?php echo '/contact&lang='.$activeLang; ?>"><?php echo LANG::menu_contactUs; ?></a>
                </li>
            </ul>
        </ul>

    </div>


    <div class="col-xs-12 mx-auto">
        <div class="newsletter-subscribe-form">
            <p class="text-center"><?php echo LANG::placeholder_newsletter; ?></p>
            <div id="input-group mb-3" class="input-form">
            <form action="https://kipsalahome.us19.list-manage.com/subscribe/post?u=850a5e611cf68591b8cad07ef&amp;id=8b317c556f" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                <div class="input-group">
                   <input type="email" value="" name="EMAIL" class="form-control nl-form" id="mce-EMAIL" placeholder="<?php echo LANG::placeholder_email_address; ?>" required>
                   <span class="input-group-btn">
                        <input type="submit" class="btn btn-outline-primary nl-btn" value=<?php echo LANG::placeholder_subscribe; ?> name="subscribe" id="mc-embedded-subscribe">
                   </span>
                </div>
            </form>
            </div>
        </div>
    </div>
    
    
    <div class="footer-bottom-copyright">
        <div class="row">
            <div class="col-lg-4 col-sm-6">
                <div class="social-medias-block">
                    <div class="socila-medias">
                        <ul class="social-icons">
                            <a href="#"><li><i class="fab fa-twitter"></i></li></a>
                            <a href="#"><li><i class="fab fa-google-plus-g"></i></li></a>
                            <a href="#"><li><i class="fab fa-facebook-f"></i></li></a>
                            <a href="#"><li><i class="fab fa-instagram"></i></li></a>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="copyright">
                    <span class="text-center">Copyright &copy;<span>
                    <?php echo date('Y'); ?>
                    <span>KipsalaHome. All Rights Reserved</span>
                </div>
            </div>

        </div>
    </div>

    </div>

    </div>

</div>
</div>





</footer>

    <script src="/public/libs/jquery-3.3.1.min.js"></script>
    <script src="/public/libs/popper.min.js"></script>
    <script src="/public/libs/bootstrap.min.js"></script>
    <script src="/public/libs/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="/public/libs/superfish.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/5.5.3/core/js/jquery.mmenu.min.all.js"></script>
    <script src="/public/libs/jquery.mhead.js"></script>
    <script src="/public/libs/aos-master/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>
    <script src="/public/js/main.js"></script>

    <noscript>
        Sorry, your browser does not support JavaScript!
        Please Enable JavaScript to continue browsing this website!
    </noscript>

</body>
</html>