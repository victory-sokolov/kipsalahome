<?php

    //Get current link
    $link = $_SERVER['REQUEST_URI'];

    $segments = null;
    
    if(strpos($link, "/appartment/") !== false) {
        $segments = explode('?', $link)[0];
        $searchLink = $segments[0];
    } 

    $segments = explode('?', $link)[0];

    $segments = array_filter(preg_split('(\/|&)', $segments));


    if(strpos(end($segments), "id=") !== false || filter_var(end($segments), FILTER_VALIDATE_INT) == true) {
      array_pop($segments);
    }
    


?>
<nav aria-label="breadcrumb" class="col-sm-12">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/&lang=<?php echo $_SESSION['lang']; ?>">Home</a></li>
        <?php if(isset($search)) echo $search; ?>
        
           <?php 
            foreach($segments as $url) :
                ($url == end($segments)) ? $class = 'not-active' : $class = '';
                ($url == 'appartment') ? $url = 'Search' : $url;
            ?>    
            <li class="breadcrumb-item" aria-current="page">
                <a href="/<?php echo $url . "&lang=" . $_SESSION['lang'] ?>" class="<?php echo $class; ?>">
                    
                    <?php 
                        $link = str_replace('_', ' ',ucfirst($url));
                        
                        if($link == 'Search') {
                            $link = 'Property';
                        }
                        echo $link; 
                    ?>
                </a>
            </li>
          <?php endforeach; ?>
    </ol>
</nav>