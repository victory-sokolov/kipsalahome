-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Фев 18 2019 г., 18:15
-- Версия сервера: 10.1.37-MariaDB-cll-lve
-- Версия PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `kipsalah_db`
--
CREATE DATABASE IF NOT EXISTS `kipsalah_db` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `kipsalah_db`;

-- --------------------------------------------------------

--
-- Структура таблицы `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `about_content_left_en` text NOT NULL,
  `about_content_left_ru` text NOT NULL,
  `about_content_left_lv` text NOT NULL,
  `about_content_right_en` text NOT NULL,
  `about_content_right_ru` text NOT NULL,
  `about_content_right_lv` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `about`
--

INSERT INTO `about` (`id`, `about_content_left_en`, `about_content_left_ru`, `about_content_left_lv`, `about_content_right_en`, `about_content_right_ru`, `about_content_right_lv`) VALUES
(1, 'Kipsala Home – it is a unique project that allows for acquisition of real estate in the Kipsala area. This area is located on the island of the Daugava River and offers lovely views of Old Riga. We have completed the construction of Zundas Dārzi and Riverside. All the apartments are spacious, bright and suitable for comfortable accommodation of modern human.\r\nThe exclusively natural materials were used in the construction of buildings. We care about the safety of our customers therefore the apartments in Riga \r\n', 'КипсалаХоум - уникальный проект, позволяющий приобрести недвижимость в районе Кипсала. Этот район расположился на острове Дуагавы и открывает прекрасные виды на Старую Ригу. Мы реализовали строительство Zundas Darzi и Riverside. Все квартиры являются просторными, светлыми и подходящими для комфортабельного проживания современного человека. В ходе возведения зданий были использованы исключительно натуральные материалы. Мы заботимся о безопасности наших клиентов, поэтому апартаменты в Риге технически оборудованы для полноценного функционирования даже в отсутствие хозяев. ', 'Kipsala Home – unikālais projekts, kas ļauj iegādāties nekustamo īpašumu Ķīpsalas rajonā. Šis rajons izvietojās Daugavas salā, un paver brīnišķīgus skatus uz Vecrīgu. Mēs realizējām „Zundas Dārzu” un „Riverside” būvniecību. Visi dzīvokļi ir plaši, gaiši un derīgi mūsdienīgā cilvēka komfortablai dzīvošanai.\r\nĒku celšanas gaitā bija izmantoti tikai dabiskie materiāli. Mēs rūpējamies par mūsu klientu drošību, tāpēc Rīgas apartamenti ir tehniski aprīkoti pilnvērtīgai funkcionēšanai pat saimnieku prombūtnē. Dzīvokļu īpašniekiem tiek piešķirta atsevišķa autostāvvieta. Pēc vēlmes, varam palīdzēt noformēt uzturēšanās atļauju.', 'City are technically equipped for full functioning even in the absence of owners. Apartment proprietors are provided with separate parking space. If required, we can help in obtaining of a residence permit.\r\nKipsala Home is a spacious fashionable housing practically in the centre of the capital of Latvia. The infrastructure is very well developed here, and all the buildings needed for a fully realised social life are located at a minute’s walk. The apartments under the project Riverside are located on the river bank. You can daily admire the passing ships and pieces of architecture that Riga is so famous for.  We offer you to become the owner of luxury housing at very attractive prices.', 'Владельцам квартир предоставляется отдельное парковочное место. При желании можем помочь с оформлением вида на жительство. КипсалаХоум - это просторное элитное жилье практически в центре столицы Латвии. Здесь очень хорошо развита инфраструктура, и все необходимые для полноценной социальной жизни здания находятся в минутной близости. Квартиры по проекту Риверсай расположены на берегу реки. Вы можете ежедневно любоваться проплывающими судами и архитектурными произведениями, которыми так знаменита Старая Рига. Мы предлагаем вам стать владельцем элитного жилья по очень привлекательным ценам.', 'Kipsala Home – tas ir plašais elitārais mājoklis praktiski Latvijas galvaspilsētas centrā. Šeit ir labi attīstīta infrastruktūra, un Ķīpsalas salā ir ļoti labi attīstīta infrastruktūra, un visas sociālai dzīvei nepieciešamās ēkas atrodas dažu minūšu pastaigas tuvumā. Projekta „Riverside” dzīvokļi ir izvietoti upes krastā. Jūs varat katru dienu tīksmināties ar garām peldošiem kuģiem un par arhitektūras darbiem, ar kuriem tik lielā mērā ir slavena Rīga. Mēs piedāvājam jums kļūt par elitārā mājokļa īpašnieku par ļoti pievilcīgām cenām.');

-- --------------------------------------------------------

--
-- Структура таблицы `about_us_main_page`
--

CREATE TABLE `about_us_main_page` (
  `id` int(11) NOT NULL,
  `about_title_en` varchar(255) NOT NULL,
  `about_title_lv` varchar(255) NOT NULL,
  `about_title_ru` varchar(255) NOT NULL,
  `sub_title_en` varchar(255) NOT NULL,
  `sub_title_ru` varchar(255) NOT NULL,
  `sub_title_lv` varchar(255) NOT NULL,
  `content_en` text NOT NULL,
  `content_lv` text NOT NULL,
  `content_ru` text NOT NULL,
  `about_us_image` varchar(255) NOT NULL,
  `alt_tag` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `about_us_main_page`
--

INSERT INTO `about_us_main_page` (`id`, `about_title_en`, `about_title_lv`, `about_title_ru`, `sub_title_en`, `sub_title_ru`, `sub_title_lv`, `content_en`, `content_lv`, `content_ru`, `about_us_image`, `alt_tag`) VALUES
(1, 'About Title  En', 'Title Lv', 'Title Ru', 'Sub Title EN', 'Modern   &nbspKipsalaHome', 'Sub Title LV', 'This is a unique project, located on the bank of the Daugava River with a lovely view of Old Riga. This project includes – a residential complex „Riverside” and a modern building „Zundas dārzi”. The infrastructure of the island Kipsala is very well developed and all the buildings needed for a fully realised social life are located at a minute’s walk. Every day you can admire the passing ships and pieces of architecture that Riga is so famous for.', 'Tas ir unikāls projekts, kas ir izvietots Daugavas upes krastā, ar brīnišķīgu skatu uz Vecrīgu. Šajā projektā ietilpst: dzīvojamais komplekss „Riverside” un mūsdienīgā ēka „Zundas Dārzi”. Ķīpsalas salā ir ļoti labi attīstīta infrastruktūra, un visas sociālai dzīvei nepieciešamas ēkas atrodas dažu minūšu pastaigas tuvumā. Jūs varat katru dienu tīksmināties ar garām peldošiem kuģiem un par arhitektūras darbiem, ar kuriem tik lielā mērā ir slavena Rīga.', 'Это уникальный проект расположенный на берегу реки  Даугавы с  прекрасным видом на Старую Ригу.  В данный проект входят - жилой комлекс „Riverside” и современное здание „Zundas Darzi”.  На острове Кипсала очень хорошо развита инфраструктура и все необходимые для полноценной социальной жизни здания находятся в минутной близости. Вы можете ежедневно любоваться проплывающими судами и архитектурными произведениями, которыми так знаменита Рига.', 'about-2.jpg', 'Team_1');

-- --------------------------------------------------------

--
-- Структура таблицы `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `admin`
--

INSERT INTO `admin` (`id`, `email`, `password`) VALUES
(2, 'viktor@gmail.com', '$2y$10$TFhX3vqZZqHWYTuJrJqmW./Z4c5mw1/VHnicO1DdDL6m8O4DDiqeS'),
(3, 'patekdevelopment@gmail.com', '$2y$10$ooL6ShEdSqm.o0FtqpErJuiuz/0.TQlSf7kR6gQUyh4ShNibUmUGO');

-- --------------------------------------------------------

--
-- Структура таблицы `apartment_gallery`
--

CREATE TABLE `apartment_gallery` (
  `id` int(11) NOT NULL,
  `image_url` varchar(255) NOT NULL,
  `alt_tag` varchar(255) NOT NULL,
  `apartment_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `apartment_gallery`
--

INSERT INTO `apartment_gallery` (`id`, `image_url`, `alt_tag`, `apartment_id`) VALUES
(130, 'cam01717b1_pr0051_st_2UTQF.jpg', '', 69),
(131, 'cam01717b1_pr0051_st_J0vfP.jpg', '', 69),
(132, 'cam01717b1_pr0051_st_tpt0V.jpg', '', 69),
(133, 'dzivoklis_st1_2.jpg', '', 69),
(134, 'cam01717b1_pr0051_st_hwSlg.jpg', '', 69),
(135, 'cam01717b1_pr0051_st_7XKnd.jpg', '', 69),
(136, 'cam01717b1_pr0051_st_7X27u.jpg', '', 69),
(137, 'cam01717b1_pr0051_st_k3Hux.jpg', '', 69),
(138, 'cam01717b1_pr0051_st_SeBVX.jpg', '', 69),
(139, 'cam01717b1_pr0051_st_sNdLE.jpg', '', 69),
(140, 'cam01717b1_pr0051_st_tydXo.jpg', '', 69),
(141, 'cam01717b1_pr0051_st_yhlyi.jpg', '', 69),
(142, 'cam01717b1_pr0051_st_pEuoT.jpg', '', 69),
(143, 'img_3980_scoro_1_.jpg', '', 71),
(144, 'img_3986_1_.jpg', '', 71),
(145, 'img_3988_1_.jpg', '', 71),
(146, 'dzivoklis_st1_1.jpg', '', 71),
(147, 'img_3982_scoro_1_.jpg', '', 71),
(148, 'cam01717b1_pr0052_st_XW21M.jpg', '', 72),
(149, 'cam01717b1_pr0052_st_QP8IP.jpg', '', 72),
(150, 'cam01717b1_pr0052_st_P68At.jpg', '', 72),
(151, 'dzivoklis_st2_5.jpg', '', 72),
(152, 'cam01717b1_pr0052_st_OxGlY.jpg', '', 72),
(153, 'cam01717b1_pr0052_st_0qbPF.jpg', '', 72),
(154, 'cam01717b1_pr0052_st_CjJpV.jpg', '', 72),
(155, 'cam01717b1_pr0052_st_CufWU.jpg', '', 72),
(156, 'cam01717b1_pr0052_st_eiiLI.jpg', '', 72),
(157, 'cam01717b1_pr0052_st_ePzHI.jpg', '', 72),
(158, 'cam01717b1_pr0052_st_yjDKG.jpg', '', 72),
(159, 'cam01717b1_pr0052_st_Y3U83.jpg', '', 72),
(160, 'cam01717b1_pr0051_st_2UTQF.jpg', '', 74),
(161, 'cam01717b1_pr0051_st_J0vfP.jpg', '', 74),
(162, 'cam01717b1_pr0051_st_tpt0V.jpg', '', 74),
(163, 'dzivoklis_st1_2.jpg', '', 74),
(164, 'cam01717b1_pr0051_st_hwSlg.jpg', '', 74),
(165, 'cam01717b1_pr0051_st_7XKnd.jpg', '', 74),
(166, 'cam01717b1_pr0051_st_7X27u.jpg', '', 74),
(167, 'cam01717b1_pr0051_st_k3Hux.jpg', '', 74),
(168, 'cam01717b1_pr0051_st_SeBVX.jpg', '', 74),
(169, 'cam01717b1_pr0051_st_sNdLE.jpg', '', 74),
(170, 'cam01717b1_pr0051_st_7XKnd.jpg', '', 74),
(171, 'cam01717b1_pr0051_st_yhlyi.jpg', '', 74),
(172, 'cam01717b1_pr0051_st_pEuoT.jpg', '', 74),
(173, 'cam01717b1_pr0051_st_Fvr2R.jpg', '', 74),
(174, 'cam01717b1_pr0051_st_Fvr2R.jpg', '', 69),
(178, 'cam01717b1_pr0043_st_soHob.jpg', '', 84),
(179, 'cam01717b1_pr0043_st_IJ8kN.jpg', '', 84),
(180, 'cam01717b1_pr0043_st_PTRuY.jpg', '', 84),
(181, '2m_dz1.jpg', '', 84),
(182, 'cam01717b1_pr0043_st_8tOEm.jpg', '', 84),
(183, 'cam01717b1_pr0043_st_vX8Bl.jpg', '', 84),
(184, 'cam01717b1_pr0043_st_YTY9W.jpg', '', 85),
(185, 'cam01717b1_pr0043_st_xsb0X.jpg', '', 85),
(186, 'cam01717b1_pr0043_st_PN8BB.jpg', '', 85),
(187, '2m_dz2.jpg', '', 85),
(188, 'cam01717b1_pr0043_st_M7ajQ.jpg', '', 85),
(189, 'cam01717b1_pr0043_st_0K3vZ.jpg', '', 85),
(200, 'cam00317b0_pr0225_st_ltshK.jpg', '', 86),
(201, 'cam00317b0_pr0225_st_6AZbg.jpg', '', 86),
(202, 'cam00317b0_pr0225_st_2KT4a.jpg', '', 86),
(203, '2m_dz3.jpg', '', 86),
(204, 'cam00317b0_pr0225_st_0pwfr.jpg', '', 86),
(205, 'cam00317b0_pr0225_st_c95c6.jpg', '', 86),
(206, 'cam00317b0_pr0225_st_hLyeH.jpg', '', 86),
(207, 'cam00317b0_pr0225_st_V0NnF.jpg', '', 86),
(208, 'cam00317b0_pr0225_st_WvODD.jpg', '', 86),
(209, 'cam00317b0_pr0225_st_v1gd7.jpg', '', 86),
(210, 'cam01717b1_pr0053_st_Emcly.jpg', '', 87),
(211, 'cam01717b1_pr0053_st_jbmfk.jpg', '', 87),
(212, 'cam01717b1_pr0053_st_Cp6qs.jpg', '', 87),
(213, '2m_dz4.jpg', '', 87),
(214, 'cam01717b1_pr0053_st_OwpXm.jpg', '', 87),
(215, 'cam01717b1_pr0053_st_xObRK.jpg', '', 87),
(216, 'cam01717b1_pr0053_st_xSI2L.jpg', '', 87),
(217, 'cam01717b1_pr0053_st_uuvXW.jpg', '', 87),
(218, 'cam01717b1_pr0043_st_s59oj.jpg', '', 76),
(219, 'cam01717b1_pr0043_st_QLXrZ.jpg', '', 76),
(220, 'cam01717b1_pr0043_st_hDJDN.jpg', '', 76),
(221, '1m_dz1.jpg', '', 76),
(222, 'cam01717b1_pr0043_st_hDIIQ.jpg', '', 76),
(223, 'cam01717b1_pr0043_st_7wTB9.jpg', '', 76),
(224, 'cam01717b1_pr0043_st_Omqbk.jpg', '', 77),
(225, 'cam01717b1_pr0043_st_FsArJ.jpg', '', 77),
(226, 'cam01717b1_pr0043_st_34ApA.jpg', '', 77),
(228, '1m_dz2.jpg', '', 77),
(229, 'cam01717b1_pr0043_st_0bRxI.jpg', '', 77),
(230, 'cam01717b1_pr0043_st_5wlng.jpg', '', 78),
(231, 'cam01717b1_pr0043_st_056x6.jpg', '', 78),
(232, 'cam01717b1_pr0043_st_661tG.jpg', '', 78),
(233, '1m_dz3.jpg', '', 78),
(234, 'cam01717b1_pr0043_st_AmbuZ.jpg', '', 78),
(235, 'cam01717b1_pr0043_st_t2Zy1.jpg', '', 78),
(236, 'cam01717b1_pr0043_st_dM8Xb.jpg', '', 78),
(237, 'cam01717b1_pr0043_st_SKU9Q.jpg', '', 78),
(238, 'cam01717b1_pr0043_st_znWEp.jpg', '', 80),
(239, 'cam01717b1_pr0043_st_MnL9k.jpg', '', 80),
(240, 'cam01717b1_pr0043_st_vqyU8.jpg', '', 80),
(241, '1m_dz5.jpg', '', 80),
(242, 'cam01717b1_pr0043_st_FebM7.jpg', '', 80),
(243, 'cam01717b1_pr0043_st_Q5yRN.jpg', '', 80),
(244, 'cam01717b1_pr0043_st_eNb0v.jpg', '', 80),
(245, 'cam01717b1_pr0043_st_FvuCx.jpg', '', 80),
(246, 'cam01717b1_pr0043_st_i96N7.jpg', '', 80),
(247, 'cam01717b1_pr0043_st_Ii7T9.jpg', '', 80),
(248, 'cam01717b1_pr0043_st_0bDZW.jpg', '', 80),
(249, 'cam01717b1_pr0043_st_4ONnk.jpg', '', 80),
(250, 'cam01717b1_pr0043_st_COJxA.jpg', '', 80),
(251, 'cam01717b1_pr0043_st_5KTD9.jpg', '', 80),
(252, 'cam01717b1_pr0043_st_BJiyE.jpg', '', 80),
(253, 'cam01717b1_pr0043_st_3XT5i.jpg', '', 81),
(254, 'cam01717b1_pr0043_st_7HWNS.jpg', '', 81),
(255, 'cam01717b1_pr0043_st_15DwJ.jpg', '', 81),
(256, '1m_dz6.jpg', '', 81),
(257, 'cam01717b1_pr0043_st_a1qE8.jpg', '', 81),
(258, 'cam01717b1_pr0043_st_B1P7a.jpg', '', 81),
(259, 'cam01717b1_pr0043_st_MAfU9.jpg', '', 81),
(260, 'cam01717b1_pr0043_st_UhePe.jpg', '', 81),
(261, 'cam01717b1_pr0043_st_yLGIp.jpg', '', 81),
(262, '3_35366149290_o.jpg', '', 82),
(263, '4_35366150880_o.jpg', '', 82),
(264, '2_35366149030_o.jpg', '', 82),
(265, '1m_dz7.jpg', '', 82),
(266, '5_35622325181_o.jpg', '', 82),
(267, '6_35366151800_o.jpg', '', 82),
(268, '7_35366148710_o.jpg', '', 82),
(269, '8_35714054186_o.jpg', '', 82),
(270, '9_35622324841_o.jpg', '', 82),
(271, '10_35622325101_o.jpg', '', 82),
(272, '11_35366148380_o.jpg', '', 82),
(273, 'cam01717b1_pr0043_st_a5Cot.jpg', '', 83),
(274, 'cam01717b1_pr0043_st_BinUe.jpg', '', 83),
(275, 'cam01717b1_pr0043_st_cafeb.jpg', '', 83),
(276, '1m_dz4.jpg', '', 83),
(277, 'cam01717b1_pr0043_st_jhNJ9.jpg', '', 83),
(278, 'cam01717b1_pr0043_st_msB87.jpg', '', 83),
(279, 'cam01717b1_pr0043_st_Xpmhf.jpg', '', 83),
(280, 'cam01717b1_pr0043_st_zClEd.jpg', '', 83),
(281, 'cam01717b1_pr0043_st_zwexA.jpg', '', 83),
(282, 'cam01717b1_pr0043_st_5wlng.jpg', '', 88),
(283, 'cam01717b1_pr0043_st_056x6.jpg', '', 88),
(284, 'cam01717b1_pr0043_st_661tG.jpg', '', 88),
(285, '1m_dz3.jpg', '', 88),
(286, 'cam01717b1_pr0043_st_AmbuZ.jpg', '', 88),
(287, 'cam01717b1_pr0043_st_dM8Xb.jpg', '', 88),
(288, 'cam01717b1_pr0043_st_dvjI3.jpg', '', 88),
(289, 'cam01717b1_pr0043_st_SKU9Q.jpg', '', 88),
(290, 'cam01717b1_pr0043_st_t2Zy1.jpg', '', 88),
(291, 'cam01717b1_pr0043_st_a5Cot.jpg', '', 90),
(292, 'cam01717b1_pr0043_st_BinUe.jpg', '', 90),
(293, 'cam01717b1_pr0043_st_cafeb.jpg', '', 90),
(294, '1m_dz4.jpg', '', 90),
(295, 'cam01717b1_pr0043_st_jhNJ9.jpg', '', 90),
(296, 'cam01717b1_pr0043_st_msB87.jpg', '', 90),
(297, 'cam01717b1_pr0043_st_Xpmhf.jpg', '', 90),
(298, 'cam01717b1_pr0043_st_zClEd.jpg', '', 90),
(299, 'cam01717b1_pr0043_st_zwexA.jpg', '', 90);

-- --------------------------------------------------------

--
-- Структура таблицы `appartment`
--

CREATE TABLE `appartment` (
  `idAppartment` int(11) NOT NULL,
  `Appartment_Title_Ru` varchar(255) DEFAULT NULL,
  `Appartment_Title_En` varchar(255) DEFAULT NULL,
  `Appartment_Title_Lv` varchar(255) DEFAULT NULL,
  `Appartment_Price` varchar(50) DEFAULT NULL,
  `Appartment_Description_Ru` longtext,
  `Appartment_Description_En` longtext,
  `Appartment_Description_Lv` longtext,
  `Appartment_Images` varchar(255) DEFAULT NULL,
  `Appartment_View` varchar(255) NOT NULL,
  `Appartment_address` varchar(255) NOT NULL,
  `Meta_Description` varchar(255) NOT NULL,
  `Project_ID` int(11) DEFAULT NULL,
  `Property_ID` int(11) DEFAULT NULL,
  `PropDet_ID` int(11) DEFAULT NULL,
  `date_added` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `appartment`
--

INSERT INTO `appartment` (`idAppartment`, `Appartment_Title_Ru`, `Appartment_Title_En`, `Appartment_Title_Lv`, `Appartment_Price`, `Appartment_Description_Ru`, `Appartment_Description_En`, `Appartment_Description_Lv`, `Appartment_Images`, `Appartment_View`, `Appartment_address`, `Meta_Description`, `Project_ID`, `Property_ID`, `PropDet_ID`, `date_added`) VALUES
(69, 'Квартира 1', 'Apartment 1 ', 'Dzivoklis Nr. 1', '225000', '<p>Столица Латвии, особенно ее центральная часть, поражает своими архитектурными постройками. Некоторые районы славятся своими историческими зданиями, которые завораживают своими деталями и необычными решениями. Вы только представьте, как вы прогуливаетесь по этим улицам и любуетесь достопримечательностями с мировым уровнем известности. Именно такая возможность появляется при покупке квартиры в Риге.</p><p>Представляем вашему вниманию шикарные апартаменты в Риге. Вновь отстроенное здание по проекту Zundas Darzi полностью соответствует представлениям о проживании современного человека. Выставленная на продажу квартира располагается на первом этаже трехэтажного здания, которое было введено в эксплуатацию чуть более года назад. При рассмотрении этого варианта обратите внимание на просторную гостиную – это помещение действительно поражает своими масштабами.</p><p>Внутренняя отделка лаконично завершает общее стилистическое направление всей квартиры. Оригинальный дизайн не сможет оставить равнодушным будущих владельцев этой элитной недвижимости. Кухонная зона совмещена с уютной гостиной. Отсутствие строгого разделения двух важных комнат позволяет проводить комфортную организацию некоторых бытовых действий. Вы можете спокойно готовить пищу и принимать ее в кругу близких. При этом хозяйка не остается в изоляции, а имеет возможность непрерывного общения со всеми членами семьи.</p><p>При желании с южной стороны гостиной расположен отдельный выход, благодаря которому вы попадаете на террасу. Озеленение этой части придомовой территории постоянно поддерживается на должном уровне, так что вы будете ежедневно любоваться на цветущие клумбы. Встроенный кухонный гарнитур включает в себя новейшие технические разработки от ведущих производителей. Здесь любая домохозяйка почувствует облегчение и даже удовольствие от повседневных забот.</p><p>Двухкомнатная квартира оборудована системой безопасности, так что вы можете не беспокоиться за собственное имущество. Помимо этого, помещение подключено к современной системе пожаротушения – датчики срабатывают мгновенно и позволяют предотвратить страшные события. Воздух и вода в помещении прогреваются за счет собственного газового котла. Обратите внимание, что по проекту в ходе строительства были использованы исключительно натуральные материалы, которые не выделяют вредных веществ.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam id posuere sapien. Integer ut lorem et turpis finibus eleifend. Aenean facilisis ullamcorper erat, at accumsan urna. Sed efficitur dictum nisi, et tempus risus ullamcorper in. Fusce vel ligula blandit nulla rutrum laoreet quis at dui. Proin quis arcu sed ipsum consequat porttitor non nec felis. Nullam pellentesque ultricies erat vel sodales. Integer dictum, justo non mattis mattis, lectus massa lobortis sapien, in volutpat mi nunc vitae lorem.</p><p>Fusce auctor, metus in pharetra luctus, nisl dui posuere lacus, id tempus justo est in turpis. Ut diam leo, varius id dolor ac, porttitor auctor lectus. Aenean luctus aliquet orci, vitae faucibus mi scelerisque eu. Vivamus lorem nulla, rutrum non efficitur sed, suscipit dignissim neque. Nam arcu sapien, ullamcorper eget interdum et, ornare id risus. Sed id libero sit amet justo mollis fringilla. Suspendisse interdum, enim vitae vehicula commodo, sapien sapien pharetra sem, cursus bibendum erat urna quis quam. Aliquam laoreet sollicitudin massa, et vestibulum libero pharetra nec. Donec ut urna sed eros dictum commodo a a nulla. Sed vel ligula ullamcorper, sodales ligula quis, finibus quam. Curabitur vitae elit nulla.</p>', '<p>Latvijas galvaspilsēta, it sevišķi tās centrālā daļa, pārsteidz ar savām arhitektūras celtnēm. Daži rajoni ir slaveni ar savām vēsturiskām ēkām, kas apbur ar savām detaļām un neparastiem risinājumiem. Jūs tikai iztēlojieties, ka jūs pastaigājaties pa šīm ielām un tīksminieties ar ievērojamām, pasaulslavenām vietām. Tieši tāda iespēja izpaužas, pērkot dzīvokli Rīgā.</p><p>Pievēršam jūsu uzmanību grezniem apartamentiem Rīgā. Atkal uzbūvētā ēka, pēc projekta „Zundas Dārzi”, pilnīgi atbilst priekšstatiem par mūsdienīgā cilvēka dzīvošanu. Pārdošanai izliktais dzīvoklis atrodas trīsstāvu ēkas pirmajā stāvā; šī ēka tika ieviesta ekspluatācijā mazliet vairāk nekā gadu&nbsp;iepriekš. Aplūkojot šo variantu pievērsiet uzmanību plašai viesistabai – šī telpa patiešām pārsteidz ar saviem mērogiem.</p><p>Iekšējā apdare lakoniski pabeidz vispārīgo visa dzīvokļa stilistisko ievirzi. Oriģinālais dizains nevar atstāt par vienaldzīgiem nākamos šī elitārā nekustamā īpašuma īpašniekus. Virtuves zona ir savienota ar mājīgu viesistabu. Divu svarīgu istabu stingras sadales neesamība ļauj veikt komfortablu dažu sadzīves darbību organizēšanu. Jūs varat mierīgi pagatavot ēdienus un lietot to tuvinieku vidū. Turklāt saimniece nepalika izolācijā, bet viņai ir iespēja nepārtraukti komunicēt ar visiem ģimenes locekļiem.</p><p>Viesistabas dienvidu pusē ir izvietota atsevišķa ieeja, un, pēc vēlmes, jūs varat nokļūst terasē. Šīs, blakus mājās atrodošās daļas apzaļumošana tiek uzturēta atbilstošā līmenī, – tātad jūs katru dienu tīksmināsieties par ziedošām puķu dobēm. Iebūvētā virtuves garnitūra iekļauj sevī visjaunākās tehniskās izstrādes no vadošajiem ražotājiem. Šeit jebkura namamāte izjutīs atvieglojumu un pat gandarījumu par ikdienas rūpēm.</p><p>Divistabu dzīvoklis ir aprīkots ar drošības sistēmu, – tātad jūs varēsiet nepārdzīvot par savu īpašumu. Turklāt telpa ir pieslēgta mūsdienīgai ugunsdzēsības sistēmai – devēji acumirklīgi iedarbojas un ļauj novērst briesmīgus notikumus. Gaiss un ūdens telpā iesildās, pateicoties savam gāzes katlam. Pievērsiet uzmanību tam, ka atbilstoši projektam, būvniecības gaitā tika izmantoti tikai dabiskie materiāli, kuri neizdala kaitīgas vielas.</p>', 'cam01717b1_pr0051_st_2UTQF.jpg', 'https://premium.giraffe360.com/city24/zvejnieku-iela-24/', 'Zvejnieku iela 24, Riga, Latvia', '', 1, 1, 168, '2019-01-09'),
(71, 'Квартира 3', 'Apartment 3', 'Dzīvoklis Nr. 3', '235000', '<p>Кипсала – чудесный островной район в центре Риги. Он расположен практически напротив Старой Риги, и добраться до него не сложно. Для этого пройдите по знаменитому Вантовому мосту. В этом уголке столицы Латвии до наших дней удалось сохранить удивительные образцы деревянной архитектуры. Прогуливаясь по этим местам, можно ежедневно наслаждаться спокойной и уютной атмосферой. Если вы собираетесь купить недвижимость за рубежом – это будет отличным вариантом.</p><p>Присмотритесь к вновь отстроенному дому по проекту Zundas Darzi. Просторные апартаменты расположены на первом этаже здания, выполненного по всем канонам современного возведения элитного жилья. Внутренняя отделка полностью завершена и позволяет сразу же заехать в понравившуюся квартиру. Заходя вовнутрь, вы по достоинству оцените просторные и светлые комнаты. Самой большой по площади считается гостиная, совмещенная с кухней. Подобное решение в зонировании квартиры уже давно считается стильным и удобным.</p><p>Взгляните на высокие потолки – они возвышаются почти на три метра над головой. Панорамные окна расположены почти во всю длину стены. А это значит, что вы будете любоваться на чудесные открывающиеся виды и максимально находиться в условиях естественного освещения. В гостиной легко может поместиться большая семья и их многочисленные друзья и родственники. Приобретая квартиры в новостройках на острове Кипсала, вы получаете жилье, соответствующее всем стандартам безопасности. Квартиры оборудованы современной противопожарной системой, проникновение в дом посторонних лиц также исключено.</p><p>С одной стороны, сразу же из гостиной вы сможете прямиком выходить на террасу. В теплый солнечный денек здесь так комфортно и приятно проводить время в окружении зелени и цветущих клумб. Это идеальное место для того, чтобы расслабиться после тяжелых трудовых будней и погрузиться в атмосферу релакса и умиротворения. В квартире предусмотрены две ванные комнаты, одна из которых совмещена с туалетом. Все помещения выполнены в едином дизайнерском стиле. У вас есть возможность меблировать комнаты в стиле модерн. При желании можно не нагружать пространство множеством деталей, а уйти в сторону минимализма. Это будет ваша самая удачная покупка, ведь вы окажетесь в центре архитектурных ансамблей, удивительных пейзажей и знаменитых по всему миру достопримечательностей.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam id posuere sapien. Integer ut lorem et turpis finibus eleifend. Aenean facilisis ullamcorper erat, at accumsan urna. Sed efficitur dictum nisi, et tempus risus ullamcorper in. Fusce vel ligula blandit nulla rutrum laoreet quis at dui. Proin quis arcu sed ipsum consequat porttitor non nec felis. Nullam pellentesque ultricies erat vel sodales. Integer dictum, justo non mattis mattis, lectus massa lobortis sapien, in volutpat mi nunc vitae lorem.</p><p>Fusce auctor, metus in pharetra luctus, nisl dui posuere lacus, id tempus justo est in turpis. Ut diam leo, varius id dolor ac, porttitor auctor lectus. Aenean luctus aliquet orci, vitae faucibus mi scelerisque eu. Vivamus lorem nulla, rutrum non efficitur sed, suscipit dignissim neque. Nam arcu sapien, ullamcorper eget interdum et, ornare id risus. Sed id libero sit amet justo mollis fringilla. Suspendisse interdum, enim vitae vehicula commodo, sapien sapien pharetra sem, cursus bibendum erat urna quis quam. Aliquam laoreet sollicitudin massa, et vestibulum libero pharetra nec. Donec ut urna sed eros dictum commodo a a nulla. Sed vel ligula ullamcorper, sodales ligula quis, finibus quam. Curabitur vitae elit nulla.</p>', '<p>Ķīpsala – brīnišķīgs salu rajons Rīgas centrā. Tas ir praktiski izvietojies iepretī Vecrīgai, un nokļūt līdz tam nav sarežģīti. Šajā nolūkā ejiet pa slaveno Vanšu tiltu. Šajā Latvijas galvaspilsētas stūrītī līdz mūsu dienām izdevās saglabāt brīnumainus koka arhitektūras paraugus. Pastaigājoties pa šīm vietām, var katru dienu baudīt mierīgu un mājīgu gaisotni. Ja jūs plānojat nopirkt nekustamo īpašumu ārzemēs – tas būs teicams variants.</p><p>Ieskatieties atkal uzbūvētajā mājā, pēc projekta „Zundas Dārzi”. Plaši apartamenti izvietojas ēkas pirmajā stāvā, kas ir izpildīta atbilstoši visiem elitārās dzīvojamās platības mūsdienīgās būvniecības kanoniem. Iekšējā apdare ir pilnīgi pabeigta un ļauj uzreiz iebraukt iepatikušajās dzīvoklī. Ienākot iekšā, jūs atbilstoši novērtējiet plašās un gaišās istabas. Par pašu platības ziņā vislielāko tiek uzskatīta viesistaba, kas ir apvienota ar virtuvi. Līdzīgs dzīvokļa zonēšanas risinājums jau sen tiek uzskatīts par stilīgu un ērtu.</p><p>Uzmetiet skatienu augstajiem griestiem, – tie paceļas gandrīz trīs metrus virs galvas. Panorāmas logi izvietojas gandrīz visas sienas garumā. Un tas nozīmē, ka jūs tīksmināsieties par brīnišķīgiem paverošiem skatiem un atradīsieties maksimāli dabiskās apgaismošanas apstākļos. Viesistabā var viegli izmitināties liela ģimene un tās daudzie draugi un radinieki. Iegādājoties dzīvokļus jaunbūvēs Ķīpsalas salā, jūs saņemat dzīvojamo platību, kas atbilst visiem drošības standartiem. Dzīvokļi ir aprīkoti ar mūsdienīgo ugunsdrošības sistēmu; svešu personu iekļūšana mājās arī nav iespējama.</p><p>No vienas puses, no viesistabas, jūs varat uzreiz iziet tieši uz terasi. Siltā, saulainā dieniņā šeit ir tik komfortabli un patīkami pavadīt laiku, kad apkārt ir zaļumi un ziedošās puķu dobes. Tā ir ideāla vieta, lai atslābinātos pēc smagām darbdienām un iegremdētos relaksēšanās un miera atmosfērā. Dzīvoklī ir paredzētas divas vannas istabas, viena no tām ir savienota ar tualeti. Visas telpas ir izpildītas vienotā dizaina stilā. Jums ir iespēja mēbelēt istabas modernā stilā. Pēc vēlmes var nenoslogot telpu ar daudzām detaļām, bet virzīties uz minimālisma pusi. Tas būs visveiksmīgākais pirkums, tā kā jūs nokļūsiet arhitektūras ansambļu, apbrīnojamo ainavu un pasaulslaveno ievērojamo vietu centrā.</p>', 'img_3980_scoro_1_.jpg', '', 'Zvejnieku iela 24, Riga, Latvia', '', 1, 1, 170, '2019-01-09'),
(72, 'Квартира 4', 'Apartment 4', 'Dzīvoklis Nr. 4', '450000', '<p>Зарубежный рынок недвижимости очень богат на отличные варианты и выгодные предложения. Хотите встретить ближайшие праздники в собственной квартире на отдельной островной территории в Риге. Кипсала – это тихий и спокойный район столицы Риги, который богат на архитектурные исторические шедевры и интересные события. Приобретая квартиру в этой части города, вы с головой окунетесь в эту атмосферу невероятных ансамблей, в которых чувствуется выдержанный стиль знаменитых творцов и архитекторов.</p><p>Если вы хотите купить квартиру в Латвии, обратите внимание на четырехкомнатную квартиру в доме с названием «Урбанистический дом с бабушкиным садом». Этот проект был отстроен по всем современным стандартам и предназначен для комфортабельного проживания всех членов семьи. В фешенебельных апартаментах предусмотрены целых три спальни. В одной из них предусмотрена отдельная гардеробная и ванная комната. Никто не будет против, если именно эта комната достанется женской половине семьи. В остальных комнатах также можно удобно расположиться и остальным жильцам. Одно из помещений непосредственно примыкает к гостиной.</p><p>Просторный и светлый холл позволяет уютно расположиться всей семьей и даже пригласить друзей. Кухонная зона не отделена от главной комнаты, а это добавляет большего удобства для вечерних посиделок с друзьями или праздничных застолий. Отсутствие зонирования позволяет сократить время на сервировку обеденного стола. Широкие и высокие панорамные окна открывают прекрасные виды на террасу. У вас будет возможность отдохнуть в жаркий день в прохладной тени чудесных зеленых насаждений. Придомовая территория облагорожена ухоженными клумбами.</p><p>Самая большая по площади ванная комната оснащена ванной от известного производителя и комфортабельной душевой кабиной. Все полы в этих помещениях идут с обогревом, так что вы легко и непринужденно будете покидать их после принятия водных процедур. Если у вас есть личный автомобиль или вы собираетесь арендовать транспорт на какое-то время, можете не беспокоиться о его постановке. Элитная недвижимость в этом районе идет с обязательным парковочным местом. Отличная новость для противников дешевого и некачественного – в ходе строительства использовались только натуральные и экологически чистые материалы. Здесь вы можете не беспокоиться за свое физическое и эмоциональное состояние.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam id posuere sapien. Integer ut lorem et turpis finibus eleifend. Aenean facilisis ullamcorper erat, at accumsan urna. Sed efficitur dictum nisi, et tempus risus ullamcorper in. Fusce vel ligula blandit nulla rutrum laoreet quis at dui. Proin quis arcu sed ipsum consequat porttitor non nec felis. Nullam pellentesque ultricies erat vel sodales. Integer dictum, justo non mattis mattis, lectus massa lobortis sapien, in volutpat mi nunc vitae lorem.</p><p>Fusce auctor, metus in pharetra luctus, nisl dui posuere lacus, id tempus justo est in turpis. Ut diam leo, varius id dolor ac, porttitor auctor lectus. Aenean luctus aliquet orci, vitae faucibus mi scelerisque eu. Vivamus lorem nulla, rutrum non efficitur sed, suscipit dignissim neque. Nam arcu sapien, ullamcorper eget interdum et, ornare id risus. Sed id libero sit amet justo mollis fringilla. Suspendisse interdum, enim vitae vehicula commodo, sapien sapien pharetra sem, cursus bibendum erat urna quis quam. Aliquam laoreet sollicitudin massa, et vestibulum libero pharetra nec. Donec ut urna sed eros dictum commodo a a nulla. Sed vel ligula ullamcorper, sodales ligula quis, finibus quam. Curabitur vitae elit nulla.</p>', '<p>Ārzemju nekustāmā īpašuma tirgus ir ļoti bagāts ar lieliskiem variantiem un izdevīgiem piedāvājumiem. Vai gribat satikt tuvākos svētkus savā dzīvoklī, atsevišķā salu teritorijā Rīgā? Ķīpsala – tas kluss un mierīgs Rīgas pilsētas rajons, kas ir bagāts ar vēsturiskiem arhitektūras šedevriem un interesantiem notikumiem. Iegādājoties dzīvokli šajā pilsētas daļā, jūs pilnīgi iegremdēsieties šajā neiedomājamo ansambļu atmosfērā, kuros ir jūtams nosvērts slaveno radītāju un arhitektu stils.</p><p>Ja jūs gribat nopirkt dzīvokli Latvijā, pievērsiet uzmanību četristabu dzīvoklim, kas atrodas mājā ar nosaukumu „Urbānistiskā māja ar vecmāmiņas dārzu”. Šis projekts bija uzbūvēts atbilstoši visiem mūsdienīgiem standartiem un ir paredzēts komfortablai visu ģimenes locekļu dzīvošanai. Greznos apartamentos ir paredzētas trīs guļamistabas. Vienā no tām ir paredzēta atsevišķa ģērbtuves istaba un vannas istaba. Neviens nebūs pretī, ja tieši šī istaba piederēs ģimenes sieviešu pusei. Pārējās istabās arī var ērti izvietoties arī pārējiem iedzīvotājiem. Viena no telpām tieši piekļaujas viesistabai.</p><p>Plaša un gaiša halle ļauj mājīgi izvietoties visai ģimenei un pat ielūgt draugus. Virtuves zona nav atdalīta no galvenās istabas, un tas dod vairāk ērtību vakarēšanām ar draugiem vakara laikā vai svētku svinēšanām pie galda. Zonēšanas neesamība ļauj samazināt laiku pusdienas galda servēšanai. Plati un augsti panorāmas logi paver lieliskus skatus uz terasi. Jums būs iespēja atpūsties karstā dienā lielisko zaļo apstādījumu vēsā ēna. Piemājas teritoriju pārveido sakoptās puķu dobes.</p><p>Vislielākā platība ziņā vannas istaba ir aprīkota ar vannu (no ievērojamāka ražotāja) un ar komfortablo dušas kabīni. Visas šīs telpas grīdas ir apsildītas, tātad jūs viegli un brīvi atstāsiet tās pēc ūdens procedūru veikšanas. Ja jums ir personiskais automobilis vai jūs plānojat iznomāt transportu uz kādu laiku, varat neuztraukties par to izvietošanu. Elitāram nekustāmām īpašumam šajā rajonā ir paredzēta obligāta stāvvieta. Lieliskais jaunums lētās un nekvalitatīvās būves pretiniekiem – būvniecības gaitā tika izmantoti tikai dabiskie un ekoloģiskie tīrie materiāli. Šeit jūs varat neuztraukties par savu fizisko un emocionālo stāvokli.</p>', 'cam01717b1_pr0052_st_XW21M.jpg', '', 'Zvejnieku iela 24, Riga, Latvia', '', 1, 1, 171, '2019-01-10'),
(74, 'Квартира 1', 'Apartment 1', 'Dzīvoklis Nr. 1', '800', '<p>Столица Латвии, особенно ее центральная часть, поражает своими архитектурными постройками. Некоторые районы славятся своими историческими зданиями, которые завораживают своими деталями и необычными решениями. Вы только представьте, как вы прогуливаетесь по этим улицам и любуетесь достопримечательностями с мировым уровнем известности. Именно такая возможность появляется при покупке квартиры в Риге.</p><p>Представляем вашему вниманию шикарные апартаменты в Риге. Вновь отстроенное здание по проекту Zundas Darzi полностью соответствует представлениям о проживании современного человека. Выставленная на продажу квартира располагается на первом этаже трехэтажного здания, которое было введено в эксплуатацию чуть более года назад. При рассмотрении этого варианта обратите внимание на просторную гостиную – это помещение действительно поражает своими масштабами.</p><p>Внутренняя отделка лаконично завершает общее стилистическое направление всей квартиры. Оригинальный дизайн не сможет оставить равнодушным будущих владельцев этой элитной недвижимости. Кухонная зона совмещена с уютной гостиной. Отсутствие строгого разделения двух важных комнат позволяет проводить комфортную организацию некоторых бытовых действий. Вы можете спокойно готовить пищу и принимать ее в кругу близких. При этом хозяйка не остается в изоляции, а имеет возможность непрерывного общения со всеми членами семьи.</p><p>При желании с южной стороны гостиной расположен отдельный выход, благодаря которому вы попадаете на террасу. Озеленение этой части придомовой территории постоянно поддерживается на должном уровне, так что вы будете ежедневно любоваться на цветущие клумбы. Встроенный кухонный гарнитур включает в себя новейшие технические разработки от ведущих производителей. Здесь любая домохозяйка почувствует облегчение и даже удовольствие от повседневных забот.</p><p>Двухкомнатная квартира оборудована системой безопасности, так что вы можете не беспокоиться за собственное имущество. Помимо этого, помещение подключено к современной системе пожаротушения – датчики срабатывают мгновенно и позволяют предотвратить страшные события. Воздух и вода в помещении прогреваются за счет собственного газового котла. Обратите внимание, что по проекту в ходе строительства были использованы исключительно натуральные материалы, которые не выделяют вредных веществ.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam id posuere sapien. Integer ut lorem et turpis finibus eleifend. Aenean facilisis ullamcorper erat, at accumsan urna. Sed efficitur dictum nisi, et tempus risus ullamcorper in. Fusce vel ligula blandit nulla rutrum laoreet quis at dui. Proin quis arcu sed ipsum consequat porttitor non nec felis. Nullam pellentesque ultricies erat vel sodales. Integer dictum, justo non mattis mattis, lectus massa lobortis sapien, in volutpat mi nunc vitae lorem.</p><p>Fusce auctor, metus in pharetra luctus, nisl dui posuere lacus, id tempus justo est in turpis. Ut diam leo, varius id dolor ac, porttitor auctor lectus. Aenean luctus aliquet orci, vitae faucibus mi scelerisque eu. Vivamus lorem nulla, rutrum non efficitur sed, suscipit dignissim neque. Nam arcu sapien, ullamcorper eget interdum et, ornare id risus. Sed id libero sit amet justo mollis fringilla. Suspendisse interdum, enim vitae vehicula commodo, sapien sapien pharetra sem, cursus bibendum erat urna quis quam. Aliquam laoreet sollicitudin massa, et vestibulum libero pharetra nec. Donec ut urna sed eros dictum commodo a a nulla. Sed vel ligula ullamcorper, sodales ligula quis, finibus quam. Curabitur vitae elit nulla.</p>', '<p>Latvijas galvaspilsēta, it sevišķi tās centrālā daļa, pārsteidz ar savām arhitektūras celtnēm. Daži rajoni ir slaveni ar savām vēsturiskām ēkām, kas apbur ar savām detaļām un neparastiem risinājumiem. Jūs tikai iztēlojieties, ka jūs pastaigājaties pa šīm ielām un tīksminieties ar ievērojamām, pasaulslavenām vietām. Tieši tāda iespēja izpaužas, pērkot dzīvokli Rīgā.</p><p>Pievēršam jūsu uzmanību grezniem apartamentiem Rīgā. Atkal uzbūvētā ēka, pēc projekta „Zundas Dārzi”, pilnīgi atbilst priekšstatiem par mūsdienīgā cilvēka dzīvošanu. Pārdošanai izliktais dzīvoklis atrodas trīsstāvu ēkas pirmajā stāvā; šī ēka tika ieviesta ekspluatācijā mazliet vairāk nekā gadu&nbsp;iepriekš. Aplūkojot šo variantu pievērsiet uzmanību plašai viesistabai – šī telpa patiešām pārsteidz ar saviem mērogiem.</p><p>Iekšējā apdare lakoniski pabeidz vispārīgo visa dzīvokļa stilistisko ievirzi. Oriģinālais dizains nevar atstāt par vienaldzīgiem nākamos šī elitārā nekustamā īpašuma īpašniekus. Virtuves zona ir savienota ar mājīgu viesistabu. Divu svarīgu istabu stingras sadales neesamība ļauj veikt komfortablu dažu sadzīves darbību organizēšanu. Jūs varat mierīgi pagatavot ēdienus un lietot to tuvinieku vidū. Turklāt saimniece nepalika izolācijā, bet viņai ir iespēja nepārtraukti komunicēt ar visiem ģimenes locekļiem.</p><p>Viesistabas dienvidu pusē ir izvietota atsevišķa ieeja, un, pēc vēlmes, jūs varat nokļūst terasē. Šīs, blakus mājās atrodošās daļas apzaļumošana tiek uzturēta atbilstošā līmenī, – tātad jūs katru dienu tīksmināsieties par ziedošām puķu dobēm. Iebūvētā virtuves garnitūra iekļauj sevī visjaunākās tehniskās izstrādes no vadošajiem ražotājiem. Šeit jebkura namamāte izjutīs atvieglojumu un pat gandarījumu par ikdienas rūpēm.</p><p>Divistabu dzīvoklis ir aprīkots ar drošības sistēmu, – tātad jūs varēsiet nepārdzīvot par savu īpašumu. Turklāt telpa ir pieslēgta mūsdienīgai ugunsdzēsības sistēmai – devēji acumirklīgi iedarbojas un ļauj novērst briesmīgus notikumus. Gaiss un ūdens telpā iesildās, pateicoties savam gāzes katlam. Pievērsiet uzmanību tam, ka atbilstoši projektam, būvniecības gaitā tika izmantoti tikai dabiskie materiāli, kuri neizdala kaitīgas vielas.</p>', 'cam01717b1_pr0051_st_2UTQF.jpg', 'https://premium.giraffe360.com/city24/zvejnieku-iela-24/', 'Zvejnieku iela 24, Riga, Latvia', '', 1, 2, 173, '2019-01-10'),
(76, 'Югенд Квартира 1', 'Yugend Apartment 1', 'Jugend Dzivoklis Nr 1', '250000', '<p>Недвижимость за рубежом – уникальная возможность познать всю прелесть и неоднозначность другой культуры. Латвия поражает своей архитектурой и современной инфраструктурой. И вот теперь вы можете стать владельцем недвижимости в лучших районах этой страны.</p><p>Квартира в доме с воссозданными историческими деталями – это верх совершенства на рынке латвийской недвижимости. Апартаменты в каменном строении – это детище самого архитектора М.Нукши. Самые выразительные черты его творчества удалось сохранить и до сегодняшнего дня.</p><p>Попадая вовнутрь помещения, заранее даже невозможно предположить такие размеры комнат. Квартира в здании Югендстиля отличается огромными по площади комнатами. Высокие потолки поражают своей величественностью. Дизайн интерьера выдержан в стиле дома и полностью соответствует творческой натуре будущего хозяина. Просторные комнаты позволяют создавать убранство в венецианском стиле. Или же Вы можете обставить всю площадь, опираясь на минимализм.</p><p>Покупка недвижимости в районе Кипсала позволит вам ощутить всю прелесть старой Риги. Здесь очень развита инфраструктура, поэтому у вас не возникнет проблем с устройством детей в детский сад или в школу. Все самые необходимые административные и социальные здания находятся в шаговой доступности.</p><p>Стоит отметить, что в квартире сохранены все исторические особенности стиля этого здания. Лепнина и розетки будто погружают вас в эпоху двадцатого века. В доме предусмотрен комфортабельный лифт, который уже переоборудован согласно современным требованиям. При этом затраты на коммунальные услуги будут минимальны. По проекту в возведенном сооружении все инженерно-коммуникационные сети максимально продуманы и эффективны.</p><p>Вся внутренняя отделка в квартире уже готова, так что вам не надо будет тратить на ремонт свои средства и, тем более, время. Просторная гостиная объединена с кухней. При желании даже можно организовать стилизованную столовую. Две огромные балконные двери, которые занимают практически всю стену в высоту, позволяют насладиться прекрасными пейзажами этой удивительной страны. Главный акцент в гостиной сделан на отреставрированную печь-камин. Именно она будет собирать вашу семью прохладными вечерами и создавать неповторимый домашний уют времяпрепровождения.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam id posuere sapien. Integer ut lorem et turpis finibus eleifend. Aenean facilisis ullamcorper erat, at accumsan urna. Sed efficitur dictum nisi, et tempus risus ullamcorper in. Fusce vel ligula blandit nulla rutrum laoreet quis at dui. Proin quis arcu sed ipsum consequat porttitor non nec felis. Nullam pellentesque ultricies erat vel sodales. Integer dictum, justo non mattis mattis, lectus massa lobortis sapien, in volutpat mi nunc vitae lorem.</p><p>Fusce auctor, metus in pharetra luctus, nisl dui posuere lacus, id tempus justo est in turpis. Ut diam leo, varius id dolor ac, porttitor auctor lectus. Aenean luctus aliquet orci, vitae faucibus mi scelerisque eu. Vivamus lorem nulla, rutrum non efficitur sed, suscipit dignissim neque. Nam arcu sapien, ullamcorper eget interdum et, ornare id risus. Sed id libero sit amet justo mollis fringilla. Suspendisse interdum, enim vitae vehicula commodo, sapien sapien pharetra sem, cursus bibendum erat urna quis quam. Aliquam laoreet sollicitudin massa, et vestibulum libero pharetra nec. Donec ut urna sed eros dictum commodo a a nulla. Sed vel ligula ullamcorper, sodales ligula quis, finibus quam. Curabitur vitae elit nulla.</p>', '<p>Nekustamais īpašums ārzemēs – unikāla iespēja izzināt visu citas kultūras daiļumu un neviennozīmīgumu. Latvija pārsteidz ar savu arhitektūru un mūsdienīgu arhitektūru. Un, lūk, tagad jūs varat kļūt par nekustāmā īpašuma īpašnieku šīs valsts labākajos rajonos.</p><p>Dzīvoklis, kas atrodas mājā ar izveidotām no jauna detaļām – tas ir pilnības virsotne Latvijas nekustāmā īpašuma tirgū. Apartamenti akmens celtnē – tas ir paša arhitekta M. Nukšas lolojums. Arī līdz mūsdienām izdevās saglabāt viņa visizteiksmīgākās jaunrades iezīmes.</p><p>Nokļūstot telpas iekšpuse, iepriekš pat nav iespējams iedomāties tādus istabu izmērus. Dzīvoklim, kas atrodas Jūgendstila ēkā, ir raksturīgas istabas ar milzīgu platību. Augstie griesti pārsteidz ar savu diženumu. Interjera dizains ir izpildīts mājas stilā un pilnīgi atbilst nākamā saimnieka radošajam raksturam. Plašas istabas ļauj radīt iekārtojumu venēciešu stilā. Vai nu Jūs varat mēbelēt visu platību, balstoties uz minimālismu.</p><p>Nekustāmā īpašuma pirkšana Ķīpsalas rajonā ļaus jums izjust visu Vecrīgas daiļumu. Šeit ir ļoti attīstīta infrastruktūra, tāpēc jums nerodas problēmas iekārtot bērnus bērnudārzā vai skolā. Visas visnepieciešamākās administratīvās un sociālās ēkas atrodas pastaigas attālumā.</p><p>Ir jāatzīmē, ka dzīvoklī ir saglabātas visas vēsturiskās šīs ēkas stila īpatnības. Veidgreznojumi un rozetes it kā iegremdē jūs divdesmitā gadsimta periodā. Mājās ir paredzēts komfortabls lifts, kas jau ir modernizēts saskaņā ar mūsdienīgām prasībām. Turklāt komunālo pakalpojumu izmaksas būs minimālas. Atbilstoši projektam, uzceltā būvē visi inženierkomunikācijas tīkli ir maksimāli pārdomāti un efektīvi.</p><p>Visa iekšējā apdare dzīvoklī jau ir gatava, – tātad jums nevajadzēs tērēt remontam savus līdzekļus un, vēl jo vairāk, laiku. Plaša viesistaba ir apvienota ar virtuvi. Ja ir vēlme, pat var ierīkot stilizētu ēdnīcu. Divas milzīgas balkona durvis, kuras praktiski aizņem visu sienas augstumu, ļauj izbaudīt šīs brīnišķīgās valsts lieliskās ainavas. Viesistabā galvenais akcents ir izdarīts uz restaurēto krāsni-kamīnu. Tieši tas sapulcinās jūsu ģimeni vēsos vakaros un radīs neatkārtojamu mājīgumu, pavadot brīvo laiku.</p>', 'cam01717b1_pr0043_st_s59oj.jpg', '', 'Ogļu iela 32, Rīga, Latvia', '', 2, 1, 175, '2019-01-10'),
(77, 'Югенд Квартира 2', 'Yugend Apartment 2', 'Jugend Dzivoklis Nr 2', '265000', '<p>Архитектурные ансамбли позволяют познать всю прелесть района Кипсалу. Если вы задумались о покупке недвижимости за рубежом, то это отличная возможность познать богатую культуру другой страны.</p><p>Особое внимание заслуживает дом из 4-х этажей, который носит название Югенд. Архитектор Мартинь Нюкша полностью обустроил здание в стиле модерн. При близком рассмотрении комнаты поражают своей площадью. Данная квартира в Риге отличатся высокими потолками (3,10 м) и отсутствием глобального зонирования. Многие комнаты сдвоены, что позволяет использовать максимум пространства в личных целях.</p><p>Представленный вариант находится на первом этаже. Именно благодаря этому в квартиру можно попасть не с одного, а сразу с двух входов. Парадные двери, которые расположены с улицы Оглю, полностью реконструированы и реставрированы. Можете не беспокоиться за свою безопасность – дом является неприступной крепостью для злоумышленников. Вторые двери полностью стилизованы под старину, но при этом они выполнены из современных прочных материалов.</p><p>Встречать гостей вы можете в просторной гостиной. В этом помещении каждый найдет для себя занятие по вкусу. Жилая комната совмещена с кухней. Огромный обеденный стол позволяет рассадить большое количество человек. Вы смело можете устраивать любые семейные праздники или торжества.</p><p>Стоит отметить, что квартира уже полностью обставлена мебелью. Выбранный дизайн интерьера отличается лаконичностью и завершенностью в каждой детали. При этом общие восстановленные исторические черты абсолютно не нагружают и не перебивают стилевую направленность.</p><p>Апартаменты в Риге – отличный вариант для тех, кто ценит личное пространство. Помимо гостиной есть еще две жилые комнаты, которые расположены рядом друг с другом. Представленная квартира очень удобно разделена на все необходимые зоны. Если вы собираетесь приезжать сюда большой семьей – места хватит всем.</p><p>Отдельно выделен кабинет для главного члена семьи. Обставленный по всем правилам угол позволяет решать свои рабочие вопросы в максимально комфортных условиях. Дизайн полностью соответствует потребностям современного человека. Если вы решили купить квартиру в Латвии недорого – обратите свое внимание именно на этот вариант. За относительно невысокую стоимость вы получаете место жительства со всеми бытовыми и социальными удобствами.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam id posuere sapien. Integer ut lorem et turpis finibus eleifend. Aenean facilisis ullamcorper erat, at accumsan urna. Sed efficitur dictum nisi, et tempus risus ullamcorper in. Fusce vel ligula blandit nulla rutrum laoreet quis at dui. Proin quis arcu sed ipsum consequat porttitor non nec felis. Nullam pellentesque ultricies erat vel sodales. Integer dictum, justo non mattis mattis, lectus massa lobortis sapien, in volutpat mi nunc vitae lorem.</p><p>Fusce auctor, metus in pharetra luctus, nisl dui posuere lacus, id tempus justo est in turpis. Ut diam leo, varius id dolor ac, porttitor auctor lectus. Aenean luctus aliquet orci, vitae faucibus mi scelerisque eu. Vivamus lorem nulla, rutrum non efficitur sed, suscipit dignissim neque. Nam arcu sapien, ullamcorper eget interdum et, ornare id risus. Sed id libero sit amet justo mollis fringilla. Suspendisse interdum, enim vitae vehicula commodo, sapien sapien pharetra sem, cursus bibendum erat urna quis quam. Aliquam laoreet sollicitudin massa, et vestibulum libero pharetra nec. Donec ut urna sed eros dictum commodo a a nulla. Sed vel ligula ullamcorper, sodales ligula quis, finibus quam. Curabitur vitae elit nulla.</p>', '<p>Arhitektūras ansambļi ļauj iepazīt visu Ķīpsalas rajona jaukumu. Ja jūs aizdomājaties par nekustāmā īpašuma pirkšanu ārzemēs, tad tā ir teicama iespēja izzināt citas valsts bagāto kultūru.</p><p>Sevišķu uzmanību pelna četrstāvu māja, kurai ir nosaukums „Jūgends”. Arhitekts Mārtiņš Ņukša pilnīgi aprīkoja ēku modernisma stilā. Tuvāk aplūkojot, istabas pārsteidz ar savu platību. Šim dzīvoklim Rīgā ir raksturīgi augsti griesti (3, 10 m) un globālās zonēšanas neesamība. Daudzas istabas ir dubultotas, kas ļauj izmantot telpas lielāko daļu personiskos nolūkos.</p><p>Piedāvātais variants atrodas pirmajā stāvā. Tieši pateicoties tam, dzīvokļi var nokļūt nevis no vienas, bet gan uzreiz no divām ieejām. Parādes durvis, kuras ir izvietotas no Ogļu ielas puses, ir pilnīgi rekonstruētas un restaurētas. Varat neuztraukties par savu drošību – māja ir nepieņemas cietoksnis ļaundariem. Otrās durvis ir pilnīgi stilizētas senlaiku stilā, taču tajā pašā laikā tās ir izgatavotas no mūsdienīgiem izturīgiem materiāliem.</p><p>Jūs varat satikt viesus plašā viesistabā. Šajā telpā katrs atradīs nodarbību pēc savas gaumes. Dzīvojamā istaba ir apvienota ar virtuvi. Milzīgs pusdienu galds ļauj izsēdināt lielu cilvēku skaitu. Jūs varat droši rīkot jebkurus ģimenes svētkus vai svinības.</p><p>Ir jāatzīmē, ka dzīvoklis jau ir pilnīgi mēbelēts. izvēlētajam interjera dizainam ir raksturīgs katras detaļas lakonisms un pabeigtība. Turklāt vispārīgās atjaunotās vēsturiskās iezīmes absolūti nenoslogo un netraucē stilistiskajai ievirzei.</p><p>Apartamenti Rīgā – teicams variants tiem, kas vērtē personīgo telpu. Bez viesistabas vēl ir divas dzīvojamās istabas, kuras ir izvietotas blakus viena otrai. Piedāvātais dzīvoklis ir ļoti ērti iedalīts visās nepieciešamajās zonās. Ja jūs plānojas atbraukt uz šejieni ar lielu ģimeni, – visiem pietiks vietas.</p><p>Ir atsevišķi izcelts kabinets ģimenes galvai. Pēc visiem noteikumiem mēbelētais stūris ļauj risināt savus darba jautājumus maksimāli komfortablos apstākļos. Dizains pilnīgi atbilst mūsdienīgā cilvēka vajadzībām. Ja jūs nolēmāt diezgan lēti nopirkt dzīvokli Latvijā, – pievērsiet savu uzmanību tieši šim variantam. Par samērā zemu cenu Jūs dabūsiet dzīves vietu ar visām sadzīviskām un sociālām ērtībām.</p>', 'cam01717b1_pr0043_st_Omqbk.jpg', '', 'Ogļu iela 32, Rīga, Latvia', '', 2, 1, 176, '2019-01-10'),
(78, 'Югенд Квартира 3', 'Yugend Apartment 3', 'Jugend Dzivoklis Nr 3', '270000', '<p>Если вас заинтересовала недвижимость в Латвии, каталог представленных вариантов поможет вам сориентироваться. Представляем вашему вниманию трехкомнатную квартиру на втором этаже дома. И хотя сооружение выполнено в стиле югенд, внутренняя отделка лишь частично сохранила память прошлого. Вы можете наблюдать внутри лепнину, падуги и розетки, а вот мебель больше приближена к современным тенденциям.</p><p>Стены выполнены в бело-серых холодных оттенках. Но дизайн интерьера продуман таким образом, что вы каждый день будете погружаться в домашнюю атмосферу уюта и тепла. Купить квартиру в Латвии с полностью обставленными комнатами очень просто. Просторная гостиная соединена с кухней. Любая домохозяйка найдет здесь огромное пространство для творчества и вдохновения. Торжественное событие будет очень легко организовать за огромным столом овальной формы и белого цвета.</p><p>Мягкий диван очень удачно сочетает в себе бледно-зеленый и насыщенно сине-бирюзовый цвет. Шторы в этой комнате идеально подобраны под гостиную и кухню одновременно. Дизайнер использовал в своей работе качественные и натуральные материалы. Холодильник замаскирован, так что его трудно сразу же разглядеть. Расположение тумбочек и кухонных принадлежностей облегчает ежедневные бытовые действия.</p><p>Одна комната оборудована отдельным санузлом и душевой комнатой. Это отличное решение для семьи, в которых есть взрослые дети и подростки. Такое расположение самых необходимых комнат способно облегчить утренние сборы на работу и на учебу. В одной из спален вы найдете удобный и небольшой диван, который при необходимости превращается в более просторное спальное место. Эта комната подойдет для того, кто обучается, т.к. здесь же расположен рабочий стол.</p><p>Вторая комната идет с отдельной душевой и гардеробной. Ее явно должна занять главная модница в семье. В отдельно выделенной зоне можно разместить много личных вещей и обуви. Во главе - огромная двухспальная кровать, которая полностью стилизована под общий дизайн интерьера. Здесь обязательно должны спать родители. Ванные комнаты также стилизованы под общую задумку всего интерьера. Зарубежная недвижимость, особенно в районе Кипсала, позволяет познать всю прелесть Риги и ее архитектурных достопримечательностей.</p>', '<p>LOREM</p>', '<p>Ja jūs ieinteresēja nekustāmais īpašums Latvijā, tad piedāvāto variantu katalogs palīdz jums orientēties. Piedāvājam jūsu uzmanībai trīsistabu dzīvokli, kas atrodas mājas otrajā stāvā. Un, kaut gan celtne bija izpildīta jūgendstilā, iekšējā apdare tikai daļēju saglabāja pagātnes atmiņas. Jūs varat novērot veidgreznojumus, augšējās sofites un rozetes, taču mēbeles ir vairāk tuvinātas mūsdienīgajām tradīcijām.</p><p>Sienas ir izpildītas pelēkbaltās aukstās nokrāsās. Bet interjera dizains ir tādā veidā pārdomāts, ka jūs katru dienu iegremdēsieties mājīguma un siltuma mājas atmosfērā. Nopirkt dzīvokli Latvijā ar pilnīgi mēbelētām istabām ir ļoti vienkārši. Plaša viesistaba ir savienota ar virtuvi. Jebkura namamāte atradīs šeit milzīgu telpu jaunradei un iedvesmojumam. Būs viegli organizēt svinīgo notikumu pie milzīga, ovālas formas un baltās krāsas galda.</p><p>Mīksts dīvāns ļoti labi apvieno sevī bāli zaļo un piesātināto zilo un tirkīza krāsu. Aizkari šajā istabā ir ideāli ieraudzīti atbilstoši viesistabai un vienlaicīgi virtuvei. Dizainers savā darbā izmantoja kvalitatīvos un dabiskos materiālus. Ledusskapis ir nomaskēts tā, ka to ir grūti uzreiz saskatīt. Skapīšu un virtuves piederumu izvietojums atvieglo ikdienas sadzīves darbības.</p><p>Viena istaba ir aprīkota ar atsevišķo sanitāro mezglu un dušas kabīni. Tas ir lielisks risinājums ģimenei, kurā ir pieaugošie bērni un pusaudži. Tādai visnepieciešamāko istabu izvietošanai ir spēja padarīt vieglāko rīta sevis sakārtošanu darbam vai mācībām. Vienā no guļamistabām jūs atradīsiet ērtu un nelielu dīvānu, kas, nepieciešamības gadījumā pārvēršas par daudz plašāku guļamvietu. Šī istaba derēs tam, kas mācās, jo šeit arī ir izvietots galds.</p><p>Otrai istabai ir paredzēta atsevišķa dušas telpa un ģērbtuves telpa. Tā, acīmredzot, ir jāaizņem galvenajai ģimenes modes dāmai. Atsevišķi izceltajā zonā var izvietot daudz personisko mantu un apavu. Galvenajā vietā – milzīga divguļamā gulta, kas ir pilnīgi stilizēta atbilstoši interjera vispārīgajam dizainam. Šeit obligāti ir jāguļ vecākiem. Vannas istabas arī ir stilizētas atbilstoši vispārīgai visa interjera iecerei. Ārzemju nekustamais īpašums, it sevišķi Ķīpsalas rajonā, ļauj izzināt visu Rīgas daiļumu un tās arhitektūras ievērojamās vietas.</p>', 'cam01717b1_pr0043_st_5wlng.jpg', 'https://premium.giraffe360.com/city24/oglu-iela-32-3/', 'Ogļu iela 32, Rīga, Latvia', '', 2, 1, 177, '2019-01-10'),
(80, 'Югенд Квартира 5', 'Yugend Apartment 5', 'Jugend Dzivoklis Nr 5', '280000', '<p>Апартаменты в Риге очень ценятся теми, кто предпочитает элитную недвижимость за рубежом. Столица Латвии привлекательна благодаря своим историческим уголкам и архитектурным ансамблям. Квартира в представленном доме поражает просторными комнатами и уютной обстановкой. Дизайн интерьера выполнен в светлых тонах, но при этом не создает атмосферу больничной палаты. Огромные окна, которые идут почти от самого потолка до пола, открывают прекрасный вид на реку и порт. Вы сможете ежедневно любоваться за маленькой жизнью Старой Риги.</p><p>Великолепно украшенный фасад сразу же определяет общее настроение знаменитого архитектора. Внутренняя отделка также сохранила в себе акценты, которые так любил делать Нюкша. Планировка квартиры очень продумана – здесь предусмотрены все основные моменты для комфортного проживания всей семьей. Просторная гостиная соединена с кухней. Это отличный вариант для гостеприимных хозяев. Приготовление пищи в такой квартире станет поистине увлекательным ежедневным делом. Можно готовить любимые блюда и параллельно разговаривать со всеми членами семьи – никакие стены и перегородки не помешают вашей беседе.</p><p>Цены на недвижимость в столице Латвии зависят от расположения квартиры. Представленный вариант – отличный шанс поселиться в центре живописного и исторического района. Светлая спальня выходит своими огромными окнами на порт. Просыпаясь ежедневно в собственной квартире, вы будете наслаждаться всеми речными мероприятиями, которых вы не увидите у себя на Родине. В доме продумана шикарная входная группа. Эта квартира находится на 3 этаже, так что вид из окна будет всегда приятным и эстетичным. Улочки в центре Риги всегда поражают своими необычными архитектурными изысками.</p><p>Вторая спальня оборудована отдельной гардеробной. Любая хозяйка достойна специального помещения для размещения своей одежды и обуви. Для ежедневных процедур предусмотрена отдельная ванная – еще одно продуманное удобство этой квартиры. Внутренняя отделка сохранила отличительные черты творца этого дома – вы непременно по достоинству оцените лепнину и резные элементы. Дубовый паркет придает эксклюзивность помещению. Все полы в квартире идут с подогревом. Если вы задумывались, как получить вид на жительство в Латвии, у нас есть хорошие новости. Эта квартира полностью подходит для ВНЖ. В стоимость покупки также входит процедура оценки.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam id posuere sapien. Integer ut lorem et turpis finibus eleifend. Aenean facilisis ullamcorper erat, at accumsan urna. Sed efficitur dictum nisi, et tempus risus ullamcorper in. Fusce vel ligula blandit nulla rutrum laoreet quis at dui. Proin quis arcu sed ipsum consequat porttitor non nec felis. Nullam pellentesque ultricies erat vel sodales. Integer dictum, justo non mattis mattis, lectus massa lobortis sapien, in volutpat mi nunc vitae lorem.</p><p>Fusce auctor, metus in pharetra luctus, nisl dui posuere lacus, id tempus justo est in turpis. Ut diam leo, varius id dolor ac, porttitor auctor lectus. Aenean luctus aliquet orci, vitae faucibus mi scelerisque eu. Vivamus lorem nulla, rutrum non efficitur sed, suscipit dignissim neque. Nam arcu sapien, ullamcorper eget interdum et, ornare id risus. Sed id libero sit amet justo mollis fringilla. Suspendisse interdum, enim vitae vehicula commodo, sapien sapien pharetra sem, cursus bibendum erat urna quis quam. Aliquam laoreet sollicitudin massa, et vestibulum libero pharetra nec. Donec ut urna sed eros dictum commodo a a nulla. Sed vel ligula ullamcorper, sodales ligula quis, finibus quam. Curabitur vitae elit nulla.</p>', '<p>Apartamentus Rīgā ļoti augsti vērtē tie, kas dod priekšroku elitāriem nekustāmiem īpašumiem ārzemēs. Latvijas galvaspilsēta ir pievilcīga, pateicoties saviem vēsturiskiem stūrīšiem un arhitektūras ansambļiem. Dzīvoklis reprezentētajā mājā pārsteidz ar plašām istabām un mājīgu aprīkojumu. Interjera dizains ir izpildīts gaišos toņos, turklāt nerada slimnīcas palātas atmosfēru. Milzīgi logi, kas sākas gandrīz no pašiem griestiem un stiepjas līdz griestiem, paver lielisku skatu uz upi un ostu. Jūs varat katru dienu tīksmināties par Vecrīgas mazo dzīvi.</p><p>Krāšņi izrotātā fasāde uzreiz nosaka ievērojamā arhitekta vispārīgo noskaņojumu. Iekšējā apdare arī saglabāja sevī akcentus, kurus Ņukšai tik ļoti patika izdarīt. Dzīvokļa plānojums ir ļoti pārdomāts, – šeit ir paredzēti visi galvenie momenti – visas ģimenes komfortablai dzīvošanai. Plaša viesistaba ir savienota ar virtuvi. Tas ir teicams variants viesmīlīgiem saimniekiem. Uztura pagatavošana tādā dzīvoklī patiešām kļūst par ikdienas aizraujošo lietu. Var pagatavot mīļākos ēdienus un paralēli runāt ar visiem ģimenes locekļiem – nekādas sienas un starpsienas netraucēs jūsu sarunai.</p><p>Nekustāmo īpašuma cenas Latvijas galvaspilsētā ir atkarīgas no dzīvokļa izvietošanas. Reprezentētais variants – teicamās izredzes apmesties uz dzīvi gleznainā un vēsturiskā rajona centrā. Gaiša guļamistaba ar saviem milzīgiem logiem iziet uz ostas pusi. Katru dienu pamostoties savā dzīvoklī, jūs baudīsiet visus upes pasākumus, kurus jūs neieraudzīsiet savā Dzimtenē. Mājā ir pārdomāta grezna ieejas grupa. Šis dzīvoklis atrodas 3. stāvā, tātad izskats pa logu vienmēr būs patīkams un estētisks. Rīgas centra ieliņas vienmēr pārsteidz ar saviem neparastiem arhitektūras smalkumiem.</p><p>Otra guļamistaba ir aprīkota ar atsevišķu ģērbtuves istabu. Jebkura saimniece ir cienīga gūt speciālo telpu sava apģērba un apavu izvietošanai. Ikdienas procedūrām ir paredzēta atsevišķa vannas istaba – vēl viena pārdomāta šī dzīvokļa ērtība. Iekšējā apdare saglabāja raksturīgās šīs mājas radītāja iezīmes, – jūs noteikti atbilstoši novērtēsiet veidgreznojumus un kokgrebuma elementus. Ozola parkets piešķir telpai ekskluzivitāti. Visas dzīvokļa grīdas ir ar sildīšanu. Ja jūs aizdomājāties, kā saņemt uzturēšanās atļauju Latvijā, tad mums ir labas ziņas. Šis dzīvoklis pilnīgi der uzturēšanās atļaujai. Pirkuma cenā arī ietilpst novērtējuma procedūra.</p>', 'cam01717b1_pr0043_st_MnL9k.jpg', '', 'Ogļu iela 32, Rīga, Latvia', '', 2, 1, 179, '2019-01-11');
INSERT INTO `appartment` (`idAppartment`, `Appartment_Title_Ru`, `Appartment_Title_En`, `Appartment_Title_Lv`, `Appartment_Price`, `Appartment_Description_Ru`, `Appartment_Description_En`, `Appartment_Description_Lv`, `Appartment_Images`, `Appartment_View`, `Appartment_address`, `Meta_Description`, `Project_ID`, `Property_ID`, `PropDet_ID`, `date_added`) VALUES
(81, 'Югенд Квартира 6', 'Yugend Apartment 6', 'Jugend Dzivoklis Nr 6', '256000', '<p>Хотите купить трехкомнатную квартиру в самом престижном районе Риги? Теперь у вас есть отличная возможность стать владельцем шикарный апартаментов в доме, который построен по проекту знаменитого латвийского архитектора М. Нукши. Заселение этой части Латвийской столицы приходится на середину XVII века. Поэтому вы сможете любоваться неповторимыми историческими постройками. Район знаменит своей благоустроенной набережной. При желании можно посещать оборудованный пляж и современный причал для яхт.</p><p>Представленная квартира расположена на третьем этаже эксклюзивного строения. Подниматься в свое жилище вы можете по отреставрированной лестнице, что придает некий шарм и прибавляет некоторую долю романтизма. Для большего комфорта и удобства предусмотрен подъем на каждый этаж в кабине современного лифта. Проходя во двор, откуда вы и будете заходить в свою вновь приобретенную недвижимость в Риге, вы сможете постоянно любоваться отреставрированным фасадом каменного здания.</p><p>Просторная гостиная, выполненная в светлых тонах, предоставляет огромные возможности для расстановки мебели в современном стиле. В этой комнате вы сможете проводить вечера в компании семьи или друзей – места хватит для всех. Большое по площади помещение также соединено с кухней. Такое зонирование позволяет расширить общее пространство и исключает коридорное разграничение. По желанию можно конструктивно разделить эти две комнаты – здесь уже все зависит от ваших взглядов и потребностей. При этом квартира полностью оборудована всеми необходимыми техническими приспособлениями, которые обеспечивают минимальные потребности человека.</p><p>Внутренняя отделка предусматривает заселение без каких-либо дополнительных затрат. Представленный вариант полностью подходит для оформления вида на жительство. Уже по фотографиям можно по достоинству оценить утонченность в каждой детали. Огромные окна, вытянувшиеся практически во всю стену, позволяют оценить всю живописность открывающихся взору пейзажей. Обе комнаты отделены от гостиной – отличный вариант для тех, кто хочет иметь собственный отдельный уголок в рамках общего жилого помещения. В ванной комнате и гостевой ванной, а также в санузлах предусмотрены теплые полы. Здесь все оборудовано для вашего комфортного проживания.</p>', '<p>Do you want to acquire three-room apartment in the most prestigious area of Riga City? Now you have a great opportunity to become the owner of luxury apartments in the house designed by the famous Latvian architect M. Nukša. The settlement of this area of the capital of Latvia falls on the middle of the XVII century. Therefore you can admire the unique historical buildings. The area is famous for its landscaped promenade. If you wish, you can visit highly developed beach and a mooring berth for yachts.</p><p>Presented apartment is located on the third floor of the exclusive building. You can access your apartment going up the restored stairs that gives a certain charm and adds some romanticism. For more comfort and convenience, there is an elevating provided to each floor in the advanced lift cab. Passing into the courtyard, from where you will enter your newly acquired real estate in Riga City, you can endlessly admire the restored facade of stone building.</p><p>The spacious family room, made in bright colours, provides great opportunities for the placement of furniture in a modern style. In this room, you can spend evenings with your family or friends – there is enough space for anyone. The large room is also connected with the kitchen. Such zoning allows you for expanding total space and excludes corridor partitioning. If desired, these two rooms can be constructively separated – everything here depends on your opinions and needs. At the same time, the apartment is fully equipped with all the necessary technical devices to satisfy minimum human needs.</p><p>Interior decoration provides dwelling at no additional cost. The presented option is fully suitable for obtaining a residence permit. &nbsp;Already watching the photos, you can appreciate the delicacy in every detail. Huge windows, stretching almost along the entire wall, allow inhabitants to be treated to sweeping views of beautiful landscapes. Both rooms are separated from the family room- a great option for those who want to have their own nook within the common living space. The heated floors are provided in the bathroom and guest bathroom, as well as in the toilet facilities. Everything here is equipped for your comfortable living.</p>', '<p>Gribat nopirkt trīsistabu dzīvokli visprestižākajā Rīgas rajonā? Tagad jums ir lieliska iespēja kļūt par mājas eleganto apartamentu īpašnieku, kas tiek uzbūvēts pēc Latvijas slavenā arhitekta M. Ņukšas projekta. Šīs Latvijas galvaspilsētas daļas apdzīvošana notiek XVII gadsimta vidū. Tāpēc jūs varēsiet tīksmināties ar neatkārtojamām vēsturiskām celtnēm. Rajons ir slavens ar savu labiekārtoto krastmalu. Ja ir vēlēšanās, tad var apmeklēt aprīkotu pludmali un mūsdienīgu jahtu piestātni.</p><p>Minētais dzīvoklis atrodas ekskluzīvās ēkas trešajā stāvā. Jūs varat pacelties savā mājoklī pa restaurētām kāpnēm, kas piešķir kādu šarmu un pievieno kādu romantisma daļu. Lielākam komfortam un ērtībai ir paredzēta pacelšanās uz katru stāvu mūsdienīgā lifta kabīnē. Pārejot pagalmā, no kurienes jūs ienāksiet savā atkal iegādājamajā nekustamajā īpašumā Rīgā, varēsiet pastāvīgi tīksmināties ar akmens ēkas restaurēto fasādi.</p><p>Plaša viesistaba, kura ir izpildīta gaišos toņos, dod lielas iespējas mēbeļu izvietošanai mūsdienīgajā stilā. Šajā istabā jūs varēsiet pavadīt vakarus ģimenes vai draugu kompānijā – vietas pietiks visiem. Platības ziņā liela telpa arī ir savienota ar virtuvi. Tāda zonēšana ļauj paplašināt vispārīgo telpu un iekļauj gaiteņa norobežojumu. Pēc vēlēšanās var konstruktīvi sadalīt šīs divas istabas, – šeit viss jau ir atkarīgs no jūsu ieskatiem un vajadzībām. Turklāt dzīvoklis ir pilnīgi aprīkots ar visām nepieciešamām tehniskām palīgierīcēm, kas nodrošina cilvēka minimālās vajadzības.</p><p>Iekšējā apdare paredz apdzīvošanu bez jebkādām papildizmaksām. Minētais variants pilnīgi der uzturēšanās atļaujas noformēšanai. Jau pēc fotogrāfijām var cienīgi novērtēt smalkumu katrā detaļā. Milzīgie logi, kuri ir izstiepti praktiski visā sienā, ļauj novērtēt visu skatam pavērušos ainavu gleznainumu. Abas istabas ir atdalītas no viesistabas – lielisks variants tiem, kas grib, lai viņam būtu savs stūrītis dzīvojamās koptelpas ietvaros. Vannas istabā un viesu vannas istabā, kā arī sanitārajos mezglos ir paredzētas siltās grīdas. Šeit viss ir aprīkots jūsu komfortablai dzīvošanai.</p>', 'cam01717b1_pr0043_st_3XT5i.jpg', '', 'Ogļu iela 32, Rīga, Latvia', '', 2, 1, 180, '2019-01-11'),
(82, 'Югенд Квартира 7', 'Yugend Apartment 7', 'Jugend Dzivoklis Nr 7', '450000', '<p>В величественном здании в стиле Югенд прослеживаются черты, характерные для творчества М. Нукши. При первом взгляде на каменный дом сразу же понимаешь, что это элитные апартаменты в Риге. Представленный вариант расположен на последнем четвертом этаже. Внешняя и внутренняя отделка полностью выполнена в стиле модерн. Каждая деталь несет в себе отголосок довоенного времени. У вас есть возможность поселиться в жилище, которое навевает настроение романтизма и мечтательности.</p><p>Высокие панорамные окна практически во всю высоту стены открывают прекрасный вид на Старый город. Прогуливаясь по этим улочкам ты до сих пор под ногами ощущаешь булыжники и как будто попадаешь в средневековье. Если вы желаете купить квартиру в Латвии, обратите внимание на этот дом, который является поистине архитектурным шедевром. Просторная и светлая гостиная представляет собой огромное открытое пространство. Здесь вы сможете воплотить в жизнь свои самые сокровенные мечты и желания. Расстановка мебели не предоставляет никаких проблем. Внутренняя отделка сохранила в себе характерные черты знаменитого архитектора – вы можете обставить все в стиле модерн или обойтись минимализмом.</p><p>Панорамные окна-балконы позволяют любоваться прекрасными пейзажами реки Дуагавы. Выходы есть как в гостиной, так и со стороны огромной кухни. Именно в этом помещении находится раздвижная дверь, которая позволит наслаждаться утренним кофе на балконе собственной квартире в живописном районе Риги. Всего в квартире 4 балкона, и каждый из них предоставляет шанс любоваться всеми прелестями чудесной столицы Латвии. Вы только представьте – как будет здорово проводить выходные или отпуск в собственных роскошных апартаментах. Некоторые архитектурные ансамбли настолько уникальны, что стирается грань между прошлым и будущим. В этой части вы не устанете любоваться на достопримечательности и места для прекрасного времяпрепровождения.</p><p>В одной из спален вы найдете полукруглое окно, которое было характерно для времен разгара творчества Мартиньша Нукши. Комфортное проживание в этих апартаментах обеспечивает наличие подогрева полов во всех жилых комнатах и других специализированных помещениях. Дом полностью оборудован современными системами, позволяющими облегчить жизнь человека. Озелененный внутренний двор позволяет побыть в тишине и провести время с пользой всей семьей.</p>', '<p>In the majestic building of the Art Nouveau style, the idioms of M. Nukša can be traced. On the first look at the stone building, you can at once realize that this is a prestigious apartment in Riga. Presented option is located on the upper fourth floor. Exterior and interior decoration is fully executed in Art Nouveau style. Every detail is fraught with the echo of the pre-war period. You may take an opportunity to stay in the house that evokes the mood of romanticism and reverie.</p><p>The high panoramic windows of almost the entire height of the walls command a lovely view of Old Riga. &nbsp;Walking along these narrow streets, you still feel the cobblestones under your feet and feel as you are in the Middle Ages. If you want to acquire an apartment in Latvia, please pay attention to this house, which is a truly architectural masterpiece. The spacious and bright living room is a huge open space. Here you can turn into reality the most intimate dreams and desires. Arrangement of furniture does not cause any challenges. The interior decoration has retained the idioms of the famous architect; you can arrange all in Art Nouveau style or adhere to minimalism.</p><p>Panoramic windows-balconies allow you to admire beautiful sceneries of the Daugava River. There are exits both in the drawing room and on the side of spacious kitchen. It is the room where a sliding door is that will allow enjoying your morning coffee on the balcony of your own apartment in this picturesque area of Riga City. In total, there are 4 balconies in this apartment, and each of them provides a chance to admire all the charms of the wonderful capital of Latvia. Just imagine how great it would be to spend a weekend or holiday in the luxurious apartment of your own. Some architectural ensembles are so unique that the line between past and future erases. In this area, you will be able to admire incessantly the beauty sights and places for a wonderful pastime.</p><p>In one of the bedrooms, you will find a semicircular window, which was typical at the time of peak of creative activities of Mārtiņš Nukša. The underfloor heating in all living rooms and other specialized areas guarantees comfortable dwelling in these apartments. The house is fully equipped with advanced systems to make the human life easier. Green courtyard allows you to stay in silence and spent time nice and profitably as a family.</p>', '<p>Majestātiskajā ēkā jugendstilā tiek novērotas iezīmes, kuras ir raksturīgas M. Ņukšas jaunradei. Pēc pirmā acu uzmetiena uz akmens māju uzreiz saproti, ka tie ir elitārie apartamenti Rīgā. Minētais variants ir izvietots pēdējā ceturtajā stāvā. Ārējā un iekšējā apdare pilnīgi ir izpildīta modernisma stilā. Katrai detaļai ir pirmskara laika atskaņa. Jums ir iespēja apmesties mājoklī, kas iedveš romantisma un sapņainuma noskaņojumu.</p><p>Augsti panorāmas logi praktiski visā sienas garumā atklāj lielisku skatu uz Vecpilsētu. Pastaigājoties pa šīm ieliņām, tu līdz šim laikam sajūti laukakmeņus zem kājām, un it kā nokļūsti viduslaikos. Ja jūs vēlaties nopirkt dzīvokli Latvijā, pievērsiet uzmanību šai mājai, kas tiešām ir arhitektūras šedevrs. Plaša un gaiša viesistaba, pēc būtības, ir milzīga atklāta telpa. Šeit jūs varēsiet iemiesot dzīvē visslepenākos sapņus un vēlmes. Mēbeļu izvietošana nerada nekādas problēmas. Iekšējā apdare saglabāja sevī slavenā arhitekta raksturīgās iezīmes, – jūs varēsiet mēbelēt visu modernisma stilā vai apieties ar minimālismu.</p><p>Panorāmas logi-balkoni ļauj tīksmināties ar brīnišķīgām Daugavas upes ainavām. Izejas ir gan viesistabā, gan arī no milzīgās virtuves puses. Tieši šajā telpā ir bīdāmās durvis, kas ļauj baudīt rīta kafiju sava dzīvokļa balkonā skaistajā Rīgas rajonā. Dzīvoklī kopā ir 4 balkoni, un katrs no tiem dod izredzi tīksmināties ar visiem Latvijas brīnumainās galvaspilsētas jaukumiem. Jūs tikai iedomājieties, cik būs lieliski pavadīt brīvdienas vai atvaļinājumu savos greznajos apartamentos. Daži arhitektūras ansambļi ir tik unikāli, ka izzūd robeža starp pagātni un nākotni. Šajā daļa jūs nenogurstat tīksmināties ar ievērojamām vietām un vietām lieliskai laika pavadīšanai.</p><p>Vienā no guļamistabām jūs atradīsiet pusapļa logs, kas bija raksturīgs Mārtiņa Ņukšas jaunrades plaukuma laikiem. Komfortablu dzīvošanu šajos apartamentos nodrošina grīdu uzsildīšana visās dzīvojamās istabās un citās specializētajās telpās. Māja ir pilnīgi aprīkota ar mūsdienīgām sistēmām, kas ļauj atvieglot cilvēka dzīvi. Apzaļumots pagalms ļauj būt klusumā un pavadīt laiku ar labumu kopā ar ģimeni.</p>', '9_35622324841_o.jpg', 'https://premium.giraffe360.com/city24/oglu-iela-32-7/', 'Ogļu iela 32, Rīga, Latvia', '', 2, 1, 181, '2019-01-11'),
(83, 'Югенд Квартира 4', 'Yugend Apartment 4', 'Jugend Dzivoklis Nr 4', '250000', '<p>Решили купить трехкомнатную квартиру за рубежом? Присмотритесь к этому варианту, расположенному на втором этаже 4-х этажного здания Югенд. Просторные комнаты и окна во всю стену – отличительная черта этого помещения. В дизайне интерьера прослеживаются черты минимализма и модерна. Светлые пастельные тона удачно сочетаются с функциональной расстановкой мебели. Здесь вы не найдете лишних деталей – каждый элемент будет использован строго по назначению.</p><p>Просторная гостиная частично разделена от кухни. Это очень удобно для организации некоторых процессов. Основной прием приглашенных гостей и беседы можно проводить в этой комнате. А вот праздничные обеды или ужины должны быть организованы на кухне. Помещение позволяет собирать большие компании. Попасть в кухню можно не только из гостиной. По планировке предусмотрен другой вход со стороны других комнат. Это очень удобно – ведь в некоторых моментах можно даже не пересекаться и не тревожить тех, кто находится в соседнем помещении.</p><p>Мебель выполнена исключительно из натуральных материалов. Эта квартира понравится тем, кто предпочитает минимализм и экологичность. Одна комната оборудована под спальню. В ней расположено огромная кровать из лакированного дерева. Шкаф, расположенный напротив, полностью сочетается со спальным местом и текстильными шторами синего цвета. Душевая выполнена в светлых тонах, и лишь синие полотенца разбавляют эту белоснежную комнату. Кстати, она расположена между гостиной и спальной. Также в коридоре предусмотрен гостевой туалет.</p><p>Вторая спальня подойдет для проживания ребенка школьного возраста или студента. Из мебели вы здесь найдете односпальную кровать и место для выполнения учебных заданий. Эту комнату также можно использовать под личный кабинет. Купить квартиру в Риге – это отличная возможность стать обладателем собственности с уникальным историческим прошлым. Вы будете жить в районе Кипсала. Гуляя по этим улицам невозможно не восхищаться архитектурными шедеврами самых талантливых творцов.</p><p>Одним из них считается Мартинь Нюкша, в доме которого вам выпадает шанс приобрести апартаменты. Только представьте – вы поселитесь в самом центре культурной столицы. Вы сможете посетить знаменитые музеи и концертные залы – и все это после приобретения квартире в доме в стиле Модерн.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam id posuere sapien. Integer ut lorem et turpis finibus eleifend. Aenean facilisis ullamcorper erat, at accumsan urna. Sed efficitur dictum nisi, et tempus risus ullamcorper in. Fusce vel ligula blandit nulla rutrum laoreet quis at dui. Proin quis arcu sed ipsum consequat porttitor non nec felis. Nullam pellentesque ultricies erat vel sodales. Integer dictum, justo non mattis mattis, lectus massa lobortis sapien, in volutpat mi nunc vitae lorem.</p><p>Fusce auctor, metus in pharetra luctus, nisl dui posuere lacus, id tempus justo est in turpis. Ut diam leo, varius id dolor ac, porttitor auctor lectus. Aenean luctus aliquet orci, vitae faucibus mi scelerisque eu. Vivamus lorem nulla, rutrum non efficitur sed, suscipit dignissim neque. Nam arcu sapien, ullamcorper eget interdum et, ornare id risus. Sed id libero sit amet justo mollis fringilla. Suspendisse interdum, enim vitae vehicula commodo, sapien sapien pharetra sem, cursus bibendum erat urna quis quam. Aliquam laoreet sollicitudin massa, et vestibulum libero pharetra nec. Donec ut urna sed eros dictum commodo a a nulla. Sed vel ligula ullamcorper, sodales ligula quis, finibus quam. Curabitur vitae elit nulla.</p>', '<p>Nolēmāt nopirkt trīsistabu dzīvokli ārzemēs? Noskatieties šo variantu, kas ir izvietots jūgendstila četrstāvu ēkas otrajā stāvā. Plašas istabas un logi, kuri izplešas pa visu sienu – šīs telpas raksturīgā iezīme. Interjera dizainā tiek novērotas minimālisma un modernisma iezīmes. Gaišie pasteļtoņi lieliski savienojas ar funkcionālo mēbeļu izvietojumu. Šeit jūs neatradīsiet liekas detaļas, – katrs elements tiks izmantots stingri pēc uzdevuma.</p><p>Plaša viesistaba ir daļēji nodalīta no virtuves. Tas ir ļoti ērti dažu procesu organizēšanai. Galveno viesu pieņemšanu un sarunas var rīkot šajā istabā. Taču svētku pusdienām vai vakariņām ir jābūt organizētām virtuvē. Telpa ļauj sapulcināt lielas kompānijas. Nokļūt virtuvē var ne tikai no viesistabas. Pēc plānojuma otrā ieeja ir paredzēta no citu istabu puses. Tas ir ļoti ērti, jo dažos brīžos var pat nesastapties un netraucēt tos, kas atrodas blakustelpā.</p><p>Mēbeles ir izgatavotas tikai no dabiskiem materiāliem. Šis dzīvoklis patiks tiem, kas dod priekšroku minimālismam un ekoloģiskumam. Viena istaba ir aprīkota atbilstoši guļamistabai. Tajā ir izvietota milzīga gulta no lakota koka. Skapis, kas ir izvietots iepretī, pilnīgi savienojas ar guļamvietu un zilās krāsas tekstila aizkariem. Dušas telpa ir izpildīta gaišos toņos, un tika zilie dvieļi „atšķaida” sniegbaltu istabu. Tomēr tā ir izvietota starp viesistabu un guļamistabu. Tāpat arī gaitenī ir paredzēta viesu tualete.</p><p>Otrā guļamistaba derēs skolas vecuma bērna vai studenta dzīvošanai. No mēbelēm Jūs atradīsiet vienguļamo gultu un vietu mācību uzdevumu izpildei. Šo istabu arī var izmantot personīgajam kabinetam. Nopirkt dzīvokli Rīgā – tā ir lieliskā iespēja kļūt par īpašuma ar unikālo vēsturisko pagātni īpašnieku. Jūs dzīvosiet Ķīpsalas rajonā. Pastaigājoties pa šīm ielām, nav iespējams nesajūsmināties par vistalantīgāko radītāju arhitektūras šedevriem.</p><p>Par vienu no viņiem tiek uzskatīts Mārtiņš Ņukša, kura mājā jums būs izredzes iegādāties apartamentus. Tikai iedomājieties, – jūs apmetāties uz dzīvi pašā kultūras galvaspilsētas centrā. Jūs varēsiet apmeklēt ievērojamus muzejus un koncertzāles, – un tas viss notiks pēc dzīvokļa iegādes, kas atrodas modernisma stila mājā.</p>', 'cam01717b1_pr0043_st_BinUe.jpg', 'https://premium.giraffe360.com/city24/oglu-iela-32-4/', 'Ogļu iela 32, Rīga, Latvia', '', 2, 1, 182, '2019-01-11'),
(84, 'Ретро Квартира 1', 'Retro Apartment 1', 'Retro Dzīvoklis Nr. 1', '300000', '<p>Если хотя бы один раз побываете в тихом центре Риги с ее архитектурными шедеврами, вы сразу же захотите приобрести недвижимость в Латвии. Представляем вашему вниманию просторную трехкомнатную квартиру в заново отстроенном доме. Внутренняя отделка полностью соответствует аутентичности деревянного фасада. Этот вариант идеально подойдет для тех, кто предпочитает стиль ретро в дизайне интерьера. Любители убранства прошлых десятилетий по достоинству оценят сочетание резных деревянных деталей и современных материалов.</p><p>Просторная гостиная поражает своей площадью. Именно здесь находится огромный камин, который был специально воссоздан для того, чтобы чувствовать себя владельцем средневековых замков. Вся семья может спокойно проводить вечера в уютной домашней атмосфере. Лакированный рояль идеально подойдет для тех, кто не может представить своей жизни без живой музыки. Приглашайте к себе в гости меломанов или своих знакомых, готовых порадовать своими незаурядными способностями. Каждый раз, когда вы будете заходить в свою квартиру со стороны двора, у вас будет возможность любоваться прекрасными видами района Кипсала.</p><p>Даже ванная комната поражает своим оформлением и отделкой. Современные детали удачно сочетаются с историческими элементами. Стоит отметить, что по проекту в этой комнате тоже предусмотрено окно. Такая квартира подходит для современных и экстравагантных жильцов, которые не комплексуют по мелочам. Светлые тона и эксклюзивный дизайн интерьера не может оставить равнодушным ни одного посетителя этой квартиры. Из огромных окон открывается шикарный вид на Рижский замок. Это здание занимает центральное место в историко-культурном наполнении столицы Латвии.</p><p>Мебель не отличается яркой и богатой расцветкой, но в этом есть свой определенный шарм и приверженность общему оформлению всего дома. Самые яркие акценты стиля ретро – объемная лепнина и резные детали полностью отражены в огромном камине. Кухня совмещена с гостиной – это отличный вариант для дружной и сплоченной семьи. Такая планировка позволяет исключить разграничение и уединение. Все остальные комнаты полностью оборудованы для комфортного проживания современного человека. После покупки этой квартиры у вас не останется не малейших сомнений в том, что вы сделали правильный выбор и стали владельцем шикарных апартаментов в центре Риги.</p>', '<p>If you at least once visit the quiet centre of Riga City with its architectural masterpieces, you will feel in a moment like to acquire the real estate in Latvia. We present to your attention a spacious three-room apartment in a newly rebuilt house. Interior decoration is fully consistent with the authenticity of the wooden facade. This option is ideal for those who prefer the retro style in interior design. Admirers of decorations of past decades will duly appreciate the combination of carved wooden details and advanced materials.</p><p>The spacious living room impresses with its area. There is a huge fireplace, which was specially recreated in order you can feel yourself like the owner of medieval castle. The whole family can peacefully spend the evenings in a cosy homey atmosphere. Lacquered piano is ideal for those who cannot imagine their life without live music. Invite music lovers or friends to your place, who are ready to gladden with their outstanding abilities. Every time you will go enter your apartment from the side of courtyard, you can take an opportunity to admire picturesque views of Kipsala area.</p><p>Even the bathroom impresses with its design and decoration. Modern details are successfully combined with historical elements. It is worth noting that a window is also provided for in the project of this room. Such an apartment is suitable for modern and extravagant tenants who have no complex about trifles. Light colours and exclusive interior design cannot leave indifferent any visitor of this apartment. Huge windows command a lovely view of Riga Castle. This building is in the central place in the historical and cultural content of the capital of Latvia.</p><p>The furniture does not have a bright and rich colour, but this has its own specific charm and adherence to the overall design of the whole house. The brightest accents of retro style – sizable stucco work and carved details are fully reflected in the huge fireplace. The kitchen is combined with the living room – this is a great option for a friendly and united family. Such layout allows avoiding isolation and seclusion. All the other rooms are fully equipped for comfortable stay of modern human. After purchasing this apartment, you will have no the slightest doubt that you made a right choice and became the owner of the luxury apartment in the centre of Riga City.</p>', '<p>Ja kaut vai reizi būsiet klusajā Rīgas centrā ar tās arhitektūras šedevriem, uzreiz gribēsiet iegādāties nekustamo īpašumu Latvijā. Piedāvājam jūsu uzmanībai trīsistabu dzīvokli no jauna uzbūvētajā mājā. Iekšējā apdare pilnīgi atbilst koka fasādes autentiskumam. Šis variants ideāli der tiem, kas atzina par labāku retro stilu interjera dizainā. Pagājušo gadu desmitu iekārtojuma amatieri cienīgi novērtēs kokgrebuma detaļu un mūsdienīgo materiālu kombinējumu.</p><p>Plaša viesistaba pārsteidz ar savu platību. Tieši šeit atrodas milzīgs kamīns, kas tika speciāli atveidots, lai justos par viduslaiku pils īpašnieku. Visa ģimene var mierīgi pavadīt vakarus mājīgā mājas gaisotnē. Lakotas klavieres ideāli der tiem, kas nevar iedomāties savu dzīvi bez dzīvās mūzikas. Aiciniet pie sevis ciemos melomānus vai savas paziņas, kuras ir gatavas iepriecināt ar savām izcilām spējām. Katru reizi, kad jūs ienāksiet savā dzīvoklī no pagalma puses, jums būs iespēja tīksmināties ar brīnišķīgiem Ķīpsalas rajona skatiem.</p><p>Pat vannas istaba pārsteidz ar savu noformējumu un apdari. Mūsdienu detaļas veiksmīgi savienojas ar vēsturiskiem elementiem. Ir jāatzīmē, ka pēc projekta šajā istabā arī ir paredzēts logs. Tāds dzīvoklis der mūsdienīgiem un ekstravagantiem iedzīvotājiem, kuriem nerodas kompleksi sīkumu dēļ. Interjera gaišie toņi un ekskluzīvs dizains nevar atstāt nevienu šī dzīvokļa apmeklētāju par vienaldzīgu. No milzīgiem logiem paveras grezns skats uz Rīgas pili. Šī ēka ieņem centrālo vietu vēsturiski kulturālajā Latvijas galvaspilsētas piepildījumā.</p><p>Mēbeles neatšķiras ar spilgtu un bagātu krāsu kombināciju, bet tajā ir savs noteikts šarms un pieķeršanās vispārīgam visas mājas noformējumam. Visspilgtākie retro stila akcenti ir apjomīgi veidgreznojumi un kokgrebuma detaļas ir pilnīgi attēlotas milzīgajā kamīnā. Virtuve ir savienota ar viesistabu – lielisks variants draudzīgai un saliedētai ģimenei. Tāds plānojums ļauj izslēgt norobežojumu un vientulību. Visas pārējās istabas ir aprīkotas mūsdienīgā cilvēka komfortablai dzīvošanai. Pēc šī dzīvokļa pirkšanas jums nebūs nekādu šaubu par to, ka jūs izdarījāt pareizu izvēli un kļuvāt par eleganto apartamentu īpašnieku Rīgas centrā.</p>', 'cam01717b1_pr0043_st_soHob.jpg', '', 'Ogļu iela 32, Rīga, Latvia', '', 2, 1, 183, '2019-01-11'),
(85, 'Ретро Квартира 2', 'Retro Apartment 2', 'Retro Dzīvoklis Nr. 2', '320000', '<p>Продажа квартир в Риге – это основное направление деятельности нашей компании. Мы предлагаем стать владельцем шикарных апартаментов в центре столицы Латвии в ее самом историческом районе Кипсала. Представляем вашему вниманию огромную квартиру, которая расположилась на первом этаже отстроенного дома в стиле ретро. Внутренняя отделка квартиры полностью продолжает общий стиль здания с деревянным фасадом. При одном только взгляде на восстановленные надписи сразу же хочется изучить историю этого места.</p><p>В этой квартире продумана каждая деталь и мелочь. Продуманная планировка исключает возможность скованности и ограничения в процессе расстановки предметов интерьера. Все комнаты светлые и просторные. Стиль ретро, который уверенно прослеживается внутри помещения, абсолютно не помешает создать дух современности и эксклюзивности. Четко очерченные грани удачно сочетаются с деревянными атрибутами. На первом этаже в гостиной центральное место занимает камин с объемными узорами в виде цветочного орнамента. Именно здесь вы можете собираться холодными вечерами и с комфортом проводить время в кругу семьи.</p><p>Купить квартиру в Риге в месте, где собраны самые необычные архитектурные ансамбли – стать владельцем эксклюзивного жилья. У вас появится возможность любоваться на реку Дуагаву и наблюдать за всеми ключевыми событиями, которые происходят на воде. По проекту в квартире предусмотрены огромные окна практически во всю высоту стен, из которых открывается прекрасный вид на роскошные городские достопримечательности. На кухне за счет таких оконных проемов удается достичь самого высокого уровня освещенности. Ежедневно вы можете любоваться на зеленые насаждения и чудесные пейзажи исторического центра. Для любителей тихих размеренных посиделок с чашечкой кофе на свежем воздухе есть отличная новость. Вы можете в любой момент пристроить к зданию террасу</p><p>По проекту квартира располагается на двух уровнях. На второй этаж вы попадаете через деревянную лестницу. Здесь находятся две просторные спальни и ванная комната. Комфортное проживание обеспечивают полы с подогревом во всех помещениях. Если вы хотите купить квартиру в Риге, присмотритесь именно к этому варианту. Вы получаете огромные шикарные апартаменты с отдельным парковочным местом во дворе, которое будет официально зарегистрировано на имя владельца.</p>', '<p>The sale of apartments in Riga City is the main activity of our company. We offer to become the owner of luxurious apartments in the centre of the capital of Latvia in the very historical its area – Kipsala. We would like to bring to your attention a large apartment, which is located on the first floor of the renovated house in retro style. Interior decoration of apartment follows in full the overall style of the house with wooden facade. With just one look on restored inscriptions, one would like at once to explore the history of this site.</p><p>In this apartment, every detail and trifle is well thought out. Sophisticated layout eliminates the possibility of constraint and limitations in the process of arrangement of interior items. All rooms are bright and spacious. Retro style that can be definitely traced inside the room absolutely does not bother to create a spirit of modernity and exclusivity. Well-defined edges are successfully combined with wooden attributes. On the first floor, in the living room, the centrepiece is a fireplace with sizable patterns in the form of a flower ornament. This is a place, where you can meet together in cold evenings and spend time comfortably in the family circle.</p><p>To purchase an apartment in Riga City, in a place where the most unusual architectural ensembles are located -means to become the owner of an exclusive residential property. You will have the opportunity to admire the Daugava River and watch all the milestone events that take place on the water. &nbsp;According to the project, the apartment includes huge windows of almost the entire height of the walls that command a lovely view of luxury city sights. In the kitchen, due to such window apertures it is possible to achieve the highest level of lighting. Every day you can admire the greeneries and picturesque sceneries of the historical centre. There is great news for lovers of quiet, relaxed gatherings with a cup of coffee in the fresh air. You can attach a terrace to the building at any time.</p><p>According to the project, the apartment is located on two levels. You can get on the second floor by climbing the wooden stairs. There are two spacious bedrooms and a bathroom. &nbsp;Comfortable accommodation is guaranteed by underfloor heating in all rooms. If you wish to acquire an apartment in Riga City, take a look at this option. You will obtain a spacious luxury apartment with a private parking space in the courtyard that will be legally registered in the name of the owner.</p>', '<p><strong>Dzīvokļu pārdošana Rīgā</strong> – tas ir mūsu kompānijas darbības pamatvirziens. Mēs piedāvājam kļūt par grezno apartamentu īpašnieku Latvijas galvaspilsētas centrā, tās visvēsturiskākajā <strong>Ķīpsalas </strong>rajonā. Piedāvājam jūsu uzmanībai milzīgo dzīvokli, kas izvietojas retro stila uzbūvētās mājas pirmajā stāvā. Dzīvokļa iekšējā apdare pilnīgi turpina ēkas ar koka fasādi vispārīgo stilu. Jau no pirmā acu uzmetiena uz atjaunotiem uzrakstiem uzreiz gribas izpētīt šīs vietas vēsturi.</p><p>Šajā dzīvoklī ir pārdomāta katra detaļa un katrs sīkums. Pārdomātais plānojums izslēdz sastiguma un ierobežojuma iespēju interjera priekšmetu izvietošanas procesā. Visas istabas ir gaišas un plašas. Retro stils, kas tiek pārliecinoši novērots telpas iekšā, absolūti netraucēs radīt mūsdienīguma un ekskluzivitātes garu. Skaidri apvilktas kontūras skaldnes ir veiksmīgi kombinētas ar koka atribūtiem. Pirmajā stāvā, viesistabā centrālo vietu aizņem kamīns ar apjomīgu rakstu ziedu ornamenta veidā. Tieši šeit jūs varat sapulcēties aukstos vakaros un komfortabli pavadīt laiku ģimenes lokā.</p><p>Pirkt dzīvokli Rīgā, tajā vietā, kur tiek koncentrēti visneparastākie arhitektūras ansambļi, tas nozīmē kļūt par ekskluzīva mājokļa īpašnieku. Jums radīsies iespēja tīksmināties par Daugavas upi un novērot visus galvenos notikumus, kas notiek uz ūdens. Pēc projekta dzīvoklī ir paredzēti milzīgi logi – praktiski visā sienu augstumā, no kuriem paveras lieliskais skats uz greznām pilsētas ievērojamām vietām. Virtuvē, pateicoties tādām milzīgiem logu ailām izdevās sasniegt augstāko apgaismojuma līmeni. Katru dienu jūs varat tīksmināties par vēsturiskā centra zaļumiem un brīnišķīgām ainavām. Klusās, mierīgās vakarēšanas pulcēšanās ar kafijas tasi svaigā gaisā mīļotājiem ir teicamas ziņas. Jebkurā laikā jūs varat ēkai piebūvēt klāt terasi.</p><p>Pēc projekta dzīvoklis tiek izvietots divos līmeņos. Otrajā stāvā jūs nokļūsiet pa koka kāpnēm. Šeit atrodas divas plašas guļamistabas un vannas istaba. Ērta izmitināšana ir nodrošināta ar apsildāmām grīdām visās telpās. Ja gribat <strong>nopirkt dzīvokli Rīgā</strong>, pievērsiet uzmanību tieši šim variantam. Jūs gūstat milzīgus greznus apartamentus ar atsevišķu autostāvvietu pagalmā, kas tiks oficiāli reģistrēts īpašnieka vārdā.</p>', 'cam01717b1_pr0043_st_YTY9W.jpg', '', 'Ogļu iela 32, Rīga, Latvia', '', 2, 1, 184, '2019-01-11'),
(86, 'Ретро Квартира 3', 'Retro Apartment 3', 'Retro Dzīvoklis Nr. 3', '330000', '<p>Центр Риги поражает своими историческими зданиями и улочками, выложенными булыжниками. Прогуливаясь по тихим и живописным местам, можно ощутить, как прошлое удачно переплетается с настоящим. Если вам приглянулся этот город, то вы просто обязаны приобрести недвижимость в Латвии. Мы предлагаем элитные апартаменты в самом эксклюзивном районе, где открываются шикарные виды на реку Дуагаву. Только представьте, что вы будете просыпаться в собственной квартире, окна которой выходят на центровые достопримечательности этого чудеснейшего города.</p><p>Представляем вашему вниманию современные и роскошные апартаменты в Риге. Трехкомнатная квартира располагается на втором этаже. Становясь владельцем этой зарубежной недвижимости, вы получаете отдельное парковочное место. Здание дома в ретро стиле находится в одном из перспективных районов Риги. Вы сразу же оказываетесь в центре всех самых важных и популярных событий. В шаговой доступности все самые необходимые элементы инфраструктуры. Из окон любой из комнат открываются живописные виды реки или исторических построек столицы Латвии.</p><p>Огромная гостиная поражает своей площадью. Балконная дверь открывает вам возможность ежедневно любоваться на прекрасные виды исторической части Риги. Вы можете наблюдать за активной жизнью в порту или ближайших достопримечательностей. Близость к центральной части отлично сочетается с обязательными природными ресурсами. Недалеко от вашего дома вы найдете самые интересные места для отдыха и приятного времяпрепровождения. В собственной квартире можно организовать отличные зоны для расслабления. Планировка предусматривает наличие двух спален, в каждой из которых поместиться большое спальное место и зона для размещения всех предметов гардероба.</p><p>Дизайн интерьера выполнен в светлых пастельных тонах – это открывает безграничные возможности для оформления всего помещения в современном стиле. Комфортное проживания в этой квартире обеспечивает предусмотренная система обогрева полов и отдельно выделенное помещение в подвале для хранения вещей. При желании вы можете оборудовать в гостиной огромный камин, который будет согревать вас и всех членов семьи холодными вечерами. Если вы не знаете, как оформить вид на жительство в Латвии – мы поможем в оформлении всех необходимых документов. Это будет самая удачная покупка в вашей жизни.</p>', '<p>The centre of Riga City impresses with its historical buildings and narrow cobbled streets. Walking along the quiet and picturesque places, you can feel how the past successfully resonates with the present. If you are pleasing to this city, you just have to purchase the real estate in Latvia. We offer luxury apartments in the most exclusive area with a magnificent view of the River Daugava. Just imagine how you will wake up in the apartment of your own, whose windows open onto the centre sights of this wonderful city.</p><p>We would like to bring to your attention the modern and luxurious apartments in Riga City. Three-room apartment is located on the second floor. By becoming the owner of this foreign property, you will get a private parking space. The building of the house of retro style is located in one of the most promising areas of Riga City. You at once will find yourself in the centre of all the most popular and important events. All the required elements of infrastructure are located at a walking distance. The windows of each room command the picturesque views of the river or historical building of the capital of Latvia.</p><p>The large living room impresses with its area. The balcony donors give you the opportunity to admire the beautiful views of historical part of Riga City. You can watch the active life in the port or nearby sights. The proximity to the central part is perfectly combined with required natural resources. Not far from your house you will find the most interesting places for rest and pleasant pastime. In your own apartment, you can organize excellent areas for relaxation. The layout provides for two bedrooms, in each of which a large bed and an area to accommodate all wardrobe items will fit in.</p><p>The interior design is made in light pastel colours – thus there is an unlimited potential for decoration the entire room in a modern style. The comfortable stay in this apartment is guaranteed by the floor heating system and a separate room in the basement for storage of belongings. If you wish, you can equip a large fireplace in the living room, which will warm you and all family members in cold evenings. If you do not know how to apply for a residence permit in Latvia – we will help in obtaining all the necessary documents. This will be the most successful buy in your life.</p>', '<p>Rīgas centrs pārsteidz ar savām vēsturiskām ēkām un ar akmeņiem bruģētām ielām. Pastaigājoties pa klusām un gleznainām vietām, var sajust, kā pagātne veiksmīgi savijas ar tagadni. Ja jums iepatiksies šī pilsēta, tad jums vienkārši ir pienākums iegādāties <strong>nekustāmo īpašumu Latvijā</strong>. Mēs piedāvājam elitāras apartamentus visekskluzīvākajā rajonā, kur paveras greznie skati uz Daugavas upi. Tikai iedomājieties, ka jūs pamodīsieties savā dzīvoklī, kura logi iziet uz šīs brīnišķīgākās pilsētas centra ievērojamām vietām.</p><p>Piedāvājam jūsu uzmanībai mūsdienīgus un greznus apartamentus Rīgā. Trīsistabu dzīvoklis tiek izvietots otrajā stāvā. Kļūstot par šī ārzemes īpašuma īpašnieku, jūs saņemat atsevišķu autostāvvietu. Retro stila ēka atrodas vienā no perspektīvākajiem Rīgas rajoniem. Jūs uzreiz nokļūstat visu vissvarīgāko un vispopulārāko notikumu centrā. Pastaigas attālumā ir visi visnepieciešamākie infrastruktūras elementi. No jebkuras istabas logiem paveras gleznaini skati uz upi vai uz Latvijas galvaspilsētas vēsturiskajām celtnēm.</p><p>Milzīga viesistaba pārsteidz ar savu platību. Balkona durvis atklāj Jums iespēju katru dienu tīksmināties par Rīgas vēsturiskās daļas lieliskiem skatiem. Jūs varat novērot aktīvo dzīvi ostā vai tuvākajās ievērojamās vietās. Tuvums centrālajai daļai lieliski kombinējas ar obligātajiem dabas resursiem. Netālu no jūsu mājas jūs atradīsiet visinteresantākās vietas atpūtai un patīkamai laika pavadīšanai. Savā dzīvoklī var organizēt lieliskas relaksēšanās zonas. Plānojums paredz divu guļamistabu esamību, katrā no tām ievietosies liela guļamvieta un zona visu garderobes priekšmetu izvietošanai.</p><p>Interjera dizains ir izpildīts gaišās pasteļtoņos – tas paver neierobežotas iespējas noformēt visu telpu mūsdienīgā stilā. Komfortablu dzīvošanu šajā dzīvoklī nodrošina paredzēta grīdas apsildes sistēma un atsevišķi izcelta telpa pagrabā – mantu glabāšanai. Ja ir vēlme, jūs varat viesistabā aprīkot milzīgu kamīnu, kas sasildīs jūs un visus ģimenes locekļus aukstos vakaros. Ja jūs nezināt, kā noformēt uzturēšanās atļauju Latvijā, – mēs palīdzēsim noformēt visus nepieciešamos dokumentus. Tas būs veiksmīgākais pirkums jūsu dzīvē.</p>', 'cam00317b0_pr0225_st_6AZbg.jpg', '', 'Ogļu iela 32, Rīga, Latvia', '', 2, 1, 185, '2019-01-11'),
(87, 'Ретро Квартира 4', 'Retro Apartment 4', 'Retro Dzīvoklis Nr. 4', '350000', '<p>Покупая недвижимость за рубежом, вы получаете уникальный шанс лично окунуться в культуру других народов. Латвия считается страной с богатым историко-культурным наследием. В Риге на правом берегу реки Дуагавы находится старейшая часть города, в которой сосредоточены изумительные здания и всемирно известные достопримечательности. Если вы хотите окунуться в этот удивительный мир необычных исторических ансамблей и неповторимых пейзажей, которые завораживают воображение, вам просто необходимо купить квартиру в Риге.</p><p>Представляем вашему вниманию огромнейшие просторные апартаменты на третьем этаже вновь отстроенного дома в стиле ретро. Деревянный фасад переплетается с элементами из аналогичного материала, которые удачно включены во внутреннюю отделку всей квартиры. Огромные окна открывают отличный вид на речные просторы и знаменитый Старый город. Ежедневно вы сможете любоваться на всемирно известные соборы и церкви. Буквально в шаговой доступности от вашего места проживания находятся ключевые места, которые вы можете посещать намного чаще, чем туристы.</p><p>Здесь все обустроено для комфортного проживания современного человека. Вход в квартиру будет со двора. Для жильцов предусмотрены свои отдельные парковочные места. Внутренняя отделка и дизайн интерьера полностью соответствуют представлениям об эксклюзивности. Уютная и просторная гостиная совмещена с кухней. Вы можете организовать огромную столовую и проводить все свободное время в кругу семьи. Отсутствие разделения позволяет принимать огромное количество гостей. Кухня оборудована качественной немецкой техникой – здесь каждая хозяйка найдет для себя удобные варианты для осуществления повседневных мероприятий.</p><p>Ванные комнаты – хозяйские и гостевые поражают своими масштабами. В каждой из них находится небольшие французские окна с отличными видами. Квартиру можно обставить в стиле модерн – это будет отличное продолжение всему заданному тону здания. Для хранения некоторых крупных вещей, которые не принято держать в жилых зонах, предусмотрены подвальные помещения. Эта квартира полностью оправдает ваши представления об элитном жилье в самом центре прекрасного города с удивительной историей и неповторимыми архитектурными ансамблями. Если вы хотите получить вид на жительство в Латвии – то это идеальный вариант для вас.</p>', '<p>By purchasing the real estate abroad, you get unique chance to immerse yourself in the culture of other nations. Latvia is considered as a country with a rich historical and cultural heritage. In Riga City, on the right bank of the Daugava River, the oldest part of the city is located, where amazing buildings and world-famous sights are concentrated. If you want to plunge into this wonderful world of unusual historical ensembles and unique landscapes fascinating the imagination, you just have to purchase an apartment in Riga City.</p><p>We would like to bring to your attention the largest spacious apartments on the third floor of newly rebuilt retro style building. The wooden façade resonates with the details of similar material that are successfully incorporated into the interior decoration of the entire apartment. The huge windows command a lovely view of the river expanses and the famous Old City. Every day you can admire the world-famous cathedrals and churches. Literally at a walk distance from your place of residence, there are the most important places, which you can visit much more often the tourists.</p><p>Everything here is equipped for the stay of modern person. The entrance to the apartment will be from the courtyard. The private parking spaces are guaranteed for the tenants. The interior decoration and design are fully consistent with the concepts of exclusivity. Cosy and spacious living room is combined with the kitchen. You can organize a large dining room and spend all your leisure time with your family. The lack of partition allows you to receive a huge number of guests. The kitchen is equipped with the high-quality German appliances; every hostess will find the convenient options for her everyday activities here.</p><p>The bathrooms – for tenants and for guests impress with their sizes. Each of them has a small French window commanding the lovely views. The apartment can be furnished in Art Nouveau style; this would be as a perfect complement to the entire style of the building. For storage of some large belongings that are improper to keep in residential areas, there is cellarage provided. This apartment will fully live up to your expectations about luxury accommodation in the very centre of a beautiful city with an amazing history and unique architectural ensembles. If you want to get a residence permit in Latvia, then this is the ideal option for you.</p>', '<p>Iegādājoties nekustāmo īpašumu ārzemēs, jūs gūstat unikālu izredzi personīgi iedziļināties citu tautu kultūrā. Latvija tiek uzskatīta par valsti ar bagātu vēsturisko un kultūras mantojumu. Rīgā, Daugavas upes labajā krastā, atrodas visvecākā pilsētas daļa, kurā ir koncentrētas pārsteidzošas ēkas un pasaulslavenās ievērojamās vietas. Ja jūs vēlaties iedziļināties šajā brīnišķīgajā, neparasto vēsturisko ansambļu un neatkārtojamo ainavu pasaulē, kas apbur iztēli, tad jums vienkārši nepieciešams nopirkt dzīvokli Rīgā.</p><p>Piedāvājam jūsu uzmanībai vislielākos plašos apartamentus, kas atrodas no jauna uzceltās retro stila mājas trešajā stāvā. Koka fasāde ir savijas ar analoģiskā materiāla elementiem, kas veiksmīgi iekļauti visa dzīvokļa apdarē. Milzīgie logi paver lielisku skatu uz upes plašumiem un ievērojamo Vecpilsētu. Katru dienu jūs varat tīksmināties ar pasaulslavenām katedrālēm un baznīcām. Burtiski pastaigas attālumā no jūsu dzīvošanas vietas atrodas galvenās vietas, kuras jūs varat apmeklēt daudz biežāk nekā tūristi.</p><p>Šeit viss ir aprīkots mūsdienīgo cilvēku komfortablai dzīvošanai. Ieeja dzīvoklī būs no pagalma. Iedzīvotājiem ir paredzētas savas atsevišķas autostāvvietas. Iekšējā apdare un interjera dizains pilnīgi atbilst priekšstatiem par ekskluzivitāti. Mājīga un plaša viesistaba ir apvienota ar virtuvi. Jūs varat aprīkot milzīgu ēdamistabu un pavadīt visu savu brīvo laiku ģimenes lokā. Sadales neesamība ļauj uzņemt ļoti lielu viesu skaitu. Virtuve ir aprīkota ar kvalitatīvo vācu tehniku – šeit katra saimniece atradīs sev ērtus variantus ikdienas pasākumu īstenošanai.</p><p>Saimnieku un viesu vannas istabas pārsteidz ar saviem mērogiem. Katrā no tām atrodas nelieli franču logi ar lieliskiem skatiem. Dzīvokli var mēbelēt modernisma stilā – tas būs lielisks turpinājums visam uzdotajam ēkas tonim. Dažu lielu mantu glabāšanai, ko nav pieņemts izvietot dzīvojamās zonās, tiek paredzēti pagrabtelpas. Šis dzīvoklis pilnībā attaisno jūsu priekšstatus par elitāro mājokli lieliskās pilsētas pašā centrā, kurai ir apbrīnojama vēsture un neatkārtojami arhitektūras ansambļi. Ja jūs vēlaties saņemt uzturēšanās atļauju Latvijā – tad tas jums ir ideālais variants.</p>', 'cam01717b1_pr0053_st_Emcly.jpg', '', 'Ogļu iela 32, Rīga, Latvia', '', 2, 1, 186, '2019-01-11');
INSERT INTO `appartment` (`idAppartment`, `Appartment_Title_Ru`, `Appartment_Title_En`, `Appartment_Title_Lv`, `Appartment_Price`, `Appartment_Description_Ru`, `Appartment_Description_En`, `Appartment_Description_Lv`, `Appartment_Images`, `Appartment_View`, `Appartment_address`, `Meta_Description`, `Project_ID`, `Property_ID`, `PropDet_ID`, `date_added`) VALUES
(88, 'Югенд Квартира 3', 'Yugend Apartment 3', 'Jugend Dzivoklis Nr 3', '1150', '<p>Если вас заинтересовала недвижимость в Латвии, каталог представленных вариантов поможет вам сориентироваться. Представляем вашему вниманию трехкомнатную квартиру на втором этаже дома. И хотя сооружение выполнено в стиле югенд, внутренняя отделка лишь частично сохранила память прошлого. Вы можете наблюдать внутри лепнину, падуги и розетки, а вот мебель больше приближена к современным тенденциям.</p><p>Стены выполнены в бело-серых холодных оттенках. Но дизайн интерьера продуман таким образом, что вы каждый день будете погружаться в домашнюю атмосферу уюта и тепла. Купить квартиру в Латвии с полностью обставленными комнатами очень просто. Просторная гостиная соединена с кухней. Любая домохозяйка найдет здесь огромное пространство для творчества и вдохновения. Торжественное событие будет очень легко организовать за огромным столом овальной формы и белого цвета.</p><p>Мягкий диван очень удачно сочетает в себе бледно-зеленый и насыщенно сине-бирюзовый цвет. Шторы в этой комнате идеально подобраны под гостиную и кухню одновременно. Дизайнер использовал в своей работе качественные и натуральные материалы. Холодильник замаскирован, так что его трудно сразу же разглядеть. Расположение тумбочек и кухонных принадлежностей облегчает ежедневные бытовые действия.</p><p>Одна комната оборудована отдельным санузлом и душевой комнатой. Это отличное решение для семьи, в которых есть взрослые дети и подростки. Такое расположение самых необходимых комнат способно облегчить утренние сборы на работу и на учебу. В одной из спален вы найдете удобный и небольшой диван, который при необходимости превращается в более просторное спальное место. Эта комната подойдет для того, кто обучается, т.к. здесь же расположен рабочий стол.</p><p>Вторая комната идет с отдельной душевой и гардеробной. Ее явно должна занять главная модница в семье. В отдельно выделенной зоне можно разместить много личных вещей и обуви. Во главе - огромная двухспальная кровать, которая полностью стилизована под общий дизайн интерьера. Здесь обязательно должны спать родители. Ванные комнаты также стилизованы под общую задумку всего интерьера. Зарубежная недвижимость, особенно в районе Кипсала, позволяет познать всю прелесть Риги и ее архитектурных достопримечательностей.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam id posuere sapien. Integer ut lorem et turpis finibus eleifend. Aenean facilisis ullamcorper erat, at accumsan urna. Sed efficitur dictum nisi, et tempus risus ullamcorper in. Fusce vel ligula blandit nulla rutrum laoreet quis at dui. Proin quis arcu sed ipsum consequat porttitor non nec felis. Nullam pellentesque ultricies erat vel sodales. Integer dictum, justo non mattis mattis, lectus massa lobortis sapien, in volutpat mi nunc vitae lorem.</p><p>Fusce auctor, metus in pharetra luctus, nisl dui posuere lacus, id tempus justo est in turpis. Ut diam leo, varius id dolor ac, porttitor auctor lectus. Aenean luctus aliquet orci, vitae faucibus mi scelerisque eu. Vivamus lorem nulla, rutrum non efficitur sed, suscipit dignissim neque. Nam arcu sapien, ullamcorper eget interdum et, ornare id risus. Sed id libero sit amet justo mollis fringilla. Suspendisse interdum, enim vitae vehicula commodo, sapien sapien pharetra sem, cursus bibendum erat urna quis quam. Aliquam laoreet sollicitudin massa, et vestibulum libero pharetra nec. Donec ut urna sed eros dictum commodo a a nulla. Sed vel ligula ullamcorper, sodales ligula quis, finibus quam. Curabitur vitae elit nulla.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam id posuere sapien. Integer ut lorem et turpis finibus eleifend. Aenean facilisis ullamcorper erat, at accumsan urna. Sed efficitur dictum nisi, et tempus risus ullamcorper in. Fusce vel ligula blandit nulla rutrum laoreet quis at dui. Proin quis arcu sed ipsum consequat porttitor non nec felis. Nullam pellentesque ultricies erat vel sodales. Integer dictum, justo non mattis mattis, lectus massa lobortis sapien, in volutpat mi nunc vitae lorem.</p><p>Fusce auctor, metus in pharetra luctus, nisl dui posuere lacus, id tempus justo est in turpis. Ut diam leo, varius id dolor ac, porttitor auctor lectus. Aenean luctus aliquet orci, vitae faucibus mi scelerisque eu. Vivamus lorem nulla, rutrum non efficitur sed, suscipit dignissim neque. Nam arcu sapien, ullamcorper eget interdum et, ornare id risus. Sed id libero sit amet justo mollis fringilla. Suspendisse interdum, enim vitae vehicula commodo, sapien sapien pharetra sem, cursus bibendum erat urna quis quam. Aliquam laoreet sollicitudin massa, et vestibulum libero pharetra nec. Donec ut urna sed eros dictum commodo a a nulla. Sed vel ligula ullamcorper, sodales ligula quis, finibus quam. Curabitur vitae elit nulla.</p>', 'cam01717b1_pr0043_st_5wlng.jpg', 'https://premium.giraffe360.com/city24/oglu-iela-32-3/', 'Ogļu iela 32, Rīga, Latvia', '', 2, 2, 187, '2019-01-15'),
(90, 'Югенд Квартира 4', 'Yugend Apartment 4', 'Jugend Dzivoklis Nr 4', '1150', '<p>Решили купить трехкомнатную квартиру за рубежом? Присмотритесь к этому варианту, расположенному на втором этаже 4-х этажного здания Югенд. Просторные комнаты и окна во всю стену – отличительная черта этого помещения. В дизайне интерьера прослеживаются черты минимализма и модерна. Светлые пастельные тона удачно сочетаются с функциональной расстановкой мебели. Здесь вы не найдете лишних деталей – каждый элемент будет использован строго по назначению.</p><p>Просторная гостиная частично разделена от кухни. Это очень удобно для организации некоторых процессов. Основной прием приглашенных гостей и беседы можно проводить в этой комнате. А вот праздничные обеды или ужины должны быть организованы на кухне. Помещение позволяет собирать большие компании. Попасть в кухню можно не только из гостиной. По планировке предусмотрен другой вход со стороны других комнат. Это очень удобно – ведь в некоторых моментах можно даже не пересекаться и не тревожить тех, кто находится в соседнем помещении.</p><p>Мебель выполнена исключительно из натуральных материалов. Эта квартира понравится тем, кто предпочитает минимализм и экологичность. Одна комната оборудована под спальню. В ней расположено огромная кровать из лакированного дерева. Шкаф, расположенный напротив, полностью сочетается со спальным местом и текстильными шторами синего цвета. Душевая выполнена в светлых тонах, и лишь синие полотенца разбавляют эту белоснежную комнату. Кстати, она расположена между гостиной и спальной. Также в коридоре предусмотрен гостевой туалет.</p><p>Вторая спальня подойдет для проживания ребенка школьного возраста или студента. Из мебели вы здесь найдете односпальную кровать и место для выполнения учебных заданий. Эту комнату также можно использовать под личный кабинет. Купить квартиру в Риге – это отличная возможность стать обладателем собственности с уникальным историческим прошлым. Вы будете жить в районе Кипсала. Гуляя по этим улицам невозможно не восхищаться архитектурными шедеврами самых талантливых творцов.</p><p>Одним из них считается Мартинь Нюкша, в доме которого вам выпадает шанс приобрести апартаменты. Только представьте – вы поселитесь в самом центре культурной столицы. Вы сможете посетить знаменитые музеи и концертные залы – и все это после приобретения квартире в доме в стиле Модерн.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam id posuere sapien. Integer ut lorem et turpis finibus eleifend. Aenean facilisis ullamcorper erat, at accumsan urna. Sed efficitur dictum nisi, et tempus risus ullamcorper in. Fusce vel ligula blandit nulla rutrum laoreet quis at dui. Proin quis arcu sed ipsum consequat porttitor non nec felis. Nullam pellentesque ultricies erat vel sodales. Integer dictum, justo non mattis mattis, lectus massa lobortis sapien, in volutpat mi nunc vitae lorem.</p><p>Fusce auctor, metus in pharetra luctus, nisl dui posuere lacus, id tempus justo est in turpis. Ut diam leo, varius id dolor ac, porttitor auctor lectus. Aenean luctus aliquet orci, vitae faucibus mi scelerisque eu. Vivamus lorem nulla, rutrum non efficitur sed, suscipit dignissim neque. Nam arcu sapien, ullamcorper eget interdum et, ornare id risus. Sed id libero sit amet justo mollis fringilla. Suspendisse interdum, enim vitae vehicula commodo, sapien sapien pharetra sem, cursus bibendum erat urna quis quam. Aliquam laoreet sollicitudin massa, et vestibulum libero pharetra nec. Donec ut urna sed eros dictum commodo a a nulla. Sed vel ligula ullamcorper, sodales ligula quis, finibus quam. Curabitur vitae elit nulla.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam id posuere sapien. Integer ut lorem et turpis finibus eleifend. Aenean facilisis ullamcorper erat, at accumsan urna. Sed efficitur dictum nisi, et tempus risus ullamcorper in. Fusce vel ligula blandit nulla rutrum laoreet quis at dui. Proin quis arcu sed ipsum consequat porttitor non nec felis. Nullam pellentesque ultricies erat vel sodales. Integer dictum, justo non mattis mattis, lectus massa lobortis sapien, in volutpat mi nunc vitae lorem.</p><p>Fusce auctor, metus in pharetra luctus, nisl dui posuere lacus, id tempus justo est in turpis. Ut diam leo, varius id dolor ac, porttitor auctor lectus. Aenean luctus aliquet orci, vitae faucibus mi scelerisque eu. Vivamus lorem nulla, rutrum non efficitur sed, suscipit dignissim neque. Nam arcu sapien, ullamcorper eget interdum et, ornare id risus. Sed id libero sit amet justo mollis fringilla. Suspendisse interdum, enim vitae vehicula commodo, sapien sapien pharetra sem, cursus bibendum erat urna quis quam. Aliquam laoreet sollicitudin massa, et vestibulum libero pharetra nec. Donec ut urna sed eros dictum commodo a a nulla. Sed vel ligula ullamcorper, sodales ligula quis, finibus quam. Curabitur vitae elit nulla.</p>', 'cam01717b1_pr0043_st_BinUe.jpg', 'https://premium.giraffe360.com/city24/oglu-iela-32-4/', 'Ogļu iela 32, Rīga, Latvia', '', 2, 2, 189, '2019-01-15');

-- --------------------------------------------------------

--
-- Структура таблицы `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `Blog_Title_ru` varchar(255) DEFAULT NULL,
  `Blog_Title_en` varchar(255) DEFAULT NULL,
  `Blog_Title_lv` varchar(255) DEFAULT NULL,
  `Blog_Date` date DEFAULT NULL,
  `Blog_Content_ru` longtext,
  `Blog_Content_en` longtext,
  `Blog_Content_lv` longtext,
  `Blog_Image` varchar(255) NOT NULL,
  `Blog_ImageDescription_ru` varchar(255) NOT NULL,
  `Blog_ImageDescription_en` varchar(255) NOT NULL,
  `Blog_ImageDescription_lv` varchar(255) NOT NULL,
  `Meta_Description` varchar(255) NOT NULL,
  `alt_tag` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `blog`
--

INSERT INTO `blog` (`id`, `Blog_Title_ru`, `Blog_Title_en`, `Blog_Title_lv`, `Blog_Date`, `Blog_Content_ru`, `Blog_Content_en`, `Blog_Content_lv`, `Blog_Image`, `Blog_ImageDescription_ru`, `Blog_ImageDescription_en`, `Blog_ImageDescription_lv`, `Meta_Description`, `alt_tag`) VALUES
(55, NULL, NULL, 'Publikācija Dienas biznesā', '2019-01-17', NULL, NULL, '<p>Ķīpsalā atgriežas jūgendstils. Nav daudz tādu, kas zinās, ka mūsu pilsētas lepnums un īpašā iezīme – Rīgā plaši pārstāvētais jūgendstils atrodams arī Ķīpsalā. Elegantā ēka Ogļu ielā 30 ir viena no interesantākajām un ekskluzīvākajām, taču retajām pēdējos gados atjaunotajām jūgendstila ēkām Daugavas kreisajā krastā.&nbsp;</p><p>Tā ir vienīgā jūgendstila mūra ēka Ķīpsalā, turklāt &nbsp;ar autentiski atjaunotu galvenās fasādes veidolu. &nbsp;Jūgendstila ziedu laiku lakoniskā, taču piemīlīgā celtne ir saglabājusi vēsturisko ielas fasādes arhitektūru un interjera dekoratīvos elementus – rozetes, kāpņu margu rakstus, slīpētos logu stiklus un griestu rotājumus.&nbsp;<a href=\"http://kipsalahome.lv/riverside-residence-lv/4-stavu-eka-modernisma-stila\">Atjaunotā mūra ēka</a>&nbsp;celta pagājušā gadsimta sākumā pēc izcilā latviešu arhitekta Mārtiņa Ņukšas projekta, kurš bijis gan galvenais arhitekts Sevastopolē, gan strādājis kā Marseļas galvenā arhitekta palīgs, pēcāk bijis arī Latvijas diplomātiskais sūtnis daudzās Eiropas valstīs. Traģiski gājis bojā padomju represiju laikā, bet viņa arhitekta veikums arī šodien saglabājies kopumā astoņos jūgendstilā projektētos Rīgas dzīvojamos namos.&nbsp;</p><p>“Patiesībā tā nav restaurācija, kas šeit ir notikusi, bet tā ir mūsu ļoti lielā vēlēšanās šajā vietā Ķīpsalu nepārtraukt, bet turpināt – cilvēciski, saprotami un iespējami patiesi,” stāsta atjaunoto ēku projekta līdzautors arhitekts Pēteris Blūms, uzsverot, ka tas nebūtu iespējams bez projekta komandas, kurā gandrīz brīnumainā kārtā satikušies līdzīgi domājoši un vidi ap sevi līdzīgi izjūtoši cilvēki, sākot jau ar projekta pasūtītāju Borisu Semjonovu, arhitekti Ditu Liepiņu ar kolēģiem, un šis nav pirmais viņu kopīgi realizētais projekts.</p><p>Nezaudējot Ķīpsalas īpašo šarmu</p><p>Nav šaubu, ka rekonstruēt vai uzbūvēt no jauna ir lētāk, un tā tas vairumā gadījumu ar šādām vēsturiskām ēkām arī notiek, bet Ogļu ielas 30. nama īpašnieks uzsvēra savu vēlmi tieši atjaunot, nevis pilnībā pārbūvēt, jo tas ir daudz interesantāk. “Protams, šis projekts ir arī bizness, bet man gribas izveidot vēl vienu skaistu Rīgas stūrīti, tuvu tam, kāds tas bijis kādreiz, un tādu, kas organiski iekļaujas visā pārējā salas apbūvē. Es pats dzīvoju Ķīpsalā, un man ļoti gribētos, lai Ķīpsala saglabātu šo savu īpašo šarmu, citādi mēs pazaudēsim savu pilsētu. Esmu lepns, ka Rīgai ir šie senie koka nami, un daudzi no tiem ir skaisti atjaunoti – tādu nav pasaules lielajās metrapolēs. Diemžēl Jūrmalā jau tas lielā mērā ir noticis – šīs vietas īpašais šarms sāk pamazām zust,” ar nožēlu piebilst B. Semjonovs.&nbsp;</p><p>“Būtībā šī ēka ir uzbūvēta no jauna, jo veco nevarēja saglabāt, tā bija bojāta kara laikā, divas reizes degusi, ieaugusi kultūrslānī, pamatu nebija. Māja tika precīzi uzmērīta, demontēta, slēģi, ieejas durvis nodotas restauratoru rokās,” stāsta Pēteris Blūms. Šodienas nams ir uzbūvēts no gāzbetona blokiem, nosiltināts, apšūts ar dēļiem. Kāpņu vestibilā kādreizējo logu ailu vietā iebūvēti divi restaurēti sākotnējo logu komplekti, kas noformēti ar fotopanorāmu kā iluzoru skatu pāri Daugavai uz 19. gadsimta Vecrīgu. &nbsp;Izcils retums ir arī restaurētie ēkas ielas fasādi rotājošie unikālie reklāmas uzraksti latviešu, krievu, vācu un somu valodā, kuri radušies jau pirms Pirmā pasaules kara. Tie tika atklāti uz fasādes dēļiem zem apmetuma un, cik zināms, &nbsp;ir vienīgie uz koka gleznotie oriģinālie un restaurētie ielu reklāmas objekti, kas apskatāmi Rīgā.&nbsp;</p><p>Mērķis restaurēt sajūtas</p><p>Pilnībā nomainītas un modernizētas visas iekšējās un ārējās komunikācijas. Arhitekti atzīst, ka ziedošanās vēstures mīlestībai nevar būt bezgalīga un “kvadrātmetri ir kvadrātmetri”, tāpēc mērķis bijis ne tik daudz restaurēt māju, bet restaurēt tā laika sajūtas. “Viss jaunais pārāk ātri dzēš mūsu laika pēdas apkārtnē pat vienas paaudzes ietvaros, tāpēc cilvēki ir izslāpuši pēc pēctecības sajūtas, viņi vēlas izjust laika ritējuma turpinājumu šodienā, un šo saikni sajūtu līmenī esam centušies maksimāli saglabāt. &nbsp;Ne katram šeit patiks, bet vēlēšanās patikt visiem absolūti nebija mūsu mērķis, strādājot pie šī projekta,” uzsver Pēteris Blūms.&nbsp;</p><p>Protams, ir mainījušies akcenti – agrāk parādes puse bija ielas fasāde, taču tagad tā vairāk ir vieta, ko apbrīno tūristi. “Ķēķa puse” ir ieguvusi pavisam citu jēgu – ieejas mājā ir no pagalma puses, šeit stāvēs mājas iedzīvotāju auto, rotaļāsies bērni.&nbsp;Pagalma pusē atjaunots arī bijušais zirgu stallis, izveidojot to par&nbsp;<a href=\"http://kipsalahome.lv/riverside-residence-lv/viengimenes-maja-pagalma-2-stavos\">ērtu divstāvu ģimenes māju</a>. Tur bija palikušas tikai drupas, bet arhitekta Mārtiņa Ņukšas projekts bija saglabājies, un māja tika veidota nevis kā kopija, bet sajūtu turpinājums, kam projekta komanda devusi savu interpretāciju.&nbsp;</p><p>“Mazliet žēl, ka RTU Arhitektūras fakultātes studentiem neiemāca ar labsirdību un labvēlību saprast, lasīt un izjust vēsturisko vidi, jo mums tās ir ārkārtīgi daudz. Ne vienmēr ir jāatstāj redzamas pēdas cita arhitekta darbā, nu, ja tu citādi nevari, maini, pārtaisi – tas nav grēks, bet ar pietāti, lai tas ir esošā turpinājums, nevis pārrāvums. Esmu šādu filosofiju piekopis jau 35 gadus, un tas ir tas, kas man rada gandarījumu dzīvē. Es neesmu bijis ļoti revolucionārs vēsturiskajā vidē, negribu satricinājumus, gribu mierīgu evolūciju, lai, atnākot uz šo vietu, šo māju, tu vari ieraudzīt savu vecvecāku laiku turpinājumu šodien, lai saikne netiek pārrauta. Ja studentiem to mācītu kā arhitektūras filosofiju, tas nekādā mērā netraucētu viņiem radīt jaunus šedevrus, bet viņi nebūtu destruktori,” uzskata Pēteris Blūms.&nbsp;</p><p>Daudz gaismas, mūsdienu kvalitātes un komforta</p><p>“Visu laiku, kopš strādājam pie šī nama, esam pret to izturējušies ne kā pret biznesa projektu, bet kā pret savas dvēseles un pārliecības daļu, tāpēc tagad, kad māja ir nodota potenciālo pircēju vērtējumam, nemaz negribas to pārdot,” atzīst Boriss Semjonovs.Tomēr bizness ir bizness, un Rīgas nekustamo īpašumu tirgus papildinājies ar vēl vienu interesantu un ļoti īpašu piedāvājumu. No atjaunotās mūra daudzstāvu ēkas Ogļu ielas pusē paveras skats uz Daugavu, bet no augšstāvu logiem redzamā Eiropas kultūras mantojuma sarakstā iekļautā Vecrīgas panorāma ir suģestējošs urbānās mākslas šedevrs.&nbsp;</p><p>“Četros nama stāvos izvietoti septiņi trīsistabu dzīvokļi, pārsvarā pa diviem dzīvokļiem katrā. Ceturtajā stāvā izveidots viens ļoti plašs dzīvoklis ar balkoniem uz visām debespusēm. Ļoti interesants ir arī jumta stāva dzīvoklis. Visiem dzīvokļiem ir skats gan uz Daugavu, gan Ķīpsalas ziemeļu daļu. Ēkā ir stikla pakešu trīskārtīgie logi koka rāmī un masīvkoka durvis, augstvērtīga santehnika. Telpu mūsdienīgo interjeru papildina eleganti jūgendstila kamīni un krāsnis ar stikla durvīm, kas domātas ne tikai skaistumam - iespējama arī opcionāla malkas apsilde. Labiekārtota un apzaļumota teritorija, dārzam ir izstrādāts labiekārtojuma un apstādījumu projekts ar kvalitatīviem dekoratīvajiem augiem, mazajām arhitektūras formām - soliņiem, lapenēm, atpūtas vietu bērniem,” stāsta projekta attīstītāju pārstāve Inna Semjonova, īpaši uzsverot arī tādu priekšrocību kā zemi komunālie maksājumi nākotnē, jo visām projekta ēkām ir kopējas inženierkomunikācijas.</p><p>Visi projekta attīstītāji ir vienisprātis, ka šie trīs gadi, kuru laikā skaistā jūgendstila ēka atjaunota, bijuši sarežģīti, bet arī ļoti interesanti, un tagad pat ir mazliet žēl, ka tas ir beidzies. Atbildot uz jautājumu, kādus cilvēkus vēlētos redzēt dzīvojam šajā namā, Boriss Semjonovs saka: “Es gribētu, lai cilvēki nevērtētu šo projektu tikai kā skaistu panorāmu aiz loga, bet lai viņi sajustu to, ko esam ielikuši arī iekšējā interjerā un ergonomiskajos risinājumos, lai viņi ieraudzītu šīs no senajiem laikiem saglabātās un restaurētās lietas. Viņiem jābūt šīs īpašās auras fanātiem – tādiem pašiem kā es! Esmu pateicīgs mūsu komandai – arhitektiem, būvniekiem, restauratoriem, interjeristiem, visiem, kas atbalstījuši šo manu pozīciju.”</p><p>“Mēs neapgalvojam, ka šīs ēkas ir pilnībā restaurētas un ir kā veco laiku Rīgas muzejs. Mēs apgalvojam, ka šajās ēkas ir ne mazums restaurētu detaļu un daudz gaismas, mūsdienu kvalitātes, komforta. Daudz patiesas Ķīpsalas, ” tā arhitekts Pēteris Blūms.&nbsp;</p><p><br>Projekta autori:</p><p>Arhitektu birojs&nbsp;Kroks&nbsp;– arhitekti Dita Lapiņa, Dace Ģine-Rutke, Edmunds Slavinskis, Ieva Ušpele, interjeriste Ināra Cine.</p><p>Arhitektu birojs&nbsp;Konvents&nbsp;– arhitekti Pēteris Blūms, Līva Garkāje, Oto Ozols.&nbsp;</p><p>Labiekārtojums un apstādījumi – birojs&nbsp;<a href=\"http://labiekoki.lv/\">Labie Koki</a>&nbsp;– Edgars Neilands un kolēģi.</p><p>Informācijai:&nbsp;<a href=\"http://kipsalahome.lv/\">http://kipsalahome.lv</a>, tālr. 20202025</p>', '13_34944126283_o.jpg', '', '', '', '', ''),
(59, 'И шарм истории, и современное творение', NULL, 'Gan vēstures šarms, gan mūsdienu elpa', '2019-01-18', '<p>Кипсала – место для центра столицы Латвии во многом уникальное. Расположенный на противоположной Старой Риге стороне реки Даугава, этот район позволяет проживать в непосредственной близости от исторического сердца города и в то же время обеспечивает приватность, тихую и спокойную среду. Вот всем тем, кто высоко ценит такие возможности, компания BIIG и предлагает поселиться на Кипсале – в одном из двух своих жилых проектов. Или в наполненном историческим шармом Riverside Residence, или в ультрамодном по своей архитектуре Zundas darzi.</p><p>Долгое время незаслуженно обделенная вниманием застройщиков, Кипсала в последние 20 лет приобретает новое современное лицо. Год за годом она становится все более престижным и удобным для проживания местом, сохраняющим историческую застройку и прирастающим новыми строениями. Развивается и инфраструктура этого района. Сегодня Кипсала – это не только ряд факультетов Рижского технического университета и полноценный плавательный бассейн, это и престижная международная школа, и детский сад, и теннисные корты, и довольно большой торговый центр. Плюс обустроенный речной пляж, причалы для швартовки яхт, офисные центры, рестораны… Ну и благодаря компании BIIG – место,где теперь располагаются и ждут своих жильцов сразу два новых жилых проекта. Riverside Residence, что на углу улиц Тиклу и Оглю, – комплекс из 11 квартир высокого класса, разместившихся в зеленой части Кипсалы, недалеко от берега Даугавы. Стараниями архитекторов и застройщика проект объединил в себе три разноплановых объема: 4- этажное каменное здание начала XX века, 3-этажный дом с деревянным фасадом – реплику строения XIX века – и абсолютно новый, отдельно стоящий 2-этажный дом. Всех они располагаются по периметру общей благоустроенной территории с собственным садом, игровой площадкой для детей и местом для парковки машин. Первые два дома комплекса имеют ярчайший исторический бэкграунд. Автором каменного здания является латышский архитектор Мартиньш Аугуст Нукша – человек с богатейшим и разносторонним жизненным опытом. За свою жизнь он успел не только реализовать свои проекты в Риге, но и занимал должности главного архитектора российского Севастополя, поработал помощником главного архитектора французского Марселя, а впоследствии почти два десятилетия трудился на дипломатической службе Латвийской Республики. Именно благодаря ему Кипсала и получила возможность гордиться своим единственным зданием югендстиля. Зданием элегантным и приведенным в порядок в наши дни.</p><p>4-этажная часть Riverside Residence отремонтирована до мельчайших подробностей: фасад аутентичен своему первозданному виду, а во внутренней отделке дома сохранены исторические интерьеры, включая выполняющие декоративную и стилистическую роль падуги, розетки и лепнину. На первых трех этажах разместились по две квартиры, из окон которых открывается вид на Даугаву. Весь четвертый, мансардный, этаж отдан под просторное жилье площадью 130 кв. м – единственное на этом этаже. Попасть в квартиры, за исключением квартиры под номером 2, можно со двора. Вход в подъезды контролирует система безопасности, включающая в себя домофон, а также электронные чипы. Сам подъезд полностью отреставрирован, и для удобства жильцов в нем поставлен современный лифт. Как непосредственно в квартирах, так и в помещениях общего пользования установлены окна с трехслойными деревянными стеклопакетами. Вторая, и уже 3-этажная, часть Riverside Residence примостилась справа от каменной. Она представляет собой абсолютно новое, но выполненное под копирку с исторического деревянного предшественника 3-этажное здание на четыре квартиры. В латвийских архивах сохранились изображения этого дома: нынешняя постройка своими визуальными очертаниями соответствует им один в один.На доме даже воспроизведены имевшие место более 100 лет назад надписи на латышском, русском, немецком и финском языках. «Жилые дома на углу улиц Тиклу и Оглю официально не являются памятниками архитектуры, но это не помешало заказчику проекта восстановить их с тщательностью, уважением и вниманием, которые оказали бы честь возрождению любого архитектурного шедевра»,–такую оценку проекту дал известный латвийский архитектор Петерис Блумс, участвовавший в возрождении Riverside Residence. Его слова нашли свое подтверждение на конкурсе «Ежегодный приз латвийского строительства – 2017», удостоившего комплекс почетной грамоты «За детальную реставрацию исторического здания 2017 года». Все квартиры Riverside Residence доступны покупателям с полной отделкой. Это подразумевает не только покраску стен и паркетные полы,но и установку дверей, выполненных из массивного дерева, оборудование ванных комнат и санузлов сантехникой Villeroy&amp;Bosch.Во всех квартирах первого этажа восстановлены исторические камины. «Площадь квартир в Riverside Residence начинается от 80 кв.м.Все имеющиеся варианты жилья–с двумя спальнями.Мы не стали подробно останавливаться на отдельном 2-этажномдоме, входящем в этот проект,лишь по той причине, что он был выкуплен самым первым.Воочию познакомиться с преимуществами и достоинствами проекта Riverside Residence можно на наших днях открытых дверей, ближайший из которых планируем провести в пятницу-субботу 11-12 мая.В эти дни потенциальным покупателям будут представлены бонусные предложения на квартиры со скидкой в 25 000 евро. Впрочем, в индивидуальном порядке показы организуем по запросу клиентов каждый день.В том числе и для иностранцев,интересующихся возможностью получения латвийского ВНЖ через покупку недвижимости: кадастровая стоимость нашего жилья позволяет это делать»,–рассказывает ассистент руководителя проектов BIIG Инна Семенова.И отмечает, что квартиры в этом комплексе, впрочем, как и в других проектах, которые развивает компания, очень экономичные с точки зрения своего повседневного содержания. Zundas Darzi – это еще один, более клубный, жилой проект компании BIIG на Кипсале. Он располагается в глубине острова, на ул. Звейниеку, 24, и в нем всего шесть квартир. 3-этажная новостройка, выполненная в современной архитектурной стилистике кубических объемов, имеет собственную огороженную территорию площадью 1700 кв. м. Внутреннее пространство всех квартир организовано таким образом, что позволяет практически из каждой комнаты наслаждаться видами прилегающего сада. В нем ландшафтные дизайнеры постарались воплотить воспоминания из детства о бабушкином саде, с его яблонями, вишнями, кустами малины, смородины и дикой земляники.</p><p>Жилая площадь квартир в «урбанистическомдоме с бабушкиным садом» – от 70 до 150 кв. м. Но на данный момент из шести квартир комплекса покупателям доступна лишь половина, остальные выкуплены. Две из остающихся свободными квартир располагаются на первом этаже и имеют собственные террасы с выходом во двор, квартира на втором этаже порадует своих будущих владельцев сразу двумя просторными балконами общей площадью 17,8 кв. м. Все три квартиры имеют полную внутреннюю отделку, ванные комнаты оборудованы сантехникой и системами подогрева полов. Положен паркетный пол из высококачественного дерева, установлены двухкамерные стеклопакеты с алюминиевой отделкой, выведена система кондиционирования воздуха и сигнализации. Отопление – газовое. Говоря о самом доме, нужно упомянуть, что в нем есть современный лифт Schindler. На территории нашлось место и для зон отдыха, игр детей и барбекю, а также для парковки 11 машин. Весь периметр комплекса огорожен, а доступ машин осуществляется через автоматические ворота. Зеленое мышление клубного дома Zundas Darzi предоставляет прекрасную возможность жить в гармонии с природой! Познакомиться с этим проектом все желающие также смогут 11- 12 мая в день открытых дверей, который для потенциальных покупателей делает сам застройщик – компания BIIG. Приходите!</p><p>По вопросам просмотра и приобретения квартир в проектах Riverside Residence И Zundas darzi обращайтесь:</p><p>+371 20202025;</p><p>&nbsp;info@kipsalahome.lv;</p><p>&nbsp;skype: sia.patek .</p>', NULL, '<p>Ķīpsala – unikāla Latvijas galvaspilsētas vieta daudzējādā ziņā. Tā ir vieta, kas atrodas Daugavas kreisajā krastā pretī Vecrīgai un ļauj cilvēkam pietuvoties pilsētas vēsturiskajai sirdij, vienlaikus nodrošinot privātumu, klusu un mierīgu vidi. Visiem, kuri augstu novērtē šādas iespējas, kompānija BIIG piedāvā mājvietu Ķīpsalā – vienā no diviem šīs kompānijas daudzdzīvokļu projektiem. Var izvēlēties Riverside Residence, tā valdzina ar savu neatvairāmo vēsturisko šarmu, vai Zundas dārzu – ultramodernās arhitektūras paraugu.</p><p>Apbūvētāji Ķīpsalu ilgu laiku bija nepelnīti aizmirsuši, taču pēdējos divdesmit gados šī vieta iegūst jaunus vaibstus un modernu veidolu. Ar katru gadu tā kļūst aizvien prestižāka un ērtāka dzīvošanai,saglabājot vēsturisko apbūvi un parādoties jaunām ēkām. Attīstās arī šī rajona infrastruktūra. Šodienas Ķīpsala nav tikai Rīgas Tehniskās universitātes fakultātes un labs peldbaseins, bet arī prestiža starptautiska skola un bērnudārzs, tenisa korti un pietiekami liels tirdzniecības centrs. Arī labiekārtota pludmale pie upes, jahtu piestātnes, biroju centri, restorāni un daudz kas cits. Pateicoties kompānijai BIIG, Ķīpsala šobrīd ir vieta, kur uzbūvēti un jau gaida savus iemītniekus divi jauni dzīvojamie projekti. Riverside Residence atrodas Tīklu un Ogļu ielas krustojumā, tā ir zaļākā Ķīpsalas vieta netālu no Daugavas krasta. Kompleksā pieejami 11 augstākās klases dzīvokļi. Arhitektu un apbūvētāju profesionalitāte un centieni ir izveidojuši projektu, kas apvieno trīs dažādu veidu un laiku ēkas: 20.gadsimta sākuma četrstāvu akmens ēka, 19.gadsimta būves reprodukcija – trīsstāvu nams ar koka fasādi − un pilnīgi jauna, atsevišķa divstāvu māja. Visas ēkas izvietotas pa perimetru, tām ir kopēja labiekārtota teritorija ar savu dārzu, bērnu spēļu laukumu un automobiļu stāvvietu. Kompleksa pirmajās divās mājās ir saglabāts spilgts vēsturisks mantojums. Akmens ēkas autors ir latviešu arhitekts Mārtiņš Augusts Ņukša – cilvēks ar bagātu un daudzveidīgu dzīves pieredzi. Savas dzīves laikā viņš ir spējis īstenot ne tikai savus projektus Rīgā, viņš arī pildīja galvenā arhitekta amatu Krievijas pilsētā Sevastopolē, ir strādājis par Francijas pilsētas Marseļas galvenā arhitekta palīgu un gandrīz divas desmitgades darbojies Latvijas Republikas diplomātiskajā dienestā. Tieši pateicoties Mārtiņam Augustam Ņukša, Ķīpsala var lepoties ar savu vienīgo jūgendstila ēku. Mūsdienās šī ir atjaunota eleganta ēka. Riverside Residence četrstāvu ēkas remonta darbos ir atjaunotas vismazākās detaļas: fasāde ir autentiska, ievērots sākotnējais izskats, mājas iekšējā apdarē saglabāts vēsturiskais interjers, tajā skaitā patinas, rozetes un citi elementi, kam ir dekoratīva un stilistiska funkcija. Katrā no trijiem stāviem ir iekārtoti divi dzīvokļi, no kuriem paveras skats uz Daugavu. Viss ceturtais stāvs- mansards, tas ir paredzēts plašam mājoklim (tā platība ir 130 m2 ), turklāt tas ir vienīgais šajā stāvā. Ieeja dzīvokļos ir no pagalma puses, izņemot 2.dzīvokli. Ieeju mājas kāpņu telpā kontrolē drošības sistēma ar namzini un elektroniskajām kartēm. Kāpņu telpa ir pilnībā restaurēta, un mājas iedzīvotāju ērtībām ierīkots moderns lifts. Gan dzīvokļos, gan koplietošanas telpās ielikti trīsslāņu koka stikla pakešu logi. Otra, trīsstāvu Riverside Residence ēkas daļa, atrodas akmens nama labajā pusē. Tā ir pilnīgi jauna, bet uzbūvēta, kopējot vēsturisko koka priekšteci, tā ir trīsstāvu ēka ar četriem dzīvokļiem. Latvijas arhīvos ir saglabājušies šī nama attēli: pašreizējā būve ar savu vizuālo izskatu simtprocentīgi atbilst attēlos redzamajam namam. Uz mājas fasādes pat atjaunoti simtgadīgi uzraksti, kuri bijuši latviešu, krievu, vācu un somu valodā. „Dzīvojamās mājas uz Tīklu un Ogļu ielas stūra oficiāli nav arhitektūras pieminekļi, taču tas netraucēja projekta pasūtītājam atjaunot tās rūpīgi, cieņpilni un uzmanīgi, tas ir gods, ar kādu būtu atjaunojams jebkurš arhitektūras šedevrs,” tādu vērtējumu projektam ir devis pazīstamais latviešu arhitekts Pēteris Blūms, kurš piedalījās Riverside Residence atjaunošanā. Viņa vārdi guva apstiprinājumu konkursā Latvijas Būvniecības Gada balva 2017, kur projekts ieguva atzinības rakstu par Detalizētu vēsturiskās ēkas restaurāciju 2017.gadā. Visi Riverside Residence dzīvokļi ir pieejami ar pilno iekšējo apdari. Tā ir ne tikai sienu nokrāsošana un parketa grīdas ieklāšana, bet arī masīvkoka durvju uzstādīšana, vannasistabu un sanitāro mezglu aprīkojuma nodrošināšana ar Villeroy&amp;Bosch sanitāro tehniku. Visos pirmā stāva dzīvokļos ir atjaunoti vēsturiskie kamīni. „Riverside Residence dzīvokļu platība ir dažāda,sākot no 80 kvadrātmetriem. Visos mājokļos ir divas guļamistabas. Mēs apzināti nepievērsām uzmanību atsevišķi esošai divstāvu mājai, kura arī ietilpst šajā projektā (tā tika nopirkta pirmā). Personīgi iepazīties ar projekta Riverside Residence priekš rocībām un ieguvumiem varēs mūsu Atvērto durvju dienās. Tuvākās Atvērto durvju dienas plānojam piektdien un sestdien, tas ir, 11. un 12. maijā. Šajās dienās potenciālajiem pircējiem tiks piedāvāti dzīvokļa iegādes bonusi. Gribu piebilst, ka, respektējot klientu pieprasījumu, katru dienu organizējam dzīvokļu apskati arī individuāli. Tajā skaitā arī ārzemniekiem, kuri interesējas par iespēju iegūt uzturēšanās atļauju Latvijā, nopērkot šeit nekustamo īpašumu: mūsu mājokļa kadastra vērtība to ļauj darīt,” tā stāsta BIIG projektu vadītāja asistente Inna Semjonova. Un atzīmē, ka dzīvokļi šajā un citos kompleksa projektos, kurus attīsta kompānija, ir ļoti energoefektīvi ikdienas uzturēšanā. Zundas dārzs – tas ir vēl viens, taču vēl “zaļāks” BIIG dzīvojamais kompless Ķīpsalā. Tas atrodas salas tālākajā daļā, proti, Zaļenieku ielā 24, un tajā ir tikai seši dzīvokļi. Trīsstāvu jaunbūvei, kura veidota, izmantojot modernās kubisma arhitektūras stilistikas elementus, pieder 1700 kvadrātmetru liela, iežogota teritorija. Visu dzīvokļu iekšējā telpa veidota tā, lai praktiski no katras istabas būtu iespēja baudīt blakus esošā dārza ainavu. Ainavu dizaineri centās atspoguļot dārzā visas bērnības atmiņas par vecmāmiņas dārzu ar ābelēm, ķiršiem, aveņu, jāņogu krūmiem un ar meža zemenēm. Dzīvojamā dzīvokļu platība urbānajā mājā ar vecmāmiņas dārzu – no 70 līdz 150 kvadrātmetriem. Bet pašlaik no sešiem kompleksa dzīvokļiem pircējiem ir pieejami tikai trīs, pārējie ir izpirkti. Divi no piedāvājumā esošiem brīvajiem dzīvokļiem trodas pirmajā stāvā, un tiem ir savas privātās terases ar izeju uz pagalmu. Dzīvoklis otrajā stāvā iepriecinās savus nākamos īpašniekus ar diviem plašiem balkoniem, kuru kopējā platība ir 17,8 kvadrātmetri. Visiem trim dzīvokļiem ir pilnā iekšējā apdare, vannasistabas ir aprīkotas ar sanitāro tehniku, un tām ir apsildāmās grīdas. Istabās ir parketa grīdas segums no augstvērtīga koka, ir ielikti divkameru stikla pakešu logi ar alumīnija apdari, ierīkota gaisa ventilācijas sistēma un signalizācija. Ir gāzes apkure. Runājot par māju, viena no tās vērtībām ir moderns Schindler lifts. Teritorijā ir atrasta vieta gan atpūtai, gan bērnu rotaļlaukumam, gan vieta barbekjū gatavošanai, kā arī stāvvieta 11 automašīnām. Viss teritorijas perimetrs ir nožogots, mašīnas iebraukšanai izmanto automātiskos vārtus. Zundas dārza mājas zaļā domāšana sniedz lielisku iespēju dzīvot saskaņā un harmonijā ar dabu! Iepazīties ar šo projektu varēs visi interesenti Atvērto durvju dienā 11. un 12. maijā, potenciālajiem klientiem tās rīko pats būvnieks – kompānija BIIG. Laipni aicināti!&nbsp;</p><p>Par dzīvokļu apskates un iegādes jautājumiem riverside residence un zundas dārzsi projektos interesēties:&nbsp;</p><p>+371 20202025;&nbsp;</p><p>info@kipsalahome.lv;&nbsp;</p><p>skype:sia.patek.</p>', '12_35366228470_o.jpg', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `contact`
--

CREATE TABLE `contact` (
  `idContact` int(11) NOT NULL,
  `Contact_Phone` varchar(50) DEFAULT NULL,
  `Contact_Email` varchar(50) DEFAULT NULL,
  `Contact_Address` varchar(50) DEFAULT NULL,
  `Contact_Mobilephone` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `contact`
--

INSERT INTO `contact` (`idContact`, `Contact_Phone`, `Contact_Email`, `Contact_Address`, `Contact_Mobilephone`) VALUES
(1, '+371 2020 2025', 'patekdevelopment@gmail.com', 'Zvejnieku iela 24, Riga, Latvia', '+371 2924 2424');

-- --------------------------------------------------------

--
-- Структура таблицы `features`
--

CREATE TABLE `features` (
  `idFeature` int(11) NOT NULL,
  `Feature_Title_Ru` varchar(255) DEFAULT NULL,
  `Feature_Title_En` varchar(255) DEFAULT NULL,
  `Feature_Title_Lv` varchar(255) DEFAULT NULL,
  `Appartment_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `features`
--

INSERT INTO `features` (`idFeature`, `Feature_Title_Ru`, `Feature_Title_En`, `Feature_Title_Lv`, `Appartment_ID`) VALUES
(523, '', '', '', 69),
(524, '', '', '', 69),
(525, '', '', '', 69),
(526, '', '', '', 69),
(527, '', '', '', 69),
(528, '', '', '', 69),
(535, '', '', '', 71),
(536, '', '', '', 71),
(537, '', '', '', 71),
(538, '', '', '', 71),
(539, '', '', '', 71),
(540, '', '', '', 71),
(541, '', '', '', 72),
(542, '', '', '', 72),
(543, '', '', '', 72),
(544, '', '', '', 72),
(545, '', '', '', 72),
(546, '', '', '', 72),
(553, '', '', '', 74),
(554, '', '', '', 74),
(555, '', '', '', 74),
(556, '', '', '', 74),
(557, '', '', '', 74),
(558, '', '', '', 74),
(565, '', '', '', 76),
(566, '', '', '', 76),
(567, '', '', '', 76),
(568, '', '', '', 76),
(569, '', '', '', 76),
(570, '', '', '', 76),
(571, '', '', '', 77),
(572, '', '', '', 77),
(573, '', '', '', 77),
(574, '', '', '', 77),
(575, '', '', '', 77),
(576, '', '', '', 77),
(577, '', '', '', 78),
(578, '', '', '', 78),
(579, '', '', '', 78),
(580, '', '', '', 78),
(581, '', '', '', 78),
(582, '', '', '', 78),
(589, '', '', '', 80),
(590, '', '', '', 80),
(591, '', '', '', 80),
(592, '', '', '', 80),
(593, '', '', '', 80),
(594, '', '', '', 80),
(595, '', '', '', 81),
(596, '', '', '', 81),
(597, '', '', '', 81),
(598, '', '', '', 81),
(599, '', '', '', 81),
(600, '', '', '', 81),
(601, '', '', '', 82),
(602, '', '', '', 82),
(603, '', '', '', 82),
(604, '', '', '', 82),
(605, '', '', '', 82),
(606, '', '', '', 82),
(607, '', '', '', 83),
(608, '', '', '', 83),
(609, '', '', '', 83),
(610, '', '', '', 83),
(611, '', '', '', 83),
(612, '', '', '', 83),
(613, '', '', '', 84),
(614, '', '', '', 84),
(615, '', '', '', 84),
(616, '', '', '', 84),
(617, '', '', '', 84),
(618, '', '', '', 84),
(619, '', '', '', 85),
(620, '', '', '', 85),
(621, '', '', '', 85),
(622, '', '', '', 85),
(623, '', '', '', 85),
(624, '', '', '', 85),
(625, '', '', '', 86),
(626, '', '', '', 86),
(627, '', '', '', 86),
(628, '', '', '', 86),
(629, '', '', '', 86),
(630, '', '', '', 86),
(631, '', '', '', 87),
(632, '', '', '', 87),
(633, '', '', '', 87),
(634, '', '', '', 87),
(635, '', '', '', 87),
(636, '', '', '', 87),
(637, '', '', NULL, 88),
(638, '', '', NULL, 88),
(639, '', '', NULL, 88),
(640, '', '', NULL, 88),
(641, '', '', NULL, 88),
(642, '', '', NULL, 88),
(649, '', '', NULL, 90),
(650, '', '', NULL, 90),
(651, '', '', NULL, 90),
(652, '', '', NULL, 90),
(653, '', '', NULL, 90),
(654, '', '', NULL, 90);

-- --------------------------------------------------------

--
-- Структура таблицы `our_team`
--

CREATE TABLE `our_team` (
  `team_id` int(11) NOT NULL,
  `employee_name_en` varchar(255) NOT NULL,
  `employee_name_lv` varchar(255) NOT NULL,
  `employee_name_ru` varchar(255) NOT NULL,
  `position_lv` varchar(255) NOT NULL,
  `position_en` varchar(255) NOT NULL,
  `position_ru` varchar(255) NOT NULL,
  `employee_photo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `our_team`
--

INSERT INTO `our_team` (`team_id`, `employee_name_en`, `employee_name_lv`, `employee_name_ru`, `position_lv`, `position_en`, `position_ru`, `employee_photo`) VALUES
(17, 'Kaspars Pile', 'Kaspars Pile', 'Kaspars Pile', 'Projektu vadītājs, apsaimniekošanas nodaļas vadītājs', 'Projektu vadītājs, apsaimniekošanas nodaļas vadītājs', 'Projektu vadītājs, apsaimniekošanas nodaļas vadītājs', 'Kaspars.jpg'),
(18, 'Māris Zondaks', 'Māris Zondaks', 'Māris Zondaks', 'Darbu aizsardzības speciālists', 'Darbu aizsardzības speciālists', 'Darbu aizsardzības speciālists', 'Māris - darbu aizsardzības specialists.jpg'),
(20, 'Normunds Putniņš', 'Normunds Putniņš', 'Normunds Putniņš', 'Būvdarbu vadītājs', 'Būvdarbu vadītājs', 'Būvdarbu vadītājs', 'Normunds2.jpg'),
(21, 'Žanna Pahomova', 'Žanna Pahomova', 'Žanna Pahomova', 'Juriste', 'Juriste', 'Juriste', 'anna - juriste.jpg'),
(22, 'Pēteris Blums', 'Pēteris Blums', 'Pēteris Blums', 'Arhitekts', 'Arhitekts', 'Arhitekts', 'Peteris Blums - arhitekts.jpg'),
(24, 'Rimars Krieviņš', 'Rimars Krieviņš', 'Rimars Krieviņš', 'Arhitekts', 'Arhitekts', 'Arhitekts', 'Rimars Krieviņ.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `project`
--

CREATE TABLE `project` (
  `id` int(11) NOT NULL,
  `Project_Name_en` varchar(255) DEFAULT NULL,
  `Project_Description_en` text NOT NULL,
  `Project_Image` varchar(255) NOT NULL,
  `Project_Name_ru` varchar(255) NOT NULL,
  `Project_Name_lv` varchar(255) NOT NULL,
  `Project_Description_ru` text NOT NULL,
  `Project_Description_lv` text NOT NULL,
  `image_alt` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `project`
--

INSERT INTO `project` (`id`, `Project_Name_en`, `Project_Description_en`, `Project_Image`, `Project_Name_ru`, `Project_Name_lv`, `Project_Description_ru`, `Project_Description_lv`, `image_alt`) VALUES
(1, 'Zundas Darzi', 'The project is a play of architectural masterpieces from different centuries - a combination of beautiful opposites of styles. Riverside consists of 11 apartments with an area of 80 m2 – 140 m2, and a two-storey private house. The windows command a lovely view of the Old Riga, the port and the Riga Castle that is the residence of the President. The residential complex is built with the highest degree of construction and interior performance; a particular attention was paid to finishing materials and details of interior.', 'e51cce84-4ece-43d3-b857-95d54d932de8.jpg', 'Zundas Darzi', 'Zundas Darzi', 'Проект представляет игру архитектур разных веков, совмещение прекрасных стилевых противоположностей. Riverside cостоит из 11 апартаментов площадью 80 – 140 кв.м и двухэтажного частного дома. Из окон открывается прекрасный вид на Старую Ригу, порт и дворец Президента. Жилой комплекс выполнен с высшей степенью исполнения строительства и внутреннего интерьера, особое внимание было уделено отделочным материалами и деталям интерьера.', 'Projekts, pēc būtības ir dažādu gadsimtu arhitektūru spēle, lielisko stilu pretējību savienojumu. „Riverside” sastāv no 11 apartamentiem, kuru platība ir 80 – 140 kvadrātmetri, un no divstāvu privātmājas. Pa logiem paveras brīnišķīgs skats uz Vecrīgu, ostu un Prezidenta pili. Dzīvojamais komplekss ir uzbūvēts, īstenojot būvniecības un iekšējā interjera visaugstāko kvalitātes pakāpi; sevišķa uzmanība tika pievērsta interjera apdares materiāliem un detaļām.', 'Zundas Darzi Project Image'),
(2, 'Riverside', 'In 2017, the construction of apartment building was completed in Kipsala, named „Zundas dārzi” that can be translated as: “Urban House with Grandma’s Garden”. The project consists of a three-storey house with 6 apartments and of area ranging from 70 m2 to 150 m2, as well as a land plot of 1, 700 m2.  Each apartment on the first floor has a wooden porch with access to the green terrace.', '5ab5c850-8bf6-41ba-aeb3-15667661e396.jpg', 'Riverside', 'Riverside', 'В 2017 году было завершили стоительство  квартирного дома в Кипсале, с названием «Zundas dārzi», что в переводе звучит:«Урбанистический дом с бабушкиным садом». Проект состоит из трехэтажного дома на 6 квартир, площадью от 70 до 150 м2 и земельного участка 1700 м2.  У каждого апартамента на первом этаже есть деревянное крыльцо  с выходом на зелёную терассу.', '2017. gadā tika pabeigta dzīvokļu mājas būvniecība Ķīpsalā, ar nosaukumu „Zundas Dārzi”, kas tulkojumā skan: „Urbānistiskā māja ar vecmāmiņas dārzu”. Projekts sastāv no trīsstāvu mājas, kurā ir paredzēti 6 dzīvokļi, kuru platība ir no 70 līdz 150m2 un 1700 m2 zemes gabala. Pirmajā stāvā katriem apartamentiem ir koka lievenis ar izeju uz zaļo terasi.', 'Riverside Project Image');

-- --------------------------------------------------------

--
-- Структура таблицы `property`
--

CREATE TABLE `property` (
  `idProperty` int(11) NOT NULL,
  `Property_Type` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `property`
--

INSERT INTO `property` (`idProperty`, `Property_Type`) VALUES
(1, 'For sale'),
(2, 'For rent');

-- --------------------------------------------------------

--
-- Структура таблицы `propertydetails`
--

CREATE TABLE `propertydetails` (
  `idPropertyDetails` int(11) NOT NULL,
  `PD_Size` int(11) DEFAULT NULL,
  `PD_Rooms` int(11) DEFAULT NULL,
  `PD_PriceUnit` varchar(50) DEFAULT NULL,
  `PD_Bathrooms` int(11) DEFAULT NULL,
  `PD_Floor` varchar(5) DEFAULT NULL,
  `PD_Parking` int(11) DEFAULT NULL,
  `PD_AptFloor` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `propertydetails`
--

INSERT INTO `propertydetails` (`idPropertyDetails`, `PD_Size`, `PD_Rooms`, `PD_PriceUnit`, `PD_Bathrooms`, `PD_Floor`, `PD_Parking`, `PD_AptFloor`) VALUES
(83, 255, 3, '1', 3, '2', 1, 4),
(84, 1, 1, '', 0, '0', 0, 0),
(85, 23, 5, '3', 1, '5', 3, 1),
(88, 12, 234, '324', 32, '5', 2, 1),
(102, 34, 0, '', 0, '', 0, 0),
(113, 0, 0, '', 0, '', 0, 0),
(118, 0, 0, '', 0, '', 0, 0),
(119, 0, 0, '', 0, '', 0, 0),
(120, 0, 0, '', 0, '', 0, 0),
(121, 0, 0, '', 0, '', 0, 0),
(122, 0, 0, '', 0, '', 0, 0),
(123, 0, 0, '', 0, '', 0, 0),
(124, 0, 0, '', 0, '', 0, 0),
(125, 0, 0, '', 0, '', 0, 0),
(126, 0, 0, '', 0, '', 0, 0),
(127, 0, 0, '', 0, '', 0, 0),
(128, 0, 0, '', 0, '', 0, 0),
(129, 0, 0, '', 0, '', 0, 0),
(130, 0, 0, '', 0, '', 0, 0),
(131, 0, 0, '', 0, '', 0, 0),
(132, 0, 0, '', 0, '', 0, 0),
(133, 0, 0, '', 0, '', 0, 0),
(149, 0, 0, '', 0, '', 0, 0),
(150, 0, 0, '', 0, '', 0, 0),
(151, 0, 0, '', 0, '', 0, 0),
(152, 0, 0, '', 0, '', 0, 0),
(153, 0, 0, '', 0, '', 0, 0),
(154, 0, 0, '', 0, '', 0, 0),
(155, 0, 0, '', 0, '', 0, 0),
(156, 0, 0, '', 0, '', 0, 0),
(157, 0, 0, '', 0, '', 0, 0),
(158, 0, 0, '', 0, '', 0, 0),
(159, 0, 0, '', 0, '', 0, 0),
(160, 0, 0, '', 0, '', 0, 0),
(161, 0, 0, '', 0, '', 0, 0),
(162, 0, 0, '', 0, '', 0, 0),
(163, 0, 0, '', 0, '', 0, 0),
(164, 0, 0, '', 0, '', 0, 0),
(165, 0, 0, '', 0, '', 0, 0),
(168, 69, 2, '', 1, '4', 0, 1),
(170, 72, 2, '', 1, '4', 0, 1),
(171, 150, 4, '', 2, '4', 0, 2),
(173, 68, 2, '', 1, '4', 0, 1),
(175, 79, 2, '', 2, '4', 0, 1),
(176, 87, 3, '', 1, '4', 0, 1),
(177, 85, 3, '', 2, '4', 0, 2),
(179, 84, 3, '', 0, '4', 0, 3),
(180, 78, 3, '', 0, '4', 0, 3),
(181, 128, 3, '', 1, '4', 0, 4),
(182, 79, 3, '', 1, '4', 0, 2),
(183, 100, 3, '', 1, '3', 0, 1),
(184, 104, 3, '', 1, '3', 0, 1),
(185, 99, 3, '', 1, '3', 0, 2),
(186, 137, 3, '', 1, '3', 0, 3),
(187, 85, 3, '', 2, '4', 0, 2),
(189, 79, 3, '', 1, '4', 0, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `slider_title_en` varchar(255) NOT NULL,
  `slider_title_ru` varchar(255) NOT NULL,
  `slider_title_lv` varchar(255) NOT NULL,
  `slider_content_en` text NOT NULL,
  `slider_content_ru` varchar(255) NOT NULL,
  `slider_content_lv` varchar(255) NOT NULL,
  `slider_image` varchar(255) NOT NULL,
  `image_alt` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `slider`
--

INSERT INTO `slider` (`id`, `slider_title_en`, `slider_title_ru`, `slider_title_lv`, `slider_content_en`, `slider_content_ru`, `slider_content_lv`, `slider_image`, `image_alt`) VALUES
(7, 'Slider Title 1', 'Заголовок Ру 3', 'Title LV 3', 'Slider Content Goes Here', 'Content Ru', 'Slider Content LV', 'kipsala-island.jpg', 'Alt tag 1'),
(37, 'En Slider Title', 'New slide', '', 'Short Content EN', 'Slide content', '', 'cam00317b0-pr0222-still07_35622506631_o.jpg', 'Alt tag for slider'),
(78, 'New Title!!', '', '', 'cotnenwta', '', '', 'cam00317b0-pr0225-still20_35084003343_o.jpg', 'ALT TAG'),
(79, 'Title 7', '', '', 'Some content', '', '', '20170801_114253.jpg', '');

-- --------------------------------------------------------

--
-- Структура таблицы `team_content`
--

CREATE TABLE `team_content` (
  `id` int(11) NOT NULL,
  `content_en` text NOT NULL,
  `content_ru` text NOT NULL,
  `content_lv` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `team_content`
--

INSERT INTO `team_content` (`id`, `content_en`, `content_ru`, `content_lv`) VALUES
(1, 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Illum, ab?', 'Lorem Ipsum - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века.', 'Lorem Ipsum – tas ir teksta salikums, kuru izmanto poligrāfijā un maketēšanas darbos. Lorem Ipsum ir kļuvis par vispārpieņemtu teksta aizvietotāju kopš 16.');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `about_us_main_page`
--
ALTER TABLE `about_us_main_page`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `apartment_gallery`
--
ALTER TABLE `apartment_gallery`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idAppartment` (`apartment_id`);

--
-- Индексы таблицы `appartment`
--
ALTER TABLE `appartment`
  ADD PRIMARY KEY (`idAppartment`),
  ADD KEY `fk_Appartment_Property1` (`Property_ID`),
  ADD KEY `fk_Appartment_Project1` (`Project_ID`),
  ADD KEY `fk_Appartment_PropertyDetails1` (`PropDet_ID`);

--
-- Индексы таблицы `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`idContact`);

--
-- Индексы таблицы `features`
--
ALTER TABLE `features`
  ADD PRIMARY KEY (`idFeature`),
  ADD KEY `fk_Feature_Appartment` (`Appartment_ID`);

--
-- Индексы таблицы `our_team`
--
ALTER TABLE `our_team`
  ADD PRIMARY KEY (`team_id`);

--
-- Индексы таблицы `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `property`
--
ALTER TABLE `property`
  ADD PRIMARY KEY (`idProperty`);

--
-- Индексы таблицы `propertydetails`
--
ALTER TABLE `propertydetails`
  ADD PRIMARY KEY (`idPropertyDetails`);

--
-- Индексы таблицы `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `team_content`
--
ALTER TABLE `team_content`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `about`
--
ALTER TABLE `about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `about_us_main_page`
--
ALTER TABLE `about_us_main_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `apartment_gallery`
--
ALTER TABLE `apartment_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=300;

--
-- AUTO_INCREMENT для таблицы `appartment`
--
ALTER TABLE `appartment`
  MODIFY `idAppartment` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT для таблицы `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT для таблицы `contact`
--
ALTER TABLE `contact`
  MODIFY `idContact` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `features`
--
ALTER TABLE `features`
  MODIFY `idFeature` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=655;

--
-- AUTO_INCREMENT для таблицы `our_team`
--
ALTER TABLE `our_team`
  MODIFY `team_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT для таблицы `project`
--
ALTER TABLE `project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `property`
--
ALTER TABLE `property`
  MODIFY `idProperty` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `propertydetails`
--
ALTER TABLE `propertydetails`
  MODIFY `idPropertyDetails` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=190;

--
-- AUTO_INCREMENT для таблицы `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `apartment_gallery`
--
ALTER TABLE `apartment_gallery`
  ADD CONSTRAINT `apartment_gallery_ibfk_1` FOREIGN KEY (`apartment_id`) REFERENCES `appartment` (`idAppartment`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `appartment`
--
ALTER TABLE `appartment`
  ADD CONSTRAINT `fk_Appartment_Project1` FOREIGN KEY (`Project_ID`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_Appartment_Property1` FOREIGN KEY (`Property_ID`) REFERENCES `property` (`idProperty`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Appartment_PropertyDetails1` FOREIGN KEY (`PropDet_ID`) REFERENCES `propertydetails` (`idPropertyDetails`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `features`
--
ALTER TABLE `features`
  ADD CONSTRAINT `fk_Feature_Appartment` FOREIGN KEY (`Appartment_ID`) REFERENCES `appartment` (`idAppartment`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
