-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Дек 14 2018 г., 10:38
-- Версия сервера: 10.1.30-MariaDB
-- Версия PHP: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `kipsala_db`
--

-- --------------------------------------------------------

--
-- Структура таблицы `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `about_content_left_en` text NOT NULL,
  `about_content_left_ru` text NOT NULL,
  `about_content_left_lv` text NOT NULL,
  `about_content_right_en` text NOT NULL,
  `about_content_right_ru` text NOT NULL,
  `about_content_right_lv` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `about`
--

INSERT INTO `about` (`id`, `about_content_left_en`, `about_content_left_ru`, `about_content_left_lv`, `about_content_right_en`, `about_content_right_ru`, `about_content_right_lv`) VALUES
(1, 'Жилой проект «RiversideResidence» расположен в уютной зеленой части острова Кипсала на берегу реки Даугава. Проект с внутренней территорией cостоит 11 апартаментов и двухэтажного частного дома для одной семьи. Площади квартир от 80 – 140 кв.м. Из окон открывается прекрасный вид на Старую Ригу, порт и дворец Президента!', 'Жилой проект «RiversideResidence» расположен в уютной зеленой части острова Кипсала на берегу реки Даугава. Проект с внутренней территорией cостоит 11 апартаментов и двухэтажного частного дома для одной семьи. Площади квартир от 80 – 140 кв.м. Из окон открывается прекрасный вид на Старую Ригу, порт и дворец Президента.', '', 'Проект представляет игру архитектур разных веков, совмещение прекрасных стилевых противоположностей. Жилой комплекс выполнен с высшей степенью исполнения строительства и внутреннего интерьера, особое внимание было уделено отделочным материалами и деталям интерьера. Проект сдан в эксплуатацию летом 2017 года.', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `about_us_main_page`
--

CREATE TABLE `about_us_main_page` (
  `id` int(11) NOT NULL,
  `about_title_en` varchar(255) NOT NULL,
  `about_title_lv` varchar(255) NOT NULL,
  `about_title_ru` varchar(255) NOT NULL,
  `sub_title_en` varchar(255) NOT NULL,
  `sub_title_ru` varchar(255) NOT NULL,
  `sub_title_lv` varchar(255) NOT NULL,
  `content_en` text NOT NULL,
  `content_lv` text NOT NULL,
  `content_ru` text NOT NULL,
  `about_us_image` varchar(255) NOT NULL,
  `alt_tag` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `about_us_main_page`
--

INSERT INTO `about_us_main_page` (`id`, `about_title_en`, `about_title_lv`, `about_title_ru`, `sub_title_en`, `sub_title_ru`, `sub_title_lv`, `content_en`, `content_lv`, `content_ru`, `about_us_image`, `alt_tag`) VALUES
(1, 'About Title  En', 'Title Lv', 'Title Ru', 'Sub Title EN', 'Sub title ru', 'Sub Title LV', 'Hall entrance UPVC double glazed door to the front, laminate flooring, storage cupboard, loft access and under floor heating.\r\n\r\nLounge/diner/kitchen 24\' 6\" x 16\' 0\" (7.47m x 4.88m) Spacious L shape open plan living, UPVC double glazed window and bi-folding doors to the rear, laminate flooring, television and telephone connection points, power sockets and under floor heating.\r\n\r\nFitted kitchen with wall and base cupboards, integrated Bosch electric hob and oven, cooker hood, lamona inset sink and drainer, mosaic style splash back tiling, integrated Bosch washing machine and dishwasher, integrated Bosch fridge freezer and power sockets.', 'Lorem ipsum content Lv', 'Lorem ipsum content Ru', 'about-2.jpg', 'Team');

-- --------------------------------------------------------

--
-- Структура таблицы `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `admin`
--

INSERT INTO `admin` (`id`, `email`, `password`) VALUES
(2, 'viktor@gmail.com', '$2y$10$TFhX3vqZZqHWYTuJrJqmW./Z4c5mw1/VHnicO1DdDL6m8O4DDiqeS');

-- --------------------------------------------------------

--
-- Структура таблицы `apartment_gallery`
--

CREATE TABLE `apartment_gallery` (
  `id` int(11) NOT NULL,
  `image_url` varchar(255) NOT NULL,
  `alt_tag` varchar(255) NOT NULL,
  `apartment_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `apartment_gallery`
--

INSERT INTO `apartment_gallery` (`id`, `image_url`, `alt_tag`, `apartment_id`) VALUES
(31, 'architectural-design-architecture-building-443383.jpg', 'Building', 31),
(32, 'apartment.jpg', 'WhiteHouse', 31),
(33, 'aerial-android-wallpaper-architectural-design-373912.jpg', 'City', 31),
(34, 'about-3.jpg', '', 31),
(53, 'apartment.jpg', 'Apartment 1', 42),
(56, '3.jpg', 'Apartment 2', 42),
(57, 'about-1.jpg', 'Apartment 3', 42),
(78, '', '', 67),
(80, 'cam00317b0-pr0225-still20_35084003343_o.jpg', '', 68);

-- --------------------------------------------------------

--
-- Структура таблицы `appartment`
--

CREATE TABLE `appartment` (
  `idAppartment` int(11) NOT NULL,
  `Appartment_Title_Ru` varchar(255) DEFAULT NULL,
  `Appartment_Title_En` varchar(255) DEFAULT NULL,
  `Appartment_Title_Lv` varchar(255) DEFAULT NULL,
  `Appartment_Price` varchar(50) DEFAULT NULL,
  `Appartment_Description_Ru` longtext,
  `Appartment_Description_En` longtext,
  `Appartment_Description_Lv` longtext,
  `Appartment_Images` varchar(255) DEFAULT NULL,
  `Appartment_View` varchar(255) NOT NULL,
  `Appartment_address` varchar(255) NOT NULL,
  `Meta_Description` varchar(255) NOT NULL,
  `Project_ID` int(11) DEFAULT NULL,
  `Property_ID` int(11) DEFAULT NULL,
  `PropDet_ID` int(11) DEFAULT NULL,
  `date_added` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `appartment`
--

INSERT INTO `appartment` (`idAppartment`, `Appartment_Title_Ru`, `Appartment_Title_En`, `Appartment_Title_Lv`, `Appartment_Price`, `Appartment_Description_Ru`, `Appartment_Description_En`, `Appartment_Description_Lv`, `Appartment_Images`, `Appartment_View`, `Appartment_address`, `Meta_Description`, `Project_ID`, `Property_ID`, `PropDet_ID`, `date_added`) VALUES
(31, 'Квартира 1', 'Apartment 1', 'Dzivoklis 1', '145453', 'Описание квартиры', 'Столица Латвии, особенно ее центральная часть, поражает своими архитектурными постройками. Некоторые районы славятся своими историческими зданиями, которые завораживают своими деталями и необычными решениями. Вы только представьте, как вы прогуливаетесь по этим улицам и любуетесь достопримечательностями с мировым уровнем известности. Именно такая возможность появляется при покупке квартиры в Риге.\r\n\r\n\r\nПредставляем вашему вниманию шикарные апартаменты в Риге. Вновь отстроенное здание по проекту Zundas Darzi полностью соответствует представлениям о проживании современного человека. Выставленная на продажу квартира располагается на первом этаже трехэтажного здания, которое было введено в эксплуатацию чуть более года назад. При рассмотрении этого варианта обратите внимание на просторную гостиную – это помещение действительно поражает своими масштабами.\r\nВнутренняя отделка лаконично завершает общее стилистическое направление всей квартиры. Оригинальный дизайн не сможет оставить равнодушным будущих владельцев этой элитной недвижимости. Кухонная зона совмещена с уютной гостиной. Отсутствие строгого разделения двух важных комнат позволяет проводить комфортную организацию некоторых бытовых действий. Вы можете спокойно готовить пищу и принимать ее в кругу близких. При этом хозяйка не остается в изоляции, а имеет возможность непрерывного общения со всеми членами семьи. \r\nПри желании с южной стороны гостиной расположен отдельный выход, благодаря которому вы попадаете на террасу. Озеленение этой части придомовой территории постоянно поддерживается на должном уровне, так что вы будете ежедневно любоваться на цветущие клумбы. Встроенный кухонный гарнитур включает в себя новейшие технические разработки от ведущих производителей. Здесь любая домохозяйка почувствует облегчение и даже удовольствие от повседневных забот. \r\nДвухкомнатная квартира оборудована системой безопасности, так что вы можете не беспокоиться за собственное имущество. Помимо этого, помещение подключено к современной системе пожаротушения – датчики срабатывают мгновенно и позволяют предотвратить страшные события. Воздух и вода в помещении прогреваются за счет собственного газового котла. Обратите внимание, что по проекту в ходе строительства были использованы исключительно натуральные материалы, которые не выделяют вредных веществ.', NULL, 'architectural-design-architecture-building-443383.jpg', 'https://premium.giraffe360.com/city24/oglu-iela-32-4/', 'Address Kipsdala', 'Meta descr', 1, 1, 86, '2018-11-20'),
(42, 'Квартира 2', 'Title 2', 'Dzivoklis 2', '9834', NULL, 'Apartment Description', NULL, 'apartment-architectural-design-architecture-323780.jpg', 'https://premium.giraffe360.com/city24/oglu-iela-32-4/', 'Kipsala 87', 'meta description', 2, 2, 144, '2018-11-20'),
(67, NULL, 'Apartment 7 ', NULL, '', NULL, '<p>Столица Латвии, особенно ее центральная часть, поражает своими архитектурными постройками. Некоторые районы славятся своими историческими зданиями, которые завораживают своими деталями и необычными решениями. Вы только представьте, как вы прогуливаетесь по этим улицам и любуетесь достопримечательностями с мировым уровнем известности. Именно такая возможность появляется при покупке квартиры в Риге.&nbsp;</p><p>Представляем вашему вниманию шикарные апартаменты в Риге. Вновь отстроенное здание по проекту Zundas Darzi полностью соответствует представлениям о проживании современного человека. Выставленная на продажу квартира располагается на первом этаже трехэтажного здания, которое было введено в эксплуатацию чуть более года назад. <i>При рассмотрении</i> этого варианта обратите внимание на просторную гостиную – это помещение действительно поражает своими масштабами.</p><p>Внутренняя отделка лаконично завершает общее стилистическое направление всей квартиры. Оригинальный дизайн не сможет оставить равнодушным будущих владельцев этой элитной недвижимости. Кухонная <strong>зона совмещена </strong>с уютной гостиной. Отсутствие строгого разделения двух важных комнат позволяет проводить комфортную организацию некоторых бытовых действий. Вы можете спокойно готовить пищу и принимать ее в кругу близких. При этом хозяйка не остается в изоляции, а имеет возможность непрерывного общения со всеми членами семьи. При желании с южной стороны гостиной расположен отдельный выход, благодаря которому вы попадаете на террасу.&nbsp;</p><p>Озеленение этой части придомовой территории постоянно поддерживается на должном уровне, так что вы будете ежедневно любоваться на цветущие клумбы. Встроенный кухонный гарнитур включает в себя новейшие технические разработки от ведущих производителей. Здесь любая домохозяйка почувствует облегчение и даже удовольствие от повседневных забот. Двухкомнатная квартира оборудована системой безопасности, так что вы можете не беспокоиться за собственное имущество.</p><p>Помимо этого, помещение подключено к современной системе пожаротушения – датчики срабатывают мгновенно и позволяют предотвратить страшные события. Воздух и вода в помещении прогреваются за счет собственного газового котла. Обратите внимание, что по проекту в ходе строительства были использованы исключительно натуральные материалы, которые не выделяют вредных веществ.</p>', NULL, '', '', '', 'Помимо этого, помещение подключено к современной системе пожаротушения – датчики срабатывают мгновенно', 1, 1, 166, '2018-12-05'),
(68, NULL, 'Apartment 8', NULL, '', NULL, 'Some content description', NULL, 'cam00317b0-pr0225-still20_35084003343_o.jpg', '', '', '', 1, 1, 167, '2018-12-05');

-- --------------------------------------------------------

--
-- Структура таблицы `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `Blog_Title_ru` varchar(255) DEFAULT NULL,
  `Blog_Title_en` varchar(255) DEFAULT NULL,
  `Blog_Title_lv` varchar(255) DEFAULT NULL,
  `Blog_Date` date DEFAULT NULL,
  `Blog_Content_ru` longtext,
  `Blog_Content_en` longtext,
  `Blog_Content_lv` longtext,
  `Blog_Image` varchar(255) NOT NULL,
  `Blog_ImageDescription_ru` varchar(255) NOT NULL,
  `Blog_ImageDescription_en` varchar(255) NOT NULL,
  `Blog_ImageDescription_lv` varchar(255) NOT NULL,
  `Meta_Description` varchar(255) NOT NULL,
  `alt_tag` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `blog`
--

INSERT INTO `blog` (`id`, `Blog_Title_ru`, `Blog_Title_en`, `Blog_Title_lv`, `Blog_Date`, `Blog_Content_ru`, `Blog_Content_en`, `Blog_Content_lv`, `Blog_Image`, `Blog_ImageDescription_ru`, `Blog_ImageDescription_en`, `Blog_ImageDescription_lv`, `Meta_Description`, `alt_tag`) VALUES
(2, 'Название новости 2', 'Title of the news 2', 'Ziņas nosaukums 2', '2018-08-20', 'Это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Lorem Ipsum – tas ir teksta salikums, kuru izmanto poligrāfijā un maketēšanas darbos. Lorem Ipsum ir kļuvis par vispārpieņemtu teksta aizvietotāju kopš 16. gadsimta sākuma. Tajā laikā kāds nezināms iespiedējs izveidoja teksta fragmentu, lai nodrukātu grāmatu ar burtu paraugiem. Tas ir ne tikai pārdzīvojis piecus gadsimtus, bet bez ievērojamām izmaiņām saglabājies arī mūsdienās, pārejot uz datorizētu teksta apstrādi. Tā popularizēšanai 60-tajos gados kalpoja Letraset burtu paraugu publicēšana ar Lorem Ipsum teksta fragmentiem un, nesenā pagātnē, tādas maketēšanas programmas kā Aldus PageMaker, kuras šablonu paraugos ir izmantots Lorem Ipsum teksts.', 'blog_1.jpg', 'Текст', 'Text 12', 'Teksts', 'Meta Description goes here', 'image is awesome'),
(8, NULL, 'Title of the news 7', NULL, '2018-10-09', 'Это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.', 'Nav daudz tādu, kas zinās, ka mūsu pilsētas lepnums un īpašā iezīme – Rīgā plaši pārstāvētais jūgendstils atrodams arī Ķīpsalā. Elegantā ēka Ogļu ielā 30 ir viena no interesantākajām un ekskluzīvākajām, taču retajām pēdējos gados atjaunotajām jūgendstila ēkām Daugavas kreisajā krastā. \r\n\r\nTā ir vienīgā jūgendstila mūra ēka Ķīpsalā, turklāt  ar autentiski atjaunotu galvenās fasādes veidolu.  Jūgendstila ziedu laiku lakoniskā, taču piemīlīgā celtne ir saglabājusi vēsturisko ielas fasādes arhitektūru un interjera dekoratīvos elementus – rozetes, kāpņu margu rakstus, slīpētos logu stiklus un griestu rotājumus. Atjaunotā mūra ēka celta pagājušā gadsimta sākumā pēc izcilā latviešu arhitekta Mārtiņa Ņukšas projekta, kurš bijis gan galvenais arhitekts Sevastopolē, gan strādājis kā Marseļas galvenā arhitekta palīgs, pēcāk bijis arī Latvijas diplomātiskais sūtnis daudzās Eiropas valstīs. Traģiski gājis bojā padomju represiju laikā, bet viņa arhitekta veikums arī šodien saglabājies kopumā astoņos jūgendstilā projektētos Rīgas dzīvojamos namos. \r\n\r\n“Patiesībā tā nav restaurācija, kas šeit ir notikusi, bet tā ir mūsu ļoti lielā vēlēšanās šajā vietā Ķīpsalu nepārtraukt, bet turpināt – cilvēciski, saprotami un iespējami patiesi,” stāsta atjaunoto ēku projekta līdzautors arhitekts Pēteris Blūms, uzsverot, ka tas nebūtu iespējams bez projekta komandas, kurā gandrīz brīnumainā kārtā satikušies līdzīgi domājoši un vidi ap sevi līdzīgi izjūtoši cilvēki, sākot jau ar projekta pasūtītāju Borisu Semjonovu, arhitekti Ditu Liepiņu ar kolēģiem, un šis nav pirmais viņu kopīgi realizētais projekts.\r\n\r\nNezaudējot Ķīpsalas īpašo šarmu\r\n\r\nNav šaubu, ka rekonstruēt vai uzbūvēt no jauna ir lētāk, un tā tas vairumā gadījumu ar šādām vēsturiskām ēkām arī notiek, bet Ogļu ielas 30. nama īpašnieks uzsvēra savu vēlmi tieši atjaunot, nevis pilnībā pārbūvēt, jo tas ir daudz interesantāk. “Protams, šis projekts ir arī bizness, bet man gribas izveidot vēl vienu skaistu Rīgas stūrīti, tuvu tam, kāds tas bijis kādreiz, un tādu, kas organiski iekļaujas visā pārējā salas apbūvē. Es pats dzīvoju Ķīpsalā, un man ļoti gribētos, lai Ķīpsala saglabātu šo savu īpašo šarmu, citādi mēs pazaudēsim savu pilsētu. Esmu lepns, ka Rīgai ir šie senie koka nami, un daudzi no tiem ir skaisti atjaunoti – tādu nav pasaules lielajās metrapolēs. Diemžēl Jūrmalā jau tas lielā mērā ir noticis – šīs vietas īpašais šarms sāk pamazām zust,” ar nožēlu piebilst B. Semjonovs. \r\n\r\n“Būtībā šī ēka ir uzbūvēta no jauna, jo veco nevarēja saglabāt, tā bija bojāta kara laikā, divas reizes degusi, ieaugusi kultūrslānī, pamatu nebija. Māja tika precīzi uzmērīta, demontēta, slēģi, ieejas durvis nodotas restauratoru rokās,” stāsta Pēteris Blūms. Šodienas nams ir uzbūvēts no gāzbetona blokiem, nosiltināts, apšūts ar dēļiem. Kāpņu vestibilā kādreizējo logu ailu vietā iebūvēti divi restaurēti sākotnējo logu komplekti, kas noformēti ar fotopanorāmu kā iluzoru skatu pāri Daugavai uz 19. gadsimta Vecrīgu.  Izcils retums ir arī restaurētie ēkas ielas fasādi rotājošie unikālie reklāmas uzraksti latviešu, krievu, vācu un somu valodā, kuri radušies jau pirms Pirmā pasaules kara. Tie tika atklāti uz fasādes dēļiem zem apmetuma un, cik zināms,  ir vienīgie uz koka gleznotie oriģinālie un restaurētie ielu reklāmas objekti, kas apskatāmi Rīgā. \r\n\r\nMērķis restaurēt sajūtas\r\n\r\nPilnībā nomainītas un modernizētas visas iekšējās un ārējās komunikācijas. Arhitekti atzīst, ka ziedošanās vēstures mīlestībai nevar būt bezgalīga un “kvadrātmetri ir kvadrātmetri”, tāpēc mērķis bijis ne tik daudz restaurēt māju, bet restaurēt tā laika sajūtas. “Viss jaunais pārāk ātri dzēš mūsu laika pēdas apkārtnē pat vienas paaudzes ietvaros, tāpēc cilvēki ir izslāpuši pēc pēctecības sajūtas, viņi vēlas izjust laika ritējuma turpinājumu šodienā, un šo saikni sajūtu līmenī esam centušies maksimāli saglabāt.  Ne katram šeit patiks, bet vēlēšanās patikt visiem absolūti nebija mūsu mērķis, strādājot pie šī projekta,” uzsver Pēteris Blūms. \r\n\r\nProtams, ir mainījušies akcenti – agrāk parādes puse bija ielas fasāde, taču tagad tā vairāk ir vieta, ko apbrīno tūristi. “Ķēķa puse” ir ieguvusi pavisam citu jēgu – ieejas mājā ir no pagalma puses, šeit stāvēs mājas iedzīvotāju auto, rotaļāsies bērni. Pagalma pusē atjaunots arī bijušais zirgu stallis, izveidojot to par ērtu divstāvu ģimenes māju. Tur bija palikušas tikai drupas, bet arhitekta Mārtiņa Ņukšas projekts bija saglabājies, un māja tika veidota nevis kā kopija, bet sajūtu turpinājums, kam projekta komanda devusi savu interpretāciju. \r\n\r\n“Mazliet žēl, ka RTU Arhitektūras fakultātes studentiem neiemāca ar labsirdību un labvēlību saprast, lasīt un izjust vēsturisko vidi, jo mums tās ir ārkārtīgi daudz. Ne vienmēr ir jāatstāj redzamas pēdas cita arhitekta darbā, nu, ja tu citādi nevari, maini, pārtaisi – tas nav grēks, bet ar pietāti, lai tas ir esošā turpinājums, nevis pārrāvums. Esmu šādu filosofiju piekopis jau 35 gadus, un tas ir tas, kas man rada gandarījumu dzīvē. Es neesmu bijis ļoti revolucionārs vēsturiskajā vidē, negribu satricinājumus, gribu mierīgu evolūciju, lai, atnākot uz šo vietu, šo māju, tu vari ieraudzīt savu vecvecāku laiku turpinājumu šodien, lai saikne netiek pārrauta. Ja studentiem to mācītu kā arhitektūras filosofiju, tas nekādā mērā netraucētu viņiem radīt jaunus šedevrus, bet viņi nebūtu destruktori,” uzskata Pēteris Blūms. \r\n\r\nDaudz gaismas, mūsdienu kvalitātes un komforta\r\n\r\n“Visu laiku, kopš strādājam pie šī nama, esam pret to izturējušies ne kā pret biznesa projektu, bet kā pret savas dvēseles un pārliecības daļu, tāpēc tagad, kad māja ir nodota potenciālo pircēju vērtējumam, nemaz negribas to pārdot,” atzīst Boriss Semjonovs.Tomēr bizness ir bizness, un Rīgas nekustamo īpašumu tirgus papildinājies ar vēl vienu interesantu un ļoti īpašu piedāvājumu. No atjaunotās mūra daudzstāvu ēkas Ogļu ielas pusē paveras skats uz Daugavu, bet no augšstāvu logiem redzamā Eiropas kultūras mantojuma sarakstā iekļautā Vecrīgas panorāma ir suģestējošs urbānās mākslas šedevrs. \r\n\r\n“Četros nama stāvos izvietoti septiņi trīsistabu dzīvokļi, pārsvarā pa diviem dzīvokļiem katrā. Ceturtajā stāvā izveidots viens ļoti plašs dzīvoklis ar balkoniem uz visām debespusēm. Ļoti interesants ir arī jumta stāva dzīvoklis. Visiem dzīvokļiem ir skats gan uz Daugavu, gan Ķīpsalas ziemeļu daļu. Ēkā ir stikla pakešu trīskārtīgie logi koka rāmī un masīvkoka durvis, augstvērtīga santehnika. Telpu mūsdienīgo interjeru papildina eleganti jūgendstila kamīni un krāsnis ar stikla durvīm, kas domātas ne tikai skaistumam - iespējama arī opcionāla malkas apsilde. Labiekārtota un apzaļumota teritorija, dārzam ir izstrādāts labiekārtojuma un apstādījumu projekts ar kvalitatīviem dekoratīvajiem augiem, mazajām arhitektūras formām - soliņiem, lapenēm, atpūtas vietu bērniem,” stāsta projekta attīstītāju pārstāve Inna Semjonova, īpaši uzsverot arī tādu priekšrocību kā zemi komunālie maksājumi nākotnē, jo visām projekta ēkām ir kopējas inženierkomunikācijas.\r\n\r\nVisi projekta attīstītāji ir vienisprātis, ka šie trīs gadi, kuru laikā skaistā jūgendstila ēka atjaunota, bijuši sarežģīti, bet arī ļoti interesanti, un tagad pat ir mazliet žēl, ka tas ir beidzies. Atbildot uz jautājumu, kādus cilvēkus vēlētos redzēt dzīvojam šajā namā, Boriss Semjonovs saka: “Es gribētu, lai cilvēki nevērtētu šo projektu tikai kā skaistu panorāmu aiz loga, bet lai viņi sajustu to, ko esam ielikuši arī iekšējā interjerā un ergonomiskajos risinājumos, lai viņi ieraudzītu šīs no senajiem laikiem saglabātās un restaurētās lietas. Viņiem jābūt šīs īpašās auras fanātiem – tādiem pašiem kā es! Esmu pateicīgs mūsu komandai – arhitektiem, būvniekiem, restauratoriem, interjeristiem, visiem, kas atbalstījuši šo manu pozīciju.”\r\n\r\n“Mēs neapgalvojam, ka šīs ēkas ir pilnībā restaurētas un ir kā veco laiku Rīgas muzejs. Mēs apgalvojam, ka šajās ēkas ir ne mazums restaurētu detaļu un daudz gaismas, mūsdienu kvalitātes, komforta. Daudz patiesas Ķīpsalas, ” tā arhitekts Pēteris Blūms. \r\n', NULL, '3.jpg', '', 'Text 7', '', 'Meta Description', 'Post Modern'),
(22, NULL, 'Title of the news 34', NULL, NULL, NULL, 'Santa is looking for a second home in the U.S.! His people reached out to RealEstate.com for help in finding the perfect off-season getaway. We’ve been granted exclusive access to his favorites folder so we can drop in recommendations and know for certain that the homes listed below are top contenders. Found a particularly beautiful home on RealEstate.com that you think Santa might love? Post it in the comments below and we’ll pass your ideas along. Requirements: Must be cozy and inconspicuous.', NULL, 'blogpost.png', '', 'Blogging Content', '', '', 'Blogging'),
(26, NULL, 'Title News 20', NULL, '2018-10-22', NULL, 'Santa is looking for a second home in the U.S.! His people reached out to RealEstate.com for help in finding the perfect off-season getaway. We’ve been granted exclusive access to his favorites folder so we can drop in recommendations and know for certain that the homes listed below are top contenders. Found a particularly beautiful home on RealEstate.com that you think Santa might love? Post it in the comments below and we’ll pass your ideas along. Requirements: Must be cozy and inconspicuous.', NULL, 'blogpost.png', '', 'test 33', '', 'Meta', 'Image!!!'),
(31, NULL, 'Post Title Nr 6', NULL, '2018-11-20', NULL, 'Santa is looking for a second home in the U.S.! His people reached out to RealEstate.com for help in finding the perfect off-season getaway. We’ve been granted exclusive access to his favorites folder so we can drop in recommendations and know for certain that the homes listed below are top contenders. Found a particularly beautiful home on RealEstate.com that you think Santa might love? Post it in the comments below and we’ll pass your ideas along. Requirements: Must be cozy and inconspicuous.', NULL, 'image1.jpg', '', 'Image title', '', 'meta description ', 'phone alt'),
(34, NULL, 'My Post title 8', NULL, '2018-11-23', NULL, 'Santa is looking for a second home in the U.S.! His people reached out to RealEstate.com for help in finding the perfect off-season getaway. We’ve been granted exclusive access to his favorites folder so we can drop in recommendations and know for certain that the homes listed below are top contenders. Found a particularly beautiful home on RealEstate.com that you think Santa might love? Post it in the comments below and we’ll pass your ideas along. Requirements: Must be cozy and inconspicuous.', NULL, 'about-1.jpg', '', '', '', 'Meta Description', '');

-- --------------------------------------------------------

--
-- Структура таблицы `contact`
--

CREATE TABLE `contact` (
  `idContact` int(11) NOT NULL,
  `Contact_Phone` varchar(50) DEFAULT NULL,
  `Contact_Email` varchar(50) DEFAULT NULL,
  `Contact_Address` varchar(50) DEFAULT NULL,
  `Contact_Mobilephone` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `contact`
--

INSERT INTO `contact` (`idContact`, `Contact_Phone`, `Contact_Email`, `Contact_Address`, `Contact_Mobilephone`) VALUES
(1, '65358939', 'email@gmail.com', 'Latvia, Riga, Kipsala 54', '+371 2354 4090');

-- --------------------------------------------------------

--
-- Структура таблицы `features`
--

CREATE TABLE `features` (
  `idFeature` int(11) NOT NULL,
  `Feature_Title_Ru` varchar(255) DEFAULT NULL,
  `Feature_Title_En` varchar(255) DEFAULT NULL,
  `Feature_Title_Lv` varchar(255) DEFAULT NULL,
  `Appartment_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `features`
--

INSERT INTO `features` (`idFeature`, `Feature_Title_Ru`, `Feature_Title_En`, `Feature_Title_Lv`, `Appartment_ID`) VALUES
(319, 'Пример 1', 'Feature 1', NULL, 31),
(320, '', 'Feature 2', NULL, 31),
(321, '', 'Feature 3', NULL, 31),
(322, '', 'Feature 4', NULL, 31),
(323, '', 'Feature 5', NULL, 31),
(324, '', 'Feature 6', NULL, 31),
(385, NULL, '', NULL, 42),
(386, NULL, '', NULL, 42),
(387, NULL, '', NULL, 42),
(388, NULL, '', NULL, 42),
(389, NULL, '', NULL, 42),
(390, NULL, '', NULL, 42),
(511, NULL, 'new image', NULL, 67),
(512, NULL, 'old tree', NULL, 67),
(513, NULL, 'fast dog', NULL, 67),
(514, NULL, 'slow man', NULL, 67),
(515, NULL, 'tree big', NULL, 67),
(516, NULL, 'cool 45', NULL, 67),
(517, NULL, '', NULL, 68),
(518, NULL, '', NULL, 68),
(519, NULL, '', NULL, 68),
(520, NULL, '', NULL, 68),
(521, NULL, '', NULL, 68),
(522, NULL, '', NULL, 68);

-- --------------------------------------------------------

--
-- Структура таблицы `our_team`
--

CREATE TABLE `our_team` (
  `team_id` int(11) NOT NULL,
  `employee_name_en` varchar(255) NOT NULL,
  `employee_name_lv` varchar(255) NOT NULL,
  `employee_name_ru` varchar(255) NOT NULL,
  `position_lv` varchar(255) NOT NULL,
  `position_en` varchar(255) NOT NULL,
  `position_ru` varchar(255) NOT NULL,
  `employee_photo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `our_team`
--

INSERT INTO `our_team` (`team_id`, `employee_name_en`, `employee_name_lv`, `employee_name_ru`, `position_lv`, `position_en`, `position_ru`, `employee_photo`) VALUES
(4, 'Ronald.A.', '', '', '', 'Director', 'Директор', 'alex-blajan-223771-unsplash.jpg'),
(5, 'Oliver.S.', '', '', '', 'CEO', '', 'ceo.jpg'),
(6, 'Andrew.B.', '', '', '', 'Developer', 'Разработчик', 'alex-blajan-223771-unsplash.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `project`
--

CREATE TABLE `project` (
  `id` int(11) NOT NULL,
  `Project_Name_en` varchar(255) DEFAULT NULL,
  `Project_Description_en` text NOT NULL,
  `Project_Image` varchar(255) NOT NULL,
  `Project_Name_ru` varchar(255) NOT NULL,
  `Project_Name_lv` varchar(255) NOT NULL,
  `Project_Description_ru` text NOT NULL,
  `Project_Description_lv` text NOT NULL,
  `image_alt` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `project`
--

INSERT INTO `project` (`id`, `Project_Name_en`, `Project_Description_en`, `Project_Image`, `Project_Name_ru`, `Project_Name_lv`, `Project_Description_ru`, `Project_Description_lv`, `image_alt`) VALUES
(1, 'Zundas Darzi', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'architectural-design-architecture-building-443383.jpg', '', '', '', '', 'Zundas Darzi Project Image'),
(2, 'Riverside', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'aerial-android-wallpaper-architectural-design-373912.jpg', '', '', '', '', 'Riverside Project Image');

-- --------------------------------------------------------

--
-- Структура таблицы `property`
--

CREATE TABLE `property` (
  `idProperty` int(11) NOT NULL,
  `Property_Type` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `property`
--

INSERT INTO `property` (`idProperty`, `Property_Type`) VALUES
(1, 'For sale'),
(2, 'For rent');

-- --------------------------------------------------------

--
-- Структура таблицы `propertydetails`
--

CREATE TABLE `propertydetails` (
  `idPropertyDetails` int(11) NOT NULL,
  `PD_Size` int(11) DEFAULT NULL,
  `PD_Rooms` int(11) DEFAULT NULL,
  `PD_PriceUnit` varchar(50) DEFAULT NULL,
  `PD_Bathrooms` int(11) DEFAULT NULL,
  `PD_Floor` varchar(5) DEFAULT NULL,
  `PD_Parking` int(11) DEFAULT NULL,
  `PD_AptFloor` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `propertydetails`
--

INSERT INTO `propertydetails` (`idPropertyDetails`, `PD_Size`, `PD_Rooms`, `PD_PriceUnit`, `PD_Bathrooms`, `PD_Floor`, `PD_Parking`, `PD_AptFloor`) VALUES
(83, 255, 3, '1', 3, '2', 1, 4),
(84, 1, 1, '', 0, '0', 0, 0),
(85, 23, 5, '3', 1, '5', 3, 1),
(86, 1, 2, '3', 54, '5', 5, 6),
(88, 12, 234, '324', 32, '5', 2, 1),
(102, 34, 0, '', 0, '', 0, 0),
(113, 0, 0, '', 0, '', 0, 0),
(118, 0, 0, '', 0, '', 0, 0),
(119, 0, 0, '', 0, '', 0, 0),
(120, 0, 0, '', 0, '', 0, 0),
(121, 0, 0, '', 0, '', 0, 0),
(122, 0, 0, '', 0, '', 0, 0),
(123, 0, 0, '', 0, '', 0, 0),
(124, 0, 0, '', 0, '', 0, 0),
(125, 0, 0, '', 0, '', 0, 0),
(126, 0, 0, '', 0, '', 0, 0),
(127, 0, 0, '', 0, '', 0, 0),
(128, 0, 0, '', 0, '', 0, 0),
(129, 0, 0, '', 0, '', 0, 0),
(130, 0, 0, '', 0, '', 0, 0),
(131, 0, 0, '', 0, '', 0, 0),
(132, 0, 0, '', 0, '', 0, 0),
(133, 0, 0, '', 0, '', 0, 0),
(144, 1, 3, '15354', 1, '1', 1, 3),
(149, 0, 0, '', 0, '', 0, 0),
(150, 0, 0, '', 0, '', 0, 0),
(151, 0, 0, '', 0, '', 0, 0),
(152, 0, 0, '', 0, '', 0, 0),
(153, 0, 0, '', 0, '', 0, 0),
(154, 0, 0, '', 0, '', 0, 0),
(155, 0, 0, '', 0, '', 0, 0),
(156, 0, 0, '', 0, '', 0, 0),
(157, 0, 0, '', 0, '', 0, 0),
(158, 0, 0, '', 0, '', 0, 0),
(159, 0, 0, '', 0, '', 0, 0),
(160, 0, 0, '', 0, '', 0, 0),
(161, 0, 0, '', 0, '', 0, 0),
(162, 0, 0, '', 0, '', 0, 0),
(163, 0, 0, '', 0, '', 0, 0),
(164, 0, 0, '', 0, '', 0, 0),
(165, 0, 0, '', 0, '', 0, 0),
(166, 0, 3, '12500', 4, '4', 2, 3),
(167, 0, 0, '', 0, '0', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `slider_title_en` varchar(255) NOT NULL,
  `slider_title_ru` varchar(255) NOT NULL,
  `slider_title_lv` varchar(255) NOT NULL,
  `slider_content_en` text NOT NULL,
  `slider_content_ru` varchar(255) NOT NULL,
  `slider_content_lv` varchar(255) NOT NULL,
  `slider_image` varchar(255) NOT NULL,
  `image_alt` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `slider`
--

INSERT INTO `slider` (`id`, `slider_title_en`, `slider_title_ru`, `slider_title_lv`, `slider_content_en`, `slider_content_ru`, `slider_content_lv`, `slider_image`, `image_alt`) VALUES
(7, 'Slider Title 3', 'Заголовок Ру 3', 'Title LV 3', 'Slider Content Goes Here', 'Content Ru', 'Slider Content LV', 'cam00317b0-pr0222-still01_35713486956_o.jpg', 'Alt tag 1'),
(37, 'En Slider Title', 'New slide', '', 'Short Content EN', 'Slide content', '', 'cam00317b0-pr0222-still07_35622506631_o.jpg', 'Alt tag for slider'),
(78, 'New Title!!', '', '', 'cotnenwta', '', '', 'cam00317b0-pr0225-still20_35084003343_o.jpg', 'ALT TAG'),
(79, 'Title 7', '', '', 'Some content', '', '', '20170801_114253.jpg', '');

-- --------------------------------------------------------

--
-- Структура таблицы `team_content`
--

CREATE TABLE `team_content` (
  `id` int(11) NOT NULL,
  `content_en` text NOT NULL,
  `content_ru` text NOT NULL,
  `content_lv` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `team_content`
--

INSERT INTO `team_content` (`id`, `content_en`, `content_ru`, `content_lv`) VALUES
(1, 'This is our Team short description', '', '');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `about_us_main_page`
--
ALTER TABLE `about_us_main_page`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `apartment_gallery`
--
ALTER TABLE `apartment_gallery`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idAppartment` (`apartment_id`);

--
-- Индексы таблицы `appartment`
--
ALTER TABLE `appartment`
  ADD PRIMARY KEY (`idAppartment`),
  ADD KEY `fk_Appartment_Property1` (`Property_ID`),
  ADD KEY `fk_Appartment_Project1` (`Project_ID`),
  ADD KEY `fk_Appartment_PropertyDetails1` (`PropDet_ID`);

--
-- Индексы таблицы `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`idContact`);

--
-- Индексы таблицы `features`
--
ALTER TABLE `features`
  ADD PRIMARY KEY (`idFeature`),
  ADD KEY `fk_Feature_Appartment` (`Appartment_ID`);

--
-- Индексы таблицы `our_team`
--
ALTER TABLE `our_team`
  ADD PRIMARY KEY (`team_id`);

--
-- Индексы таблицы `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `property`
--
ALTER TABLE `property`
  ADD PRIMARY KEY (`idProperty`);

--
-- Индексы таблицы `propertydetails`
--
ALTER TABLE `propertydetails`
  ADD PRIMARY KEY (`idPropertyDetails`);

--
-- Индексы таблицы `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `about`
--
ALTER TABLE `about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `about_us_main_page`
--
ALTER TABLE `about_us_main_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `apartment_gallery`
--
ALTER TABLE `apartment_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT для таблицы `appartment`
--
ALTER TABLE `appartment`
  MODIFY `idAppartment` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT для таблицы `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT для таблицы `contact`
--
ALTER TABLE `contact`
  MODIFY `idContact` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `features`
--
ALTER TABLE `features`
  MODIFY `idFeature` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=523;

--
-- AUTO_INCREMENT для таблицы `our_team`
--
ALTER TABLE `our_team`
  MODIFY `team_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `project`
--
ALTER TABLE `project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `property`
--
ALTER TABLE `property`
  MODIFY `idProperty` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `propertydetails`
--
ALTER TABLE `propertydetails`
  MODIFY `idPropertyDetails` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=168;

--
-- AUTO_INCREMENT для таблицы `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `apartment_gallery`
--
ALTER TABLE `apartment_gallery`
  ADD CONSTRAINT `apartment_gallery_ibfk_1` FOREIGN KEY (`apartment_id`) REFERENCES `appartment` (`idAppartment`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `appartment`
--
ALTER TABLE `appartment`
  ADD CONSTRAINT `fk_Appartment_Project1` FOREIGN KEY (`Project_ID`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_Appartment_Property1` FOREIGN KEY (`Property_ID`) REFERENCES `property` (`idProperty`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Appartment_PropertyDetails1` FOREIGN KEY (`PropDet_ID`) REFERENCES `propertydetails` (`idPropertyDetails`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `features`
--
ALTER TABLE `features`
  ADD CONSTRAINT `fk_Feature_Appartment` FOREIGN KEY (`Appartment_ID`) REFERENCES `appartment` (`idAppartment`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
