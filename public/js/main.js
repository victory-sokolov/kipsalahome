$(document).ready(function () {


	/* Mega Menu */

	$(".sf-menu").superfish({
		delay: 200,
		speed: "fast",
		cssArrows: false
	})
		.after("<div id='mobile-menu'>").clone().appendTo("#mobile-menu");
	$("#mobile-menu").find("*").attr("style", "");
	$("#mobile-menu").children("ul").removeClass("sf-menu")
		.parent().mmenu({
			extensions: ['widescreen', 'theme-white', 'effect-menu-slide', 'pagedim-black'],
			navbar: {
				title: "Menu"
			}
		});

	$(".toggle-mnu").click(function () {
		$(this).addClass("on");
	});
	var api = $("#mobile-menu").data("mmenu");
	api.bind("closed", function () {
		$(".toggle-mnu").removeClass("on");
	});


	function slideDownContactForm() {
		$(".contact-form-apartment p").slideDown(500)
		$(".contact-form-apartment i").slideDown(500)
		$(".contact-form-apartment form").slideDown(500)
	}

	// show the part
	$(".contact-form-apartment").click(function () {
		slideDownContactForm();
	});

	// close the hidden part
	$(".contact-form-apartment i").click(function () {
		$(this).slideUp(500)
		$(".contact-form-apartment p").slideUp(500)
		$(".contact-form-apartment form").slideUp(500)
		return false
	});

	//open contact form after successfull submition
	$(".alert-success").filter(function () {
		if (($(this).css("display") == "none") == false) {
			slideDownContactForm();
		}
	});

	/**
	 * Sticky menu
	 * applie transparent background
	 * change menu elements color
	*/


	function applyColor(color) {
		let menuColor = document.querySelectorAll(".sf-menu > li > a, #btnGroupDrop1, .active-language");
		$(".header-contact-number > span").css("color", color);

		menuColor.forEach(function (item) {
			item.style.color = color;
		});
	}

	const mainNav = document.querySelector('.main-navigation');
	const logoNav = document.querySelector('.top-header-menu');
	const contNav = document.querySelector('.cntc-and-lang');
	const contPhoneNav= document.querySelector('.header-contact-number');
	var div = document.getElementById('mm-blocker');
	const navOffset = mainNav.offsetTop;
	const wrapperDiv = document.createElement("div");

	function wrap(element, wrapper) {
		element.parentNode.insertBefore(wrapper, element);
		wrapper.className = "nav-placeholder";
		wrapper.appendChild(element);
	}

	wrap(mainNav, wrapperDiv);

	const navPlaceholder = document.querySelector(".nav-placeholder");
	navPlaceholder.style.height = mainNav.offsetHeight;
	
	let contactSection = document.querySelector(".contact-page-wrapper");
	let blogPosts = document.querySelector(".blogpost-wrapper");
	let apartmentSection = document.querySelector(".appartment-page-section")

	//if on about-us, apartment page, make menu element white
	function menuWhiteFunc() {

		if (blogPosts !== null || apartmentSection != null) {
			$(".main-navigation").css("border-bottom", "1px solid #eee");
			// Change logotype ot white
			$(".logo-wrap  img").attr("src", "/public/images/KipsalaHome_Normal Horizontal.png");
      
			applyColor("#222");
 			$(".phone-icon i").css("color", "orange");
			// change color to black for burge menu
            $(".toggle-mnu").addClass("burger-menu-mobile");
		} 
		
		
	}
	menuWhiteFunc();
	

	window.onscroll = function () {
	    
		// Get scroll in pixels
		let scrollPos = this.scrollY;

		if (scrollPos > navOffset) {
		    
			mainNav.classList.add("menu-transparency");
			logoNav.classList.add("logo-sticky");
			contNav.classList.add("cont-sticky");
			contPhoneNav.classList.add("phone-sticky");
			
			div.innerHTML = 
                	'<a href="#mobile-menu" class="toggle-mnu off burger-menu-mobile" aria-hidden="true" style="\
                	"><span></span></a>';
	
	    
	        $("html.mm-pagedim-black.mm-opening #mm-blocker").css("opacity", 0.6);
	        
			mainNav.style.boxShadow = "0 0px 0px 0 rgba(0,0,0,.2), 0 0px 10px 0 rgba(0,0,0,.2)";
			
			applyColor("#222");
		
			$(".logo-wrap  img").attr("src", "/public/images/KipsalaHome_Normal Horizontal.png");
			//remove class from mobile menu
			$(".toggle-mnu").removeClass("burger-menu-mobile");
			
			//burger menu to black
			 $(".toggle-mnu").addClass("burger-menu-mobile");

		} else {
			mainNav.classList.remove("menu-transparency");
			logoNav.classList.remove("logo-sticky");
			contNav.classList.remove("cont-sticky");
			contPhoneNav.classList.remove("phone-sticky");
			div.innerHTML = '';
			
			$();
			
			mainNav.style.boxShadow = 'none';
			$(".logo-wrap  img").attr("src", "/public/images/KipsalaHome_White Vertical.png");


			applyColor("#fff");
			menuWhiteFunc();
			
			if (contactSection == null && blogPosts == null && apartmentSection == null) {
			    //burger menu to white
			    $(".toggle-mnu").removeClass("burger-menu-mobile");       
			}
			
		}
	}
	

	/* Slider */
	$(".mCustomScrollbar").mCustomScrollbar({ axis: "x" });

	const carouseFirstItem = $(".carousel-item:first-child").addClass("active");
	carouseFirstItem.className += " active";


	/* Contact Form */
	const contactForm = document.querySelector(".contact-form");
	const allInputs = document.querySelectorAll(".contact-form input, textarea");
	let inputStyles = document.querySelectorAll(".inputMaterial");
	let inputStyleLabel = document.querySelectorAll(".inputMaterial ~ label");

	// apply keyup event on contact page
	if (contactForm !== null) {

		//When submitting form, if required fields are empty add class 'submitted' which will add red border
		document.querySelector(".submit-form").addEventListener("click", function () {
			    document.querySelector(".contact-form").className = "submitted";
		});

		contactForm.addEventListener("keyup", applyStyleToFormInputs);
	}

	/**
	 * Add negative top to the label when input is valid
	 */
	function applyStyleToFormInputs() {
	    
		allInputs.forEach(function (el) {

			if (el.value !== "") {

				//apply css styles on form inputs
				//let inputFields = document.getElementsByName(el.name);

				inputStyles.forEach(function (input, i) {

					// if input with specific name is filled out, applie custom css styles
					if (input.name == el.name) {
						inputStyleLabel[i].style.top = "-20px";
					}
				});
			}
		});
	}


    // apply different top on the search page for breadcrumbs
    if(window.location.href.indexOf("search") > -1) {
         $(".breadcrumb").css("top", 155);
    }
   

});

