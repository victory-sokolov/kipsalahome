/**
* Clear search fields on click
*/

function _$(selector) {
    return document.querySelector(selector);
}

const selectDOM = {
    selectProperty: _$("#select-property"),
    selectProjects: _$(".select-projects"),
    selectRooms: _$(".select-rooms"),
    filterForm: _$(".filter-appartment"),
    clearFields: _$(".clear-filter-link"),
    currentLang: _$(".active-language").innerHTML.trim()
};


const translation = {
    Floor_en : "Floor",
    Floor_lv : "Stavs",
    Floor_ru : "Этаж",
    Room_ru  : "Комнаты",
    Room_lv  : "Istabas",
    Room_en  : "Rooms",
    Month_en : "Month",
    Month_ru : "Месяц",
    Month_lv : "Mēnesī",
    ForSale_en: "For Sale",
    ForSale_ru: "Продажа",
    ForSale_lv: "Pārdod",
    ForRent_en: "For Rent",
    ForRent_ru : "Аренда",
    ForRent_lv : "Izīrē"
}


selectDOM.clearFields.addEventListener('click', clearSelectedFields);

function clearSelectedFields() {

    const searchFileds = document.querySelectorAll(".filter-appartment .form-group");

    searchFileds.forEach(function (arrElement) {

        // Clear selection field
        if (arrElement.innerHTML.indexOf("select") > -1) {
            arrElement.querySelector("select").selectedIndex = 0;
        }

        // Clear input field
        if (arrElement.innerHTML.indexOf("input") > -1) {
            arrElement.querySelector("input").value = "";
        }

        removeFilterTag();
        localStorage.clear();
        
    });
    
    filterFormValues();
    
}

// Load selected filter from Local Storage
function loadFilters() {
    
    for(let [key, val] of Object.entries(selectDOM)) {
        if(localStorage[key]) {
            val.value  = localStorage[key];
        }
    }
}


// Saves selected filters to Local Storage
function saveFilters() {
    
    for(let [key, val] of Object.entries(selectDOM)) {
        if(key !== 'filterForm') {
              val.onchange = function() {
                 localStorage[key] = this.value;
            } 
        }
    }
}



/* Remove filter tags */
function removeFilterTag() {
    // Remove all tags
    let filterDiv = document.querySelectorAll('.filter-tag');
    filterDiv.forEach(function (filterTag) {
        filterTag.remove();
    });
}

/*
*   Set filter parameters
*/

function setFilterParam() {

    let currentUrl = window.location.href;
    let urlObject = new URL(currentUrl);
    
    let projectParam =  urlObject.searchParams.get("project");
    let propertyParam = urlObject.searchParams.get("appartment");
    
    // Save filters to local storage
   let localKeys = Object.keys(selectDOM);
    
    // Select project and property in filter page
    if (propertyParam !== null) {
        
        let property = `For ${propertyParam}`.toLowerCase()
                                .split(' ').map((s) => s.charAt(0).toUpperCase() +  s.substring(1))
                                .join(' ');
        
        localStorage[localKeys[0]] = property;
        
        // capitalize every letter in word
        selectDOM.selectProperty.value = property;
        
        
    } 
    
    if(projectParam !== null) {
   
        if(propertyParam == null) {
            selectDOM.selectProperty.value = "All Property";
             localStorage[localKeys[0]] = "All Property";
        }
        
        let project = decodeURIComponent(projectParam).replace(/ /g, '');
    
        if(project == "ZundasDarzi") {
            localStorage[localKeys[1]] = 1;
        } else {
            localStorage[localKeys[1]] = 2;
        }
    }
    
    
  
}

setFilterParam();

/**
 * Get all selected value
*/

selectDOM.filterForm.addEventListener('change', filterFormValues);

/**
 * return string
 */
function getCurrentLang() {
    let lang = selectDOM.currentLang.toLowerCase();
    return lang.charAt(0).toUpperCase() + lang.substr(1);
}

let filterParameters = {
    "current_lang": getCurrentLang(),
    "project_id": null,
    "property_type": null,
    "min_price": null,
    "max_price": null,
    "rooms": null
}

function filterFormValues() {

    let filtersArray = [
        selectDOM.selectProjects.options[selectDOM.selectProjects.selectedIndex],
        selectDOM.selectProperty.options[selectDOM.selectProperty.selectedIndex],
        document.querySelectorAll(".filter-appartment input")[0],
        document.querySelectorAll(".filter-appartment input")[1],
        selectDOM.selectRooms.options[selectDOM.selectRooms.selectedIndex]
    ];
    


    removeFilterTag();

    let appartmentDiv = document.querySelector(".appartments-carts > div");
    appartmentDiv.innerHTML = "";

    let keys = Object.keys(filterParameters); // Filterparameters object keys
    keys.shift(); // Removing the first key, which is not null
    keys.forEach((key, i) => filterParameters[key] = filtersArray[i].value); // insert array value into an object
   

    // send data to php via ajax
    $.ajax({
        url: 'app/views/classes/FilterData.php',
        type: "POST",
        dataType: 'json',
        data: {
            param: filterParameters
        },
        success: function (data) {

            // Get total records from db and delte key and value from object
            let totalRecords = data['Total_Records'];
            delete data['Total_Records'];

            // Add to DOM available appartments
            setTotalRecordsFound(totalRecords);

            for (let key in data) {
                //Call function with parameters to build Appartment card DOM
                appartmentDiv.innerHTML += AppartmentResultSet(
                    data[key].Project_ID,
                    data[key].id,
                    data[key].title,
                    data[key].url_title,
                    data[key].Appartment_Price,
                    data[key].Property_Type,
                    data[key].image,
                    data[key].PD_Rooms,
                    data[key].PD_SIZE,
                    data[key].id,
                    data[key].Property_ID,
                    data[key].PD_Floor,
                    data[key].PD_AptFloor
                    
                );
            }
        },
        error: function (data) {
            var responseText = JSON.parse(data.responseText);
        }

    });
}




function setTotalRecordsFound(totalRecords) {
    let resultsFound = document.querySelector(".top-result-indicator span");
    resultsFound.innerHTML = totalRecords;
}


/**
 * Property Result from php
 */

function AppartmentResultSet(project, id, title, urlParam, price, type, image, room, size, id, property, totalfloor, floor) {


    let lang = selectDOM.currentLang.toLowerCase();
    let floorTranslation, roomsTranslation, monthTranslation, propertyTranslation;
   
    let projectName = (project == 1) ? "ZundasDarzi" : "Riverside";


    switch(lang) {
        case 'ru':
            floorTranslation = translation.Floor_ru;
            roomsTranslation = translation.Room_ru;
            monthTranslation = translation.Month_ru;
            propertyTranslation = (type == 'For sale') ? translation.ForSale_ru : translation.ForRent_ru;
        break;
        case 'lv':
            floorTranslation = translation.Floor_lv;
            roomsTranslation = translation.Room_lv;
            monthTranslation = translation.Month_lv;
            propertyTranslation = (type == 'For sale') ? translation.ForSale_lv : translation.ForRent_lv;
        break;
        case 'en':
            floorTranslation = translation.Floor_en;
            roomsTranslation = translation.Room_en;
            monthTranslation = translation.Month_en;
            propertyTranslation = (type == 'For sale') ? translation.ForSale_en : translation.ForRent_en;
        break;       
            
            
    }

    let titleUrlParam = urlParam.trim().replace(/ /g, '_');
    
    let apartmentPrice = price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

    (property == 2) ? apartmentPrice = `&euro; ${apartmentPrice} / ${monthTranslation}` : apartmentPrice = `&euro; ${apartmentPrice}`;

    
    return `
        <div class="col-sm-6 col-lg-4">
            <div class="img-block">
                <span class="property-label">${projectName}</span>
                <a href="appartment/${titleUrlParam}&id=${id}/?lang=${selectDOM.currentLang.toLowerCase()}">
                    <img class="image" src="images/appartment/${image}">
                </a>
            </div>

            <div class="appartment-well-cart">

                <div class="appartment-description">
                    <h5 class="appartment-title text-center">
                        <a href="appartment/${titleUrlParam}&id=${id}/?lang=${selectDOM.currentLang.toLowerCase()}">${title}</a>
                    </h5>
                    <div class="specifications">
                        <p><img src="/images/stairs-with-handrail.svg">${floorTranslation}: ${floor}/${totalfloor}</p>
                        <p><i class="far fa-square"></i>${size} m<sup>2</sup></p>
                        <p><i class="fas fa-bed"></i>${roomsTranslation}: ${room}</p>
                    </div>
                </div>
                <hr>
                <div class="price-block">
                    <p>${propertyTranslation}</p>
                    <p class="price">${apartmentPrice}</p>
                </div>
                <input type="hidden" name="apartment-id" value="${id}">
            </div>

        </div>
    `;

}


// change photo inside slider
let projectSelection = selectDOM.selectProjects;
projectSelection.addEventListener('change', function () {
    displayProjectInSlider(projectSelection.options[projectSelection.selectedIndex].text);
})


function displayProjectInSlider(projectID) {

    const projects = [
        {
            'project': "Zundas Darzi",
            'photo': ['cam01717b1-pr0043-still15_42620800234_o', 'cam01717b1-pr0043-still20_29471650508_o'],
            'text': ['Text Zundas 1', 'Text Zundas 2']
        },
        {
            'project': "Riverside",
            'photo': ['20170801_114253', 'cam00317b0-pr0222-still01_35713486956_o'],
            'text': ['Text River 1', 'Text River 2']
        }
    ];

    let slides = $('.carousel-inner');
    let slides_query = document.querySelector('.carousel-inner');

    //Delete all images from slider
    while (slides_query.firstChild) slides_query.removeChild(slides_query.firstChild);

    if (projectID == 'ZundasDarzi') {

        for (let i = 0; i < projects[0].photo.length; i++) {
            slides.append(renderSlides(projects[0].photo[i], projects[0].text[i]));
        }

    } else if (projectID == 'Riverside') {
        for (let i = 0; i < projects[1].photo.length; i++) {
            slides.append(renderSlides(projects[1].photo[i], projects[1].text[i]));
        }
    }
    else {
        // Output all projects
        projects.forEach(function (item) {
            for (let i = 0; i < item.photo.length; i++) {
                slides.append(renderSlides(item.photo[i], item.text[i]));
            }
        });
    }

    $('.carousel-item:first-child').addClass("active");

};
displayProjectInSlider(projectSelection.options[projectSelection.selectedIndex].text);


function renderSlides(image, text) {
    return `
    <div class='carousel-item'>
        <img src='/public/images/Slider/${image}.jpg'>
        <div class="info-project">
            <h1 class="h-underline animated fadeInDown delay-1s">RiverSide Project</h1>
            <p class="animated fadeInLeft delay-1s">${text}</p>
        </div>
    </div>
    `;
}


loadFilters();

filterFormValues();
// Call function loadFilters that should be called after DOM is generated

saveFilters();


