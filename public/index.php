<?php

session_start();

require_once __DIR__ . '/../vendor/autoload.php';

// Load Config file
require_once '../app/config/config.php';


/* Language definition */
$i18n = new i18n(__DIR__ .'/../vendor/philipp15b/php-i18n/lang/lang_{LANGUAGE}.yml', 'langcache/', 'en');
// Parameters: language file path, cache dir, default language (all optional)

// init object: load language files, parse them if not cached, and so on.
$i18n->setPrefix('LANG');
$i18n->init();

$languages = ['EN', 'LV', 'RU'];
// Current active language
$activeLang = strtoupper($i18n->getAppliedLang());


//$_SESSION['lang'] = ucfirst(strtolower($activeLang));
$_SESSION['lang'] = $activeLang;


// Remove active language from dropdown
if (($key = array_search($activeLang, $languages)) !== false) {
    unset($languages[$key]);
}


$database   = new \Libs\Database;
$init       = new \Libs\Core;
$controller = new \Libs\Controller;

use Libs\Core;
use Libs\Database;
use Libs\Controller;